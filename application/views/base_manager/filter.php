<div class="col-md-12">
	<div class="content-manage e_content_manage">
		<div class="table-content">
			<div class="filter-block">
				<?php if (!empty($data_filter) && is_array($data_filter)): ?>
					<?php foreach ($data_filter as $filter_item): ?>
						<span class="filter-select js-filter" data-filter="<?= empty($filter_item['db_field']) ? '' : $filter_item['db_field']; ?>">
							<label><?= empty($filter_item['label']) ? 'Label' : $filter_item['label']; ?></label>
							<select class="selectpicker" multiple data-live-search="true">
								<?php $select_data = json_decode($filter_item['data_select']); ?>
								<?php if (!empty($select_data)): ?>
									<?php foreach ($select_data as $key => $value): ?>
										<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
									<?php endforeach; ?>
								<? else: ?>
									<option value="">Chưa có dữ liệu</option>
								<?php endif; ?>
							</select>
						</span>
					<?php endforeach; ?>
				<?php endif; ?>
				<span>
					<span class="action-bar">
						<label>Tìm kiếm</label>
						<input class="input-search form-control" name="search-user" type="text" placeholder="Nhập nội dung tìm kiếm">
						<i class="fa fa-times btn-reset-search js-btn-reset-search"
						   title="Click để reset dữ liệu tìm kiếm"></i>
					</span>
				</span>
				<span class="sort-item js-filter-by-time hidden" style="margin-right: 20px;">
					<label>Ngày đăng ký từ: </label>
					<input class="input-date-from" type="datetime-local">
					<label>Đến: </label>
					<input class="input-date-to" type="datetime-local">
				</span>
				<span class="sort-item js-filter-item order-by-sort">
					<label>Sắp xếp theo: </label>
					<select class="js-filter-item-select">
						<option value="desc" data-filter-row="id" selected> Mới nhất</option>
						<option value="asc" data-filter-row="id">Cũ nhất</option>
					</select>
				</span>
			</div>
			<div class="count-row">
				<span class="float-right">
					<label>Kích thước trang</label>
<!--					<input class="form-control current-limit js-limit-value" name="limit-page" type="number" value="10" min="0" max="500">-->
                    <select class="js-filter-item-select current-limit js-limit-value">
						<option value="10" selected>10</option>
						<option value="20">20</option>
                        <option value="50">50</option>
					</select>
				</span>
			</div>
		</div>
	</div>
</div>
