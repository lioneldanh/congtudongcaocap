<div class="modal fade delete-popup-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Delete</h4>
			</div>
			<div class="modal-body">
				<p>Bạn có chắc chắn muốn xoá <span id="count-item">1</span> item?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-delete-data">Xoá</button>
				<button type="button" class="btn btn-secondary btn-close-popup" data-dismiss="modal">Đóng</button>
			</div>
		</div>
	</div>
</div>
