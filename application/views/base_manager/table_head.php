<div class="row table-head">
	<div class="col-sm-12">
		<div class="action-bar-top float-left">
			<span class="title-menu"><?= isset($title) ? $title : '' ?></span>
		</div>
		<?php if (!empty($url_add)) : ?>
		<div class="action-bar-top float-right">
			<a href="<?= isset($url_add) ? site_url($url_add) : '' ?>"><span class="btn btn-primary">Thêm mới</span></a>
		</div>
		<?php endif; ?>
	</div>
</div>
