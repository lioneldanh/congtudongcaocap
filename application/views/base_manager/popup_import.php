<div class="modal fade import-popup-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Import</h4>
                <button type="button" class="close" aria-label="Close" data-dismiss="modal">
                    <i aria-hidden="true">&times;</i>
                </button>
			</div>
			<div class="modal-body">
                <form method="post" id="import_file" enctype="multipart/form-data">
                    <div class="form-group ">
                        <label for="input_file">Import</label>
                        <div class="custom-file">
                            <input type="file" name="file" id="input_file" class="custom-file-input"/>
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                    </div>
                    <?php if(isset($csv_link)):?>
                    <div class="csv-dowload">
                        <br>
                        <a href="<?= $csv_link ?>" download="Example.xlsx">Example Xlsx</a>
                    </div>
                    <?php endif;?>
                </form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-import-data">Import</button>
				<button type="button" class="btn btn-secondary btn-close-popup" data-dismiss="modal">Đóng</button>
			</div>
		</div>
	</div>
</div>
