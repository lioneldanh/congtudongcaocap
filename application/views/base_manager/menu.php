<div class="ml-2 ">
	<?php if ($this->session->userdata("username")) : ?>
	<div class="dropdown">
		<button class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<img class="img-show" src="<?= base_url('assets/img/menu.png')?>">
		</button>
		<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item <?= isset($manage_author) ? 'selected' : '' ?>"
			   href="<?php echo site_url('manage_author') ?>">Quản lý Tác giả </a>
			<a class="dropdown-item <?= isset($manager_publisher) ? 'selected' : '' ?>"
			   href="<?php echo site_url('manage_publisher') ?>">Quản lý Nhà xuất bản</a>
			<a class="dropdown-item <?= isset($manage_category) ? 'selected' : '' ?>"
			   href="<?php echo site_url('manage_category') ?>">Quản lý Danh mục</a>
			<a class="dropdown-item <?= isset($manage_tool_config) ? 'selected' : '' ?>"
			   href="<?php echo site_url('manage_tool_config') ?>">Quản lý Pháp khí</a>
			<a class="dropdown-item <?= isset($manage_book) ? 'selected' : '' ?>"
			   href="<?php echo site_url('manage_book') ?>">Quản lý sách</a>
			<a class="dropdown-item <?= isset($manage_revoke_book) ? 'selected' : '' ?>"
			   href="<?php echo site_url('manage_revoke_book') ?>">Quản lý sách đã thu hồi</a>
			<a class="dropdown-item <?= isset($manage_published_book) ? 'selected' : '' ?>"
			   href="<?php echo site_url('manage_published_book') ?>">Quản lý sách đã xuất bản</a>
			<a class="dropdown-item <?= isset($manage_book_deleted) ? 'selected' : '' ?>"
			   href="<?php echo site_url('manage_book_deleted') ?>">Quản lý sách đã xoá</a>
			<a class="dropdown-item <?= isset($manage_ts) ? 'selected' : '' ?>"
			   href="<?php echo site_url('manage_ts') ?>">Cấu hình túc số</a>
		</div>
		<?php endif; ?>
	</div>
	<div id="loading-modal" style="display: none">
		<div class="loader"></div>
	</div>
</div>
