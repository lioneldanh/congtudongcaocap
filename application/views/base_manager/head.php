<html xmlns="http://www.w3.org/1999/html">
<head>
	<title><?= isset($title) ? $title : '' ?></title>
	<link rel="stylesheet" href="<?php echo base_url("bower_components/bootstrap/dist/css/bootstrap.css"); ?>"/>
	<link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap-select.css"); ?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/css/sync_user_import/manager.css"); ?>"/>
	<link rel="stylesheet" href="<?php echo base_url("assets/css/datepicker/datepicker.css"); ?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/css/styles.css"); ?>"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
			integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
			crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
			integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
			crossorigin="anonymous"></script>
	<script type="text/javascript" src="<?php echo base_url("bower_components/jquery/dist/jquery.min.js"); ?>"></script>
	<script type="text/javascript">
		window.jQuery || document.write("<script src='<?php echo base_url('bower_components/jquery/dist/jquery.min.js') ?>'>" + "<" + "/script>");
	</script>
	<script src="<?php echo base_url("bower_components/bootstrap/dist/js/bootstrap.min.js"); ?>"></script>
	<script src="<?php echo base_url("assets/js/bootstrap-select.min.js"); ?>"></script>
	<script type="text/javascript">
		if ('ontouchstart' in document.documentElement)
			document.write("<script src='<?php echo base_url('bower_components/jquery-mobile/js/jquery.mobile.js'); ?>'>" + "<" + "/script>");
	</script>

	<script src="<?php echo base_url("assets/js/notify.js"); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/base_table.js"); ?>"></script>
	<script src="<?php echo base_url("assets/js/datepicker/moment.min.js"); ?>"></script>
	<script src="<?php echo base_url("assets/js/datepicker/datepicker.js"); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/all.js"); ?>"></script>
	<script src="<?php echo base_url("assets/js/user/table.js"); ?>"></script>
	<script type="text/javascript" src="/assets/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/assets/ckfinder/ckfinder.js"></script>
</head>
