<?php ?>
<ul class="e_data_paginate pagination justify-content-end pagination-sm">
    <?php
    if ($current_page == 1) {
        echo '<li data-page="1" class="disabled page-item"><a class="page-link to_first disable">&nbsp;&nbsp;Trang đầu</a></li>';
    } else {
        echo '<li class="page-item"><a class="page-link" title="Click để tới trang đầu tiên" data-page="1" class="to_first page-item" href="' . $link . '/' . 0 . '">&nbsp;&nbsp;Trang đầu</a></li>';
    }
    if ($current_page > 1) {
        echo '<li class="click-able prev-btn page-item" title="Click để tới trang phía trước"><a class="page-link" data-page="' . ($current_page - 1) . '" href="' . $link . '/' . (($current_page - 2) * $key) . '">Trước</a></li>';
    } else {
        echo '<li class="disabled prev-btn page-item"><a class="page-link" data-page="' . ($current_page - 1) . '" class=disable">Trước</a></li>';
    }

    ?>
    <li class="page-item current-page-item">
        <label>Trang số: </label>
        <input type="number" value="<?php echo $current_page; ?>"> /<?php echo empty($total_page) ? 1 : $total_page ?>
    </li>

    <?php

    if ($current_page < $total_page) {
        echo '<li class="click-able next-btn page-item" title="Click để tới trang tiếp theo"><a class="page-link" data-page="' . ($current_page + 1) . '" href="' . $link . '/' . (($current_page) * $key) . '">Tiếp</a></li>';
    } else {
        echo '<li class="disabled next-btn page-item"><a class="page-link" data-page="' . ($current_page) . '" class=disable">Tiếp</a></li>';
    }
    if ($current_page == $total_page) {
        echo '<li class="disabled page-item"><a class="page-link" data-page="' . $total_page . '" class="to_end disable">Trang cuối&nbsp;</a></li>';
    } else {
        echo '<li class="page-item" title="Click để tới trang cuối"><a class="page-link" data-page="' . $total_page . '" class="to_end" href="' . $link . '/' . $total_page . '">Trang cuối&nbsp;</a></li>';
    }
    ?>
</ul>
