<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="mediumModalLabel">Xác nhận hành động</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>Bạn có chắc chắn muốn xử lý <?php echo count($list_id); ?> bản ghi này?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
            <button type="submit" class="btn btn-primary btn-submit-public-private" data-dismiss="modal"
                    data-list-id="<?php echo json_encode($list_id); ?>"
                    data-type = '<?php echo $type?>'
                    data-url="<?php echo empty($url_submit) ? '' : $url_submit; ?>">OK, Thực hiện
            </button>
        </div>
    </div>
</div>

