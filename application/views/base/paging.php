<?php  ?>
<ul class="e_data_paginate pagination justify-content-end pagination-sm">
	<?php
	if ($current_page == 1) {
		echo '<li data-page="1" class="disabled page-item"><a class="page-link to_first disable">&nbsp;&nbsp;<i class="fa fa-angle-double-left fa-2x fa-bold" aria-hidden="true"></i></a></li>';
	} else {
		echo '<li class="page-item"><a class="page-link" data-page="1" class="to_first page-item" href="' . $link . '/' . 0 . '">&nbsp;&nbsp;<i class="fa fa-angle-double-left fa-2x fa-bold" aria-hidden="true"></i></a></li>';
	}
	if ($current_page > 1) {
		echo '<li class="click-able prev-btn page-item"><a class="page-link" data-page="' . ($current_page - 1) . '" href="' . $link . '/' . (($current_page - 2) * $key) . '"><i class="fa fa-angle-left fa-2x fa-bold" aria-hidden="true"></i></a></li>';
	} else {
		echo '<li class="disabled prev-btn page-item"><a class="page-link" data-page="' . ($current_page - 1) . '" class=disable"><i class="fa fa-angle-left fa-2x fa-bold" aria-hidden="true"></i></a></li>';
	}
	//	if ($total_page <= $page_link_display) {
	//		//Hiển thị tất cả
	//		for ($i = 1; $i < ($total_page + 1); $i++) {
	//			if ($i == $current_page) {
	//				echo '<li class="active page-item"><a class="page-link" data-page="' . $i . '" href="#">' . $i . '</a></li>';
	//			} else {
	//				echo '<li class="page-item"><a class="page-link" data-page="' . $i . '" href="' . $link . '/' . (($i - 1) * $key) . '">' . $i . '</a></li>';
	//			}
	//		}
	//	} else {
	//		if ($current_page > intval($page_link_display / 2) && $current_page < $total_page - intval($page_link_display / 2)) {
	//			//Hiển thị mỗi 1/2 ở mỗi đầu trước ($page_link_display / 2)-1 => $current_page
	//			for ($i = 0; $i < (intval($page_link_display / 2)); $i++) {
	//				echo '<li class="page-item"><a class="page-link" data-page="' . ($current_page - intval($page_link_display / 2) + $i) . '" href="' . $link . '/' . (($current_page - intval($page_link_display / 2) + $i - 1) * $key) . '">' . ($current_page - intval($page_link_display / 2) + $i) . '</a></li>';
	//			}
	//			//Hiển thị giữa
	//			echo '<li class="active page-item"><a class="page-link" data-page=' . $current_page . ' href="#">' . $current_page . '</a></li>';
	//			//Hiển thị mỗi 1/2 ở mỗi đầu sau $current_page + 1 => $page_link_display / 2
	//			for ($i = 1; $i < (intval($page_link_display / 2) + 1); $i++) {
	//				echo '<li class="page-item"><a class="page-link" data-page="' . ($current_page + $i) . '" href="' . $link . '/' . (($current_page + $i - 1) * $key) . '">' . ($current_page + $i) . '</a></li>';
	//			}
	//		} elseif ($current_page <= intval($page_link_display / 2)) {
	//			//Hiển thị tất cả $page_link_display ở đầu trước
	//			for ($i = 1; $i < ($page_link_display + 1); $i++) {
	//				if ($i == $current_page) {
	//					echo '<li class="active page-item"><a class="page-link" data-page="' . $i . '" href="#">' . $i . '</a></li>';
	//				} else {
	//					echo '<li class="page-item"><a class="page-link" data-page="' . $i . '"  href="' . $link . '/' . (($i - 1) * $key) . '">' . $i . '</a></li>';
	//				}
	//			}
	//		} elseif ($current_page >= $total_page - intval($page_link_display / 2)) {
	//			//Hiển thị tất cả $page_link_display ở đầu sau
	//			for ($i = 1; $i < ($page_link_display + 1); $i++) {
	//				if (($total_page - $page_link_display + $i) == $current_page) {
	//					echo '<li class="active page-item"><a class="page-link" data-page="' . ($total_page - $page_link_display + $i) . '" href="#">' . ($total_page - $page_link_display + $i) . '</a></li>';
	//				} else {
	//					echo '<li class="page-item"><a class="page-link" data-page="' . ($total_page - $page_link_display + $i) . '" href="' . $link . '/' . (($total_page - $page_link_display + $i - 1) * $key) . '">' . ($total_page - $page_link_display + $i) . '</a></li>';
	//				}
	//			}
	//		}
	//	}

	?>
	<li class="page-item current-page-item pt-1">
		<label>Trang số: </label>
		<input type="number" value="<?php echo $current_page; ?>"> /<?php echo empty($total_page) ? 1 : $total_page ?>
	</li>

	<?php

	if ($current_page < $total_page) {
		echo '<li class="click-able next-btn page-item"><a class="page-link" data-page="' . ($current_page + 1) . '" href="' . $link . '/' . (($current_page) * $key) . '"><i class="fa fa-angle-right fa-2x fa-bold" aria-hidden="true"></i></a></li>';
	} else {
		echo '<li class="disabled next-btn page-item"><a class="page-link" data-page="' . ($current_page) . '" class=disable"><i class="fa fa-angle-right fa-2x fa-bold" aria-hidden="true"></i></a></li>';
	}
	if ($current_page == $total_page) {
		echo '<li class="disabled page-item"><a class="page-link" data-page="' . $total_page . '" class="to_end disable"><i class="fa fa-angle-double-right fa-2x fa-bold" aria-hidden="true"></i>&nbsp;</a></li>';
	} else {
		echo '<li class="page-item"><a class="page-link" data-page="' . $total_page . '" class="to_end" href="' . $link . '/' . $total_page . '"><i class="fa fa-angle-double-right fa-2x fa-bold" aria-hidden="true"></i>&nbsp;</a></li>';
	}
	?>
</ul>
