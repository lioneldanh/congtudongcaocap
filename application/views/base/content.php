<?= isset($head) ? $head : ''; ?>
<body id='main_body' class="content-layout manager_user" data-ajax-url="">
<div class="container full-width" style="width: 90%; position: relative; max-width: 90%;">
	<header class="page-header row">
		<div class="col-md-6">
			<?= isset($menu) ? $menu : ''; ?>
		</div>
		<div class="col-md-6">
			<?= isset($header) ? $header : ''; ?>
		</div>
	</header>
	<div class="content  main-content">
		<div class="animated fadeIn">
			<?= isset($table_head) ? $table_head : ''; ?>
			<div class="row">
				<?= isset($filter) ? $filter : ''; ?>
				<div class="col-sm-4 mb-3">
					<div class="page-header float-left">
						<button class="btn btn-danger btn-delete-data-select">Xoá</button>
					</div>
				</div>
			</div>
		</div>
		<div class="data-table"
			 data-url="<?= isset($url_ajax_data_table) ? site_url($url_ajax_data_table) : ''; ?>">
			<p id="count-selected" style="font-style: italic"></p>
			<div class="ajax-data-table"></div>
		</div>
	</div>
</div>
<?= isset($popup_preview) ? $popup_preview : '' ?>
<?= isset($popup_delete) ? $popup_delete : ''; ?>
<?= isset($popup_import) ? $popup_import : ''; ?>
</body>
</html>
