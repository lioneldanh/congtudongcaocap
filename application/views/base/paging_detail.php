<?php if($from < $to){ ?>
    <span class="paging-detail">
	Hiển thị bản ghi số <span class="from_offset"><?php echo empty($from) || empty($total) ? '0' : $from; ?></span> đến <span
                class="to_offset"><?php echo empty($to) ? '0' : $to; ?></span>
	trên tổng số <span class="total_item"><?php echo empty($total) ? '0' : $total; ?></span> bản ghi
</span>
<?php }elseif($from == $to) {?>
    <span class="paging-detail">Có 1 bản ghi.</span>
<?php }else { ?>
    <span class="paging-detail">Không có bản ghi nào.</span>
<?php } ?>
