<div class="modal-dialog modal-sm" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="mediumModalLabel">Xác nhận xóa dữ liệu</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<p>Bạn có chắc chắn muốn xóa <?php echo count($list_id); ?> bản ghi này chứ</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
			<button type="submit" class="btn btn-danger btn-submit-delete" data-dismiss="modal"
					data-list-id="<?php echo json_encode($list_id); ?>"
					data-url="<?php echo empty($url_delete) ? '' : $url_delete; ?>">OK, Xóa
			</button>
		</div>
	</div>
</div>

