<div class="table-container">
	<table class="table table-striped table-bordered">
		<thead class="thead-dark">
		<tr>
			<th scope="col" class="col-check"><input class="check-all" type="checkbox"></th>
			<th scope="col">Họ và tên</th>
			<th scope="col">Tên đăng nhập</th>
			<th scope="col">Email</th>
			<th scope="col">Đăng nhập gần nhất</th>
			<th scope="col" class="col-action">Thao tác</th>
		</tr>
		</thead>
		<tbody class="js-data-table">
		<?php
		if (!empty($record_list_data)) {
			foreach ($record_list_data as $record_data) { ?>
				<tr data-record-id="<?php echo empty($record_data->id) ? ' ' : $record_data->id; ?>">
					<td class="col-check"><input class="check-item" type="checkbox"
												 data-id="<?php echo $record_data->id; ?>"></td>
					<td><?php echo empty($record_data->fullname) ? ' ' : $record_data->fullname; ?></td>
					<td><?php echo empty($record_data->username) ? ' ' : $record_data->username; ?></td>
					<td><?php echo empty($record_data->email) ? ' ' : $record_data->email; ?></td>
					<td><?php echo empty($record_data->last_login) ? 'Chưa đăng nhập' : $record_data->last_login; ?></td>
					<td class="col-action">
						<button class="btn btn-sm btn-primary btn-edit-record e_ajax_form" title="Cập nhật tài khoản"
								data-url="<?php echo site_url('user/add/' . $record_data->id) ?>"
						><i class="fa fa-pencil"></i>
						</button>
<!--						<button class="btn btn-sm btn-danger btn-delete-record" title="Xóa tài khoản"-->
<!--								data-url="--><?php //echo site_url('user/delete/' . $record_data->id) ?><!--"-->
<!--						><i class="fa fa-trash-o"></i>-->
<!--						</button>-->
					</td>
				</tr>
			<?php }
		} ?>
		</tbody>
	</table>
</div>
<?php echo empty($paging_detail) ? '' : $paging_detail; ?>
<?php echo $paging; ?>
