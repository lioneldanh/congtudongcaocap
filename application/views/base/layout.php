<!DOCTYPE html>
<html lang="vi">
<head>
<!--    <link rel="shortcut icon" type="image/png" href="--><?php //echo base_url('assets/img/logo.png') ?><!--"/>-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8"/>
    <!--Font material-icons-->
    <!--<link href="-->
    <?php //echo base_url("assets/icon/MaterialIcons/material-icons.css"); ?><!--" rel="stylesheet">-->
    <link rel="stylesheet" href="<?php echo base_url("bower_components/font-awesome/css/font-awesome.min.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("bower_components/flag-icon-css/css/flag-icon.min.css"); ?>">
    <link rel="stylesheet"
          href="<?php echo base_url("vendor/components/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"); ?>">

    <!-- basic scripts -->

    <script type="text/javascript" src="<?php echo base_url("bower_components/jquery/dist/jquery.min.js"); ?>"></script>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='<?php echo base_url('bower_components/jquery/dist/jquery.min.js') ?>'>" + "<" + "/script>");
    </script>

    <script type="text/javascript">
        if ('ontouchstart' in document.documentElement)
            document.write("<script src='<?php echo base_url('bower_components/jquery-mobile/js/jquery.mobile.js'); ?>'>" + "<" + "/script>");
    </script>
    <script src="<?php echo base_url("bower_components/popper.js/dist/umd/popper.min.js"); ?>"></script>

    <script src="<?php echo base_url("bower_components/jquery-ui/jquery-ui.js"); ?>"></script>
    <script src="<?php echo base_url("bower_components/bootstrap/dist/js/bootstrap.min.js"); ?>"></script>
    <script
            src="<?php echo base_url("vendor/components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"); ?>"></script>
    <script src="<?php echo base_url("bower_components/jquery-validation/dist/jquery.validate.min.js"); ?>"></script>
    <script src="<?php echo base_url("bower_components/jquery-validation/dist/additional-methods.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/bootstrap-select.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/datatables.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/messages_vi.js"); ?>"></script>
    <link rel="stylesheet" href="<?php echo base_url("assets/css/summernote-bs4.css"); ?>">
    <script src="<?php echo base_url("assets/js/summernote-bs4.js"); ?>"></script>

    <!--Modal-->
    <script src="<?php echo base_url("assets/js/notify.js"); ?>"></script>
    <script src="<?php echo base_url("assets/sufee/js/main.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/base_table.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/base.js"); ?>"></script>
    <!---Custom css--->
    <link rel="stylesheet" href="<?php echo base_url("bower_components/jquery-ui/themes/base/jquery-ui.min.css"); ?>"/>
    <link rel="stylesheet" href="<?php echo base_url("bower_components/bootstrap/dist/css/bootstrap.css"); ?>"/>
    <link rel="stylesheet"
    <link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap-select.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/css/datatables.min.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/sufee/css/style.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/css/admin.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/css/base.css"); ?>">
    <title>GEK Admin System</title>
    <?php
    if (!empty($list_js_head)) {
        foreach ($list_js_head as $js) { ?>
            <script src="<?php echo base_url($js); ?>"></script>
        <?php }
    }

    if (!empty($list_css)) {
        foreach ($list_css as $css) { ?>
            <link rel="stylesheet" href="<?php echo base_url($css); ?>">
        <?php }
    }
    ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135468051-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-135468051-1');
    </script>


</head>
<body class="">
<!-- Left Panel -->

<aside id="left-panel" class="left-panel" style="display: none">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <h3 class="menu-title">GEK Admin System</h3>
                <?php
                if (!empty($data_menu) && is_array($data_menu)) {
                    $user_menu = !empty($this->session->userdata('user_menu')) ? $this->session->userdata('user_menu') : NULL;
                    foreach ($data_menu as $menu_item) {
                        if ($this->session->userdata('user_id') == 1 || (!empty($user_menu) && in_array($menu_item['controller'], $user_menu)) ||
                        in_array($this->session->userdata('user_role'),['supper_admin,organization_admin'])) {
                            if (empty($menu_item['menu_child'])) {
                                ?>
                                <li class="<?php if ($controller == $menu_item['controller']) echo 'active'; ?>">
                                    <a href="<?php echo site_url($menu_item['controller']); ?>"><?php echo empty($menu_item['label']) ? '' : $menu_item['label']; ?>
                                    </a>
                                </li>
                            <?php } else { ?>
                                <li class="menu-item-has-children dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                       aria-expanded="false"><?php echo empty($menu_item['label']) ? '' : $menu_item['label']; ?>
                                    </a>
                                    <ul class="sub-menu children dropdown-menu">
                                        <?php
                                        $list_menu_child = $menu_item['menu_child'];
                                        foreach ($list_menu_child as $menu_child) { ?>
                                            <li>
                                                <a
                                                        href="<?php echo site_url($menu_child['controller']); ?>"><?php echo empty($menu_child['label']) ? '' : $menu_child['label']; ?></a>
                                            </li>

                                        <?php }
                                        ?>
                                    </ul>
                                </li>
                            <?php }
                        }
                    }
                }

                ?>

            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside><!-- /#left-panel -->

<!-- Left Panel -->
<div id="right-panel" class="right-panel">
    <div id="loading-modal">
        <div class="loader"></div>
    </div>
    <!-- Header-->
    <header id="header" class="header">

        <div class="header-menu">

            <div class="col-sm-7">
				<span id="btnMenu" class="menutoggle pull-left" data-open="0">
					<i id="btnMenuOpen" class="icon-open fa fa-chevron-down hidden"></i>
					<i id="btnMenuClose" class="icon-close fa fa-list"></i></span>
            </div>
            <div class="col-sm-9 breadcrumb-logo">
                <img class="m-auto" src="<?php echo base_url('assets/img/logo.png'); ?>" alt="">
                <span class="breadcrumb-controller">
					<?php
                    if (!empty($breadcrumb_data['first_controller'])) { ?>

                        <a href="<?php echo site_url(); ?>"><i class="fa fa-home icon-root"></i></a>
                        <i class="fa fa-angle-right icon-next"></i>
                        <span
                                class="controller-name first-controller"><?php echo $breadcrumb_data['first_controller']; ?></span>
                    <?php } ?>
                    <?php
                    if (!empty($breadcrumb_data['second_controller'])) { ?>
                        <i class="fa fa-angle-right icon-next"></i>
                        <span
                                class="controller-name second-controller"><?php echo $breadcrumb_data['second_controller']; ?></span>
                    <?php }
                    ?>

                    <?php
                    if (!empty($breadcrumb_data['list_method'][$method])) { ?>
                        <i class="fa fa-angle-right icon-next"></i>
                        <span
                                class="controller-name second-controller"><?php echo $breadcrumb_data['list_method'][$method]; ?></span>
                    <?php }
                    ?>
				</span>
            </div>
            <div class="col-sm-3">
                <div class="user-area dropdown float-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">
                        <img class="user-avatar rounded-circle"
                             src="<?php echo base_url('assets/img/avatar_default.jpg') ?>" alt="User Avatar">
                        <span><?php echo empty($this->session->userdata('username')) ? 'Username' : $this->session->userdata('username'); ?></span>
                    </a>

                    <div class="user-menu dropdown-menu">
                        <a class="nav-link" href="<?php echo site_url('user/profile') ?>"><i class="fa fa-user"></i> Hồ
                            sơ cá nhân</a>

                        <a class="nav-link" href="<?php echo site_url('sys_config'); ?>"><i class="fa fa-cog"></i>
                            Cài đặt hệ thống</a>

                        <a class="nav-link" href="<?php echo site_url('login/logout') ?>"><i
                                    class="fa fa-power-off"></i> Đăng xuất</a>
                    </div>
                </div>
            </div>
        </div>

    </header><!-- /header -->
    <!-- Header-->
    <div class="container">
        <?php echo $content; ?>
    </div>
    <div class="modal fade" id="modalAjax" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel"
         aria-hidden="true">
    </div>

</div>
<div class="both"></div>
<?php
if (!empty($list_js_footer)) {
    foreach ($list_js_footer as $js) { ?>
        <script src="<?php echo base_url($js); ?>"></script>
    <?php }
}
?>
</body>
</html>
