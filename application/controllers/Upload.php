<?php

/**
 * Class Upload
**/
class Upload extends CI_Controller {

	/**
	 * Upload constructor.
	 */
	public function __construct() {
		parent::__construct();

	}

	/**
	 * Index
	 */
	public function index() {
		$name_file = '';
		/* Check file upload */
		if (!empty($_FILES['upload']['name'])) {
			$config['upload_path'] = "uploads/ckeditor/";
			/**
			 * Check folder exits
			 */
			if(!is_dir($config['upload_path'])){
				mkdir($config['upload_path'], 0777, true);
			}
			/* set config upload */
			$config['allowed_types'] = "*";
			$config['file_name'] = $_FILES['upload']['name'];

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			/* Upload file */
			if ($this->upload->do_upload('upload')) {
				$uploadData = $this->upload->data();
				$name_file = $uploadData['file_name'];
			}
		}
		$path = 'uploads/ckeditor/'.$name_file;
		$data_return = [];
		$data_return['uploaded'] = 1;
		$data_return['fileName'] = $name_file;
		$data_return['url'] = base_url($path);
		$data_return['src'] = base_url($path);
		echo json_encode($data_return);
		return true;
	}
}
