<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Licences
 * @property M_licence                   licence
 * @property M_organization_licence_pack organization_licence
 * @property M_licence_pack              licence_pack
 */
class Licences {
    protected $_ci;

    public function __construct() {
        $this->_ci = &get_instance();
        $this->_ci->load->model('M_licence', 'licence');
        $this->_ci->load->model('M_organization_licence_pack', 'organization_licence');
        $this->_ci->load->model('M_licence_pack', 'licence_pack');
    }

    /**
     * Supper admin sell licence pack for organization
     *
     * @param $data [id_pack,organization_id]
     * @return array
     */
    public function sell_licence_for_organization($data) {
        if (empty($data) || empty($data['id_pack']) || empty($data['organization_id'])) {
            return ['status' => FALSE, 'msg' => 'Dữ liệu gửi lên không hợp lệ'];
        }
        $pack = $this->_ci->licence_pack->get($data['id_pack']);
        if (empty($pack)) return ['status' => FALSE, 'msg' => 'Dữ liệu gửi lên không hợp lệ'];
        $organization_licence = $this->_ci->organization_licence->get_by([
            'licence_value'   => $pack->value,
            'organization_id' => $data['organization_id']
        ]);
        if (empty($organization_licence)) {
            $this->_ci->organization_licence->insert([
                'organization_id'          => $data['organization_id'],
                'number_licence_available' => $pack->value,
                'licence_value'            => $pack->value
            ]);
        } else {
            $this->_ci->organization_licence->update($organization_licence->id, [
                'number_licence_available' => $organization_licence->number_licence_available + $pack->value]);
        }
        return ['status' => TRUE, 'msg' => 'Xử lý thành công.'];
    }

    /**
     * Admin create licence for user
     *
     * @param $data
     * @return array
     */
    public function create_licence($data) {
        if (empty($data) || empty($data['licence_value']) || empty($data['id']) ||
            empty($data['username']) || empty($data['organization_id']))
            $organization_licence = $this->_ci->organization_licence->get_by(['licence_value' => $data['licence_value'], 'organization_id' => $data['organization_id']]);
        if (empty($organization_licence) || empty($organization_licence->number_licence_available)) {
            return ['status' => FALSE, 'msg' => 'Gói licence đã hết, vui lòng mua thêm.'];
        }
        $str = time() . $data['username'] . $data['id'];
        $str = md5($str);
        $data_licence = $this->encrypt_licence($str, $data);
        $this->_ci->organization_licence->update_with_condition(['number_licence_available' => $organization_licence->number_licence_available - 1],
            ['licence_value' => $data['licence_value'], 'organization_id' => $data['organization_id']]);
        $this->_ci->licence->insert([
            'code'            => $data_licence['original_licence'],
            'status'          => 'available',
            'user_id'         => $data['id'],
            'organization_id' => $data['organization_id'],
            'value'           => $data['licence_value'],
        ]);
        return ['status' => TRUE, 'licence' => $data_licence['licence']];
    }

    public function check_licence($licence, $list_original_licence) {
        foreach ($list_original_licence as $item) {
            if (md5($item) == str_replace('-', '', $licence)) {
                return ['status' => TRUE, 'original_licence' => $item];
            }
        }
        return ['status' => FALSE];
    }

    private function encrypt_licence($str, $data_user) {
        $key = $data_user['username'];
        $str = $this->encrypt($str, $key);
        $str = $this->encrypt($str, SECRET_KEY);
        return ['licence' => rtrim(chunk_split(md5($str), 4, '-'), '-'), 'original_licence' => $str];
    }

    private function encrypt($str, $key) {
        return openssl_encrypt($str, 'AES-128-CBC', $key, 0, SECRET_IV_KEY);
    }

    public function decrypt($str, $key) {
        return openssl_decrypt($str, 'AES-128-CBC', $key, 0, SECRET_IV_KEY);
    }

}
