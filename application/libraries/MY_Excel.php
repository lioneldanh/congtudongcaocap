<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . "/third_party/PHPExcel/PHPExcel.php";

class MY_Excel extends PHPExcel {
    protected $_ci;

    public function __construct() {
        $this->_ci = &get_instance();
        parent::__construct();
    }

    public function export_chat_report($record_list_data = array()) {
        $sheet = new PHPExcel();

        $ews = $sheet->getSheet(0);
        $ews->setTitle('Chat report');
        $this->set_default_header($sheet, 'BÁO CÁO LỊCH SỬ TƯƠNG TÁC', 'A3:K4', 'J5');

        $data_excel['content'] = $record_list_data["record_list_data"];

        $data_excel['header'] = Array(
            "stt"             => "STT",
            "fullname"        => "Họ Tên",
            "company"         => "Mã số NPP",
            "email"           => "Email",
            "phone"           => "SĐT",
            "operator_name"   => "Đào tạo viên",
            "time"            => "Thời gian",
            "date"            => "Ngày",
            "type"            => "Hình thức",
            "target_group_id" => "Chủ đề",
            "status_text"     => "Trạng thái",
        );
        $col = 0;
        // In ra header
        foreach ($data_excel['header'] as $field => $value) {
            $sheet->getActiveSheet()->setCellValueByColumnAndRow($col, 6, $value);
            $col++;
        }
        $row = 7;
        //Fill color header
        $sheet->getActiveSheet()->getStyle('A6:K6')->applyFromArray(
            array(
                'fill'      => array(
                    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'DBE5A1'),
                ),
                'alignment' => array(
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ),
                'font'      => array(
                    'bold'  => TRUE,
                    'color' => array('rgb' => '000000'),
                    'name'  => 'Cambria',
                    'size'  => 10,
                ),
            )
        );
        $sheet->getActiveSheet()->getStyle('A6:L6')->getAlignment()->setWrapText(TRUE);
        $i = 1;
        foreach ($data_excel['content'] as $row_data) {
            $col = 0;
            $row_data->stt = $i;
            // In ra row
            foreach ($data_excel['header'] as $field_name => $value) {
                if ($field_name == 'time') {
                    $sheet->getActiveSheet()->setCellValueByColumnAndRow($col, $row, date('H:i:s', $row_data->time));
                } elseif ($field_name == 'date') {
                    $sheet->getActiveSheet()->setCellValueByColumnAndRow($col, $row, date('d/m/Y', $row_data->time));
                } else {
                    $sheet->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $row_data->$field_name);
                }

                $col++;
            }
            $row++;
            $i++;
        }
        $sheet->getActiveSheet()->getStyle('A6:K' . --$row)->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    ),
                ),
            )
        );
        $sheet->getActiveSheet()->getStyle('C7:C' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->getActiveSheet()->getStyle('I7:I' . $row)->getAlignment()->setWrapText(TRUE);
        $sheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $sheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $sheet->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $sheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $sheet->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $sheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $sheet->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $sheet->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $sheet->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $sheet->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $sheet->getActiveSheet()->getColumnDimension('K')->setWidth(15);


        $sheet_writer = PHPExcel_IOFactory::createWriter($sheet, 'Excel5');

        ob_start();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="report-chat.xls"');
        header('Cache-Control: max-age=0');
        $sheet_writer->save('php://output');
        $xlsData = ob_get_contents();
        ob_end_clean();
        $response = array(
            'name' => "report-chat.xls",
            'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData),
        );
        echo(json_encode($response));
        return TRUE;
    }

    public function export_log_import($record_list_data = array()) {
        $sheet = new PHPExcel();

        $ews = $sheet->getSheet(0);
        $ews->setTitle('Log import');
        $this->set_default_header($sheet, 'LỊCH SỬ NHẬP DỮ LIỆU ', 'A3:E4', 'E5');

        $data_excel['content'] = $record_list_data["record_list_data"];

        $data_excel['header'] = Array(
            "stt"         => "STT",
            "file_upload" => "File upload",
            "content"     => "Chi tiết",
            "created_on"  => "Thời gian",
            "username"    => "Người upload",
        );
        $col = 0;
        // In ra header
        foreach ($data_excel['header'] as $field => $value) {
            $sheet->getActiveSheet()->setCellValueByColumnAndRow($col, 6, $value);
            $col++;
        }
        $row = 7;
        //Fill color header
        $sheet->getActiveSheet()->getStyle('A6:E6')->applyFromArray(
            array(
                'fill'      => array(
                    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'DBE5A1'),
                ),
                'alignment' => array(
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ),
                'font'      => array(
                    'bold'  => TRUE,
                    'color' => array('rgb' => '000000'),
                    'name'  => 'Cambria',
                    'size'  => 10,
                ),
            )
        );
        $sheet->getActiveSheet()->getStyle('A6:E6')->getAlignment()->setWrapText(TRUE);
        $i = 1;
        foreach ($data_excel['content'] as $row_data) {
            $col = 0;
            $row_data->stt = $i;
            // In ra row
            foreach ($data_excel['header'] as $field_name => $value) {
                if ($field_name == 'time') {
                    $sheet->getActiveSheet()->setCellValueByColumnAndRow($col, $row, date('H:i:s', $row_data->time));
                } elseif ($field_name == 'date') {
                    $sheet->getActiveSheet()->setCellValueByColumnAndRow($col, $row, date('d/m/Y', $row_data->time));
                } else {
                    $sheet->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $row_data->$field_name);
                }

                $col++;
            }
            $row++;
            $i++;
        }
        $sheet->getActiveSheet()->getStyle('A6:E' . --$row)->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    ),
                ),
            )
        );
        $sheet->getActiveSheet()->getStyle('C7:C' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $sheet->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $sheet->getActiveSheet()->getColumnDimension('C')->setWidth(40);
        $sheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $sheet->getActiveSheet()->getColumnDimension('E')->setWidth(15);


        $sheet_writer = PHPExcel_IOFactory::createWriter($sheet, 'Excel5');

        ob_start();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="log-import.xls"');
        header('Cache-Control: max-age=0');
        $sheet_writer->save('php://output');
        $xlsData = ob_get_contents();
        ob_end_clean();
        $response = array(
            'name' => "log-import.xls",
            'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData),
        );
        echo(json_encode($response));
        return TRUE;
    }


    /**
     * init setting
     *
     * @param        $sheet
     * @param string $name_report
     * @param string $name_position
     * @param string $date_position
     */
    public function set_default_header($sheet, $name_report = '', $name_position = 'A3:G4', $date_position = 'G5') {
        //default style
        $default_style = array(
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size'  => 12,
                'name'  => 'Cambria',
            ),
        );
        $sheet->getActiveSheet()->getDefaultStyle()->applyFromArray($default_style);
        $sheet->getActiveSheet()->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        // info company
        $style_info = array(
            'font' => array(
                'bold' => TRUE,
            ),
        );
        $name_company = 'HERBALIFE';
        $name_company = empty($name_company) ? "HERBALIFE" : $name_company;
        $sheet->getActiveSheet()->getStyle('A1:A2')->applyFromArray($style_info);
        $sheet->getActiveSheet()->getCell('A1')->setValue($name_company);
        $sheet->getActiveSheet()->getCell('A2')->setValue('Thống kê lịch sử tương tác');
        $style_title = array(
            'font'      => array(
                'bold' => TRUE,
                'size' => 14,
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ));

        $first_name_position = explode(':', $name_position);
        $name_start = empty($first_name_position[0]) ? 'C3' : $first_name_position[0];
        $pValue = mb_strtoupper($name_report);
        $sheet->getActiveSheet()->getCell($name_start)->setValue($pValue);
        $sheet->getActiveSheet()->mergeCells($name_position);
        $sheet->getActiveSheet()->getStyle($name_position)->applyFromArray($style_title);
        $sheet->getActiveSheet()->getCell($date_position)->setValue('Thời gian xuất báo cáo: ' . date('d-m-Y'));
    }


    /**
     * get data excel import
     *
     * @param string $excel_file
     *
     * @return array
     */
    public function import_report_excel($excel_file = 'assets/report_default.xlsx') {
        $objPHPExcel = PHPExcel_IOFactory::load($excel_file);
        $dataSheet = $objPHPExcel->setActiveSheetIndex(0); // Set sheet sẽ được đọc dữ liệu
        $highestRow = $dataSheet->getHighestRow(); // Lấy số row lớn nhất trong sheet
        $data_return = array();
        for ($row = 2; $row <= $highestRow; $row++) { // For chạy từ 2 vì row 1 là title
            $item_data = array(
                'name'           => $dataSheet->getCellByColumnAndRow(0, $row)->getValue(), // lấy dữ liệu từng ô theo col và row
                'code'           => $dataSheet->getCellByColumnAndRow(1, $row)->getValue(),
                'status'         => $dataSheet->getCellByColumnAndRow(2, $row)->getValue(),
                'time_start'     => date('Y-m-d H:i:s', PHPExcel_Shared_Date::ExcelToPHP($dataSheet->getCellByColumnAndRow(3, $row)->getValue())),
                'time_end'       => date('Y-m-d H:i:s', PHPExcel_Shared_Date::ExcelToPHP($dataSheet->getCellByColumnAndRow(4, $row)->getValue())),
                'total_time'     => $dataSheet->getCellByColumnAndRow(5, $row)->getValue(),
                'point'          => $dataSheet->getCellByColumnAndRow(6, $row)->getValue(),
                'course_trainer' => $dataSheet->getCellByColumnAndRow(7, $row)->getValue(),
            );
            $data_return[] = (object)$item_data;
        }
        return $data_return;
    }

    /**
     * read data import user date
     *
     * @param string $excel_file
     *
     * @return array
     */
    public function import_sync_date_excel($excel_file = 'assets/import_default.xlsx') {
        $objPHPExcel = PHPExcel_IOFactory::load($excel_file);
        $dataSheet = $objPHPExcel->setActiveSheetIndex(0); // Set sheet sẽ được đọc dữ liệu
        $highestRow = $dataSheet->getHighestRow(); // Lấy số row lớn nhất trong sheet
        $data_return = array();
        for ($row = 2; $row <= $highestRow; $row++) { // For chạy từ 2 vì row 1 là title
            $item_data = array(
                'UserLogonID' => $dataSheet->getCellByColumnAndRow(0, $row)->getValue(), // lấy dữ liệu từng ô theo col và row
                'ImportType'  => $dataSheet->getCellByColumnAndRow(1, $row)->getValue(),
                'valueDate'   => date('Y-m-d H:i:s', PHPExcel_Shared_Date::ExcelToPHP($dataSheet->getCellByColumnAndRow(2, $row)->getValue())),
            );
            $data_return[] = (object)$item_data;
        }
        return $data_return;
    }

}
