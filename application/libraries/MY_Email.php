<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by IntelliJ IDEA.
 * User: miunh_nim
 * Date: 12-Feb-19
 * Time: 4:13 PM
 */

/**
 * CodeIgniter PHPMailer Class
 *
 * This class enables SMTP email with PHPMailer
 *
 * @category    Libraries
 * @author      CodexWorld
 * @link        https://www.codexworld.com
 */

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

class MY_Email {
	protected $_ci;

	public function __construct() {
		log_message('Debug', 'PHPMailer class is loaded.');
        $this->_ci = &get_instance();
//        $this->_ci->load->model('M_log_send_email', 'log_send_email');
	}

	/**
	 * send email
	 *
	 * @param array $data_email
	 *
	 * @return bool
	 */
	public function send_email($data_email = array()) {
		$from_email = 'okkemdau@gmail.com';
		$from_password = 'hiflyntiuspkjmzs';
//		$from_email = 'huanluyenhatgiong.herbalife@gmail.com';
//		$from_password = 'HLFVN@123';
		$mail_header = 'Ebook';
		require_once APPPATH.'third_party/PHPMailer/Exception.php';
		require_once APPPATH.'third_party/PHPMailer/PHPMailer.php';
		require_once APPPATH.'third_party/PHPMailer/SMTP.php';
		// Include PHPMailer library files
		if (empty($data_email['to_email'])) {
			echo "Vui lòng nhập đủ thông tin";
			return FALSE;
		}
		$data_email['from_email'] = $from_email;
		$email = $from_email;
		$password = $from_password;
		$to_id = empty($data_email['to_email']) ? '' : $data_email['to_email'];
		$message = empty($data_email['message']) ? '' : $data_email['message'];
		$mail = new PHPMailer();
		$mail->IsSMTP(); // we are going to use SMTP
		$mail->SMTPAuth = TRUE; // enabled SMTP authentication
//		$mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
//		$mail->Host = "smtp.gmail.com";      // setting GMail as our SMTP server
//		$mail->Port = 465;
//		$mail->SMTPSecure = "tls";  // prefix for secure protocol to connect to the server
		$mail->Host = "smtp.gmail.com";      // setting GMail as our SMTP server
		$mail->Port = 587;
		$mail->Username = $email;
		$mail->Password = $password;
		$mail->CharSet = 'utf-8';
		$header = $mail_header;
		$mail->SetFrom($email, empty($header) ? 'Herbalife' : $header);
		$mail->isHTML(TRUE);
		$mail->Subject = empty($data_email['subject']) ? '' : $data_email['subject'];
		$mail->Body = $message;
		$mail->AddAddress($to_id, "Receiver");// Who is addressed the email to
		if (!$mail->Send()) {
			$error = $mail->ErrorInfo;
			return $error;
		} else {
//			$this->save_log_email($data_email);
			return TRUE;
		}
	}

	/**
	 * save log send email
	 *
	 * @param array $data_email
	 */
	public function save_log_email($data_email = array()) {
		$this->_ci->load->model('M_log_send_email', 'log_email');
		$data_log = array(
			'to_email'   => $data_email['to_email'],
			'subject'    => $data_email['subject'],
			'content'    => $data_email['message'],
			'created_on' => date('Y-m-d H:i:s'),
			'status'     => 'sent',
		);
		$this->_ci->log_email->insert($data_log);
	}
}
