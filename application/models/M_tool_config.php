<?php

class M_tool_config extends MY_model {

	/**
	 * @var string
	 */
	protected $_table = 'gk_tool_config';

	/**
	 * @var bool
	 */
	protected $soft_delete = true;

	protected $limit = 50;

	/**
	 * @var array
	 */
	public $schema =
		[
			"tool_name" => [
				"field"    => "tool_name",
				"db_field" => "m.tool_name",
			],
		];

	/**
	 * M_user constructor.
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * @param $data_get
	 *
	 * @return array
	 */
	public function get_list($data_get) {
		$page = empty($data_get['page']) ? 1 : $data_get['page'];
		$limit = $this->limit;
		$offset = ($page - 1) * $limit;
		$this->db->select('*');
		$this->db->from($this->_table);
		$this->db->limit($limit, $offset);
		$this->db->where('deleted', '0');
		$query = $this->db->get()->result_array();
		return $query;
	}
}
