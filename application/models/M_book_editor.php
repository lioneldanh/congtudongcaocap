<?php

class M_book_editor extends MY_model {

	/**
	 * @var string
	 */
	protected $_table = 'gk_book_editor';

	/**
	 * @var string
	 */
	protected $_table_alias = 'gk_book_editor_chap';

	/**
	 * @var string
	 */
	protected $_table_editor_toc = 'gk_book_editor_toc';

	/**
	 * @var bool
	 */
	protected $soft_delete = true;

	/**
	 * @var array
	 */
	public $schema =
		[
			"id" => [
				"field"    => "id",
				"db_field" => "m.id",
			],
		];

	/**
	 * M_user constructor.
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * get content proofread
	 *
	 * @param $book_id
	 *
	 * @return array
	 */
	public function get_content_proofread($book_id) {
		$this->db->select(
			$this->_table_alias . '.chap_data,'.
			$this->_table_alias . '.chap_name'

		);
		$this->db->from($this->_table);
		$this->db->join($this->_table_alias, $this->_table . '.id = ' . $this->_table_alias . '.book_editor_id');
		$this->db->join($this->_table_editor_toc, $this->_table_alias . '.id = ' . $this->_table_editor_toc . '.chap_id');
		if (!empty($book_id)) {
			$this->db->where($this->_table . '.book_id', $book_id);
		}
		$this->db->where($this->_table_editor_toc . '.allow_preview', '1');
		$query = $this->db->get()->result_array();
		return $query;
	}
}
