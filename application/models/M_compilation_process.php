<?php

class M_compilation_process extends MY_model {

	/**
	 * @var string
	 */
	protected $_table = 'gk_compilation_process';

	/**
	 * @var bool
	 */
	protected $soft_delete = true;

	/**
	 * @var array
	 */
	public $schema =
		[
			"id" => [
				"field"    => "id",
				"db_field" => "m.id",
			],
		];

	/**
	 * M_user constructor.
	 */
	public function __construct() {
		parent::__construct();
	}
}
