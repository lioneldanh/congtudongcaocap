<?php

class M_customer_book extends MY_model {

	/**
	 * @var string
	 */
	protected $_table = 'gk_customer_book';

	/**
	 * @var string
	 */
	protected $_table_customer_log = 'gk_customer_book_log';

	/**
	 * @var string
	 */
	protected $_table_book = 'gk_book';

	/**
	 * @var bool
	 */
	protected $soft_delete = true;

	protected $limit_default = 50;

	/**
	 * @var array
	 */
	public $schema =
		[
			"id" => [
				"field"    => "id",
				"db_field" => "m.id",
			],
		];

	/**
	 * M_user constructor.
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * @param $data_get;
	 *
	 * @return array
	 */
	public function get_list_book_viewed($data_get) {
		$offset = empty($data_get['page']) ? 0 : ($data_get['page'] - 1) * 50;
		$customer_id =  empty($data_get['customer_id']) ? '' : $data_get['customer_id'];
		$this->db->select($this->_table . '.book_id');
		$this->db->from($this->_table);
		$this->db->join($this->_table_customer_log, $this->_table . '.id = ' . $this->_table_customer_log . '.customer_book_id');
		$this->db->where($this->_table.'.customer_id', $customer_id);
		$this->db->where($this->_table_customer_log.'.deleted', 0);
		$this->db->limit($this->limit_default, $offset);
		$query = $this->db->get()->result_array();
		return $query;
	}

	/**
	 * @param $data_get
	 *
	 * @return array
	 */
	public function get_book_filter($data_get) {
		$page  = empty($data_get['page']) ? 1 : $data_get['page'];
		$limit = $this->limit_default;
		$offset = ($page - 1) * $limit + 1;
		$customer_id = $data_get['customer_id'];
		$type = empty($data_get['type']) ? '' : $data_get['type'];
		$customer_book_type = empty($data_get['customer_book_type']) ? '' : $data_get['customer_book_type'];
 		$this->db->select($this->_table . '.book_id');
		$this->db->from($this->_table);
		$this->db->join($this->_table_book, $this->_table . '.book_id = ' . $this->_table_book . '.id');
		$this->db->where('customer_id', $customer_id);
		if (!empty($type)) {
			$this->db->where('book_type', $type);
		}
		if (!empty($customer_book_type)) {
			$this->db->where('status', 1);
		}
		$this->db->limit($limit, $offset);
		$query = $this->db->get()->result_array();
		return $query;
	}
}
