<?php

class M_news extends MY_model {

	/**
	 * @var string
	 */
	protected $_table = 'gk_news';

	/**
	 * @var bool
	 */
	protected $soft_delete = true;

	/**
	 * @var int
	 */
	protected $_default_limit = 50;

	/**
	 * @var array
	 */
	public $schema =
		[
			"id" => [
				"field"    => "id",
				"db_field" => "m.tab_id",
			],
		];

	/**
	 * M_news constructor.
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Get News
	 *
	 * @param $page
	 *
	 * @return array
	 */
	public function get_news($page) {
		$offset = ($page - 1) * $this->_default_limit;
		$this->db->select('*');
		$this->db->from($this->_table);
		$this->db->where('deleted', 0);
		$this->db->limit($this->_default_limit, $offset);
		$query = $this->db->get()->result_array();
		return $query;
	}
}
