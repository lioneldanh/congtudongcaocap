<?php

class M_file_library extends MY_model {

	/**
	 * @var string
	 */
	protected $_table = 'gk_file_library';

	/**
	 * @var bool
	 */
	protected $soft_delete = true;

	/**
	 * @var array
	 */
	public $schema =
		[
			"id" => [
				"field"    => "id",
				"db_field" => "m.tab_id",
			],
		];

	/**
	 * M_news constructor.
	 */
	public function __construct() {
		parent::__construct();
	}
}
