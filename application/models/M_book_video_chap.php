<?php

class M_book_video_chap extends MY_model {

	/**
	 * @var string
	 */
	protected $_table = 'gk_book_video_chap';

	/**
	 * @var bool
	 */
	protected $soft_delete = true;

	/**
	 * @var array
	 */
	public $schema =
		[
			"id" => [
				"field"    => "id",
				"db_field" => "m.id",
			],
		];

	/**
	 * M_user constructor.
	 */
	public function __construct() {
		parent::__construct();
	}
}
