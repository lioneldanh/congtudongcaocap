<?php

class M_book_audio extends MY_model {

	/**
	 * @var string
	 */
	protected $_table = 'gk_book_audio';

	/**
	 * @var string
	 */
	protected $_table_alias = 'gk_book_audio_chap';

	/**
	 * @var string
	 */
	protected $_table_audio_toc = 'gk_book_audio_toc';

	/**
	 * @var bool
	 */
	protected $soft_delete = true;

	/**
	 * @var array
	 */
	public $schema =
		[
			"id" => [
				"field"    => "id",
				"db_field" => "m.id",
			],
		];

	/**
	 * M_user constructor.
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * get content proofread
	 *
	 * @param $book_id
	 *
	 * @return array
	 */
	public function get_content_proofread($book_id) {
		$this->db->select(
			$this->_table_alias . '.audio_file'

		);
		$this->db->from($this->_table);
		$this->db->join($this->_table_alias, $this->_table . '.id = ' . $this->_table_alias . '.book_audio_id');
		$this->db->join($this->_table_audio_toc, $this->_table_alias . '.id = ' . $this->_table_audio_toc . '.chap_id');
		if (!empty($book_id)) {
			$this->db->where($this->_table . '.book_id', $book_id);
		}
		$this->db->where($this->_table_audio_toc . '.allow_preview', '1');
		$query = $this->db->get()->result_array();
		return $query;
	}
}
