<?php

class M_book extends MY_model {

	/**
	 * Table gk_book
	 *
	 * @var string
	 */
	protected $_table = 'gk_book';

	/**
	 * Table gk_book
	 *
	 * @var string
	 */
	protected $_table_categories = 'gk_book_categories';

	/**
	 * Table gk_book_categories
	 *
	 * @var string
	 */
	protected $_table_format = 'gk_book_format';

	/**
	 * Table gk_book_format
	 *
	 * @var string
	 */
	protected $_table_customer_book = 'gk_customer_book';

	/**
	 * Table gk_recovered_books
	 *
	 * @var string
	 */
	protected $_table_recovered_book = 'gk_recovered_books';

	protected $_limit_default = 50;

	/**
	 * @var bool
	 */
	protected $soft_delete = true;

	/**
	 * @var array
	 */
	public $schema =
		[
			"book_name" => [
				"field"    => "book_name",
				"db_field" => "m.book_name",
			]
		];

	/**
	 * M_user constructor.
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Get list book used
	 *
	 * @param $data_post
	 *
	 * @return array
	 */
	public function get_list_book_used($data_post) {
		$types = empty($data_post['type']) ? '' : $data_post['type'];
		$categories = empty($data_post['categories']) ? '' : $data_post['categories'];
		$limit = empty($data_post['limit']) ? 50 : $data_post['limit'];
		$offset = empty($data_post['offset']) ? 0 : $data_post['offset'];
		$customer_id =  empty($data_post['customer_id']) ? '' : $data_post['customer_id'];
		$this->db->select(
			$this->_table . '.*'
		);
		$this->db->from($this->_table);
		$this->db->join($this->_table_categories, $this->_table . '.id = ' . $this->_table_categories . '.book_id');
		$this->db->join($this->_table_format, $this->_table . '.id = ' . $this->_table_format . '.book_id');
		$this->db->join($this->_table_customer_book, $this->_table . '.id = ' . $this->_table_customer_book . '.book_id');
		$this->db->where($this->_table_customer_book . '.status', '1');
		$this->db->where($this->_table_customer_book . '.customer_id', $customer_id);
		$this->db->where($this->_table . '.published', 1);
		if (!empty($types)) {
			foreach ($types as $item) {
				$this->db->where($this->_table_format . '.type', $item);
			}
		}
		if (!empty($categories)) {
			foreach ($categories as $item) {
				$this->db->where($this->_table_categories . '.category_id', $item);
			}
		}
		$this->db->limit($limit, $offset);
		$query = $this->db->get()->result_array();
		return $query;
	}

	/**
	 * Get list book by type
	 *
	 * @param $data_post
	 *
	 * @return array
	 */
	public function get_list_book($data_post) {
		$type = empty($data_post['type']) ? '' : $data_post['type'];
		$limit = empty($data_post['limit']) ? 50 : $data_post['limit'];
		$offset = empty($data_post['offset']) ? 0 : $data_post['offset'];
		$this->db->select(
			$this->_table . '.*'
		);
		$this->db->from($this->_table);
		$this->db->join($this->_table_categories, $this->_table . '.id = ' . $this->_table_categories . '.book_id');
		$this->db->join($this->_table_format, $this->_table . '.id = ' . $this->_table_format . '.book_id');
		$this->db->where($this->_table . '.published', 1);
		if (!empty($type)) {
			$this->db->where($this->_table_format . '.type', $type);
		}
		$this->db->limit($limit, $offset);
		$query = $this->db->get()->result_array();
		return $query;
	}

	/**
	 * Get list book download
	 *
	 * @param $data_post
	 *
	 * @return array
	 */
	public function get_list_book_downloaded($data_post) {
		$customer_id = empty($data_post['customer_id']) ? '' : $data_post['customer_id'];
		$limit = empty($data_post['limit']) ? 50 : $data_post['limit'];
		$offset = empty($data_post['offset']) ? 0 : $data_post['offset'];
		$this->db->select(
			$this->_table . '.*'
		);
		$this->db->join($this->_table_customer_book, $this->_table . '.id = ' . $this->_table_customer_book . '.book_id');
		$this->db->where($this->_table_customer_book . '.customer_id', $customer_id);
		$this->db->limit($limit, $offset);
		$query = $this->db->get()->result_array();
		return $query;
	}

	/**
	 * Search Book
	 *
	 * @param $data_get
	 *
	 * @return array
	 */
	public function search_book($data_get) {
		$key_search = empty($data_get['key_search']) ? '' : $data_get['key_search'];
		$type = empty($data_get['type']) ? '' : $data_get['type'];
		$limit = $this->_limit_default;
		$offset = empty($data_get['page']) ? 1 : ($data_get['page'] - 1) * $limit + 1;
		$this->db->select($this->_table.'.id as book_id');
		$this->db->from($this->_table);
		$this->db->join($this->_table_format, $this->_table . '.id = ' . $this->_table_format . '.book_id');
		$this->db->where($this->_table . '.published', 1);
		if (!empty($type)) {
			$this->db->where($this->_table_format . '.type', $type);
		}
		$this->db->like($this->_table.'.book_name', $key_search);
		$this->db->limit($limit, $offset);
		$query = $this->db->get()->result_array();
		return $query;
	}

	/**
	 * @param $type
	 *
	 * @return array
	 */
	public function get_suggestions($type) {
		$limit = empty($data_post['limit']) ? 50 : $data_post['limit'];
		$offset = empty($data_post['offset']) ? 0 : $data_post['offset'];
		$this->db->select($this->_table . '.id AS book_id');
		$this->db->from($this->_table);
		$this->db->join($this->_table_format, $this->_table . '.id = ' . $this->_table_format . '.book_id');
		$this->db->where($this->_table . '.published', 1);
		if (!empty($type)) {
			$this->db->where($this->_table_format . '.type', $type);
		}
		$this->db->limit($limit, $offset);
		$query = $this->db->get()->result_array();
		return $query;
	}

	/**
	 * @param $data
	 *
	 * @param $limit
	 *
	 * @return array
	 */
	public function get_book_deleted($data, $limit = '') {
		if ($data) {
			$this->db->select(
				$this->_table . '.*'
			);
			$this->db->from($this->_table);
			$this->db->where($this->_table . '.deleted =', 1);

			if (!empty($data['search']['search_all'])) {
				$this->db->like($this->_table . '.book_name', $data['search']['search_all']);
			}
			if (!empty($data['search']['search_key'])) {
				$this->db->like($this->_table . '.book_name', $data['search']['search_all']);
			}
			if (!empty($data['order_by']['id']) && $data['order_by']['id'] == 'asc') {
				$this->db->order_by($this->_table . ".id", "asc");
			} else {
				$this->db->order_by($this->_table . ".id", "desc");
			}
			if ($limit !== 'all') {
				if (!empty($data['limit'])) {
					$offset = 0;
					if (!empty($data['offset'])) {
						$offset = $data['offset'];
					}
					$this->db->limit($data['limit'], $offset);
				}
			}
			$query = $this->db->get()->result_array();
			return $query;
		} else {
			return [];
		}
	}

	/**
	 * @param $book_id
	 * @return array
	 */
	public function get_book_by_id($book_id) {
		$this->db->select($this->_table . '.*');
		$this->db->from($this->_table);
		$this->db->where($this->_table . '.deleted =', 1);
		$this->db->where($this->_table . '.id =', $book_id);
		$query = $this->db->get()->row_array();
		return $query;
	}

	/**
	 * @param $book_id
	 *
	 * @return mixed
	 */
	public function delete_book($book_id) {
		$this->db->where($this->_table . '.deleted =', 1);
		$this->db->where($this->_table . '.id =', $book_id);
		$result = $this->db->delete($this->_table);
		return $result;
	}

	/**
	 * @param $data
	 *
	 * @return array
	 */
	public function get_list_book_recovered($data) {
		$this->db->select(
			$this->_table . '.*'
		);
		$this->db->from($this->_table);
		$this->db->join($this->_table_recovered_book, $this->_table . '.id = ' . $this->_table_recovered_book . '.book_id');
		$this->db->where($this->_table_recovered_book . '.deleted', 0);
		$this->db->where($this->_table . '.deleted =', 0);

		if (!empty($data['search']['search_all'])) {
			$this->db->like($this->_table . '.book_name', $data['search']['search_all']);
		}
		if (!empty($data['search']['search_key'])) {
			$this->db->like($this->_table . '.book_name', $data['search']['search_all']);
		}
		if (!empty($data['order_by']['id']) && $data['order_by']['id'] == 'asc') {
			$this->db->order_by($this->_table . ".id", "asc");
		} else {
			$this->db->order_by($this->_table . ".id", "desc");
		}
		if (!empty($data['limit'])) {
			$offset = 0;
			if (!empty($data['offset'])) {
				$offset = $data['offset'];
			}
			$this->db->limit($data['limit'], $offset);
		}
		$query = $this->db->get()->result_array();
		return $query;
	}

	/**
	 * @return array
	 */
	public function get_book_by_type() {
		$type = empty($data_post['type']) ? '' : $data_post['type'];
		$limit = empty($data_post['limit']) ? 50 : $data_post['limit'];
		$offset = empty($data_post['offset']) ? 0 : $data_post['offset'];
		$this->db->select(
			$this->_table . '.*'
		);
		$this->db->from($this->_table);
		$this->db->join($this->_table_categories, $this->_table . '.id = ' . $this->_table_categories . '.book_id');
		$this->db->join($this->_table_format, $this->_table . '.id = ' . $this->_table_format . '.book_id');
		$this->db->where($this->_table . '.published', 1);
		if (!empty($type)) {
			$this->db->where($this->_table_format . '.type', $type);
		}
		$this->db->limit($limit, $offset);
		$query = $this->db->get()->result_array();
		return $query;
	}

	/**
	 * @param $book_id
	 * @return array
	 */
	public function get_book_by_book_id($book_id) {
		$this->db->select($this->_table . '.*');
		$this->db->from($this->_table);
		$this->db->where($this->_table . '.id =', $book_id);
		$query = $this->db->get()->row_array();
		return $query;
	}
}
