<?php

class M_category extends MY_model {

	/**
	 * @var string
	 */
	protected $_table = 'gk_category';

	/**
	 * @var bool
	 */
	protected $soft_delete = true;

	/**
	 * @var array
	 */
	public $schema =
		[
			"category_name_vn" => [
				"field"    => "category_name_vn",
				"db_field" => "m.category_name_vn",
			],
			"category_name_en" => [
				"field"    => "category_name_en",
				"db_field" => "m.category_name_en",
			]
		];

	/**
	 * M_user constructor.
	 */
	public function __construct() {
		parent::__construct();
	}
}
