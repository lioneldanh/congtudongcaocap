<?php

class M_customer_book_log extends MY_model {

	/**
	 * @var string
	 */
	protected $_table = 'gk_customer_book_log';

	/**
	 * @var bool
	 */
	protected $soft_delete = true;

	/**
	 * @var array
	 */
	public $schema =
		[
			"id" => [
				"field"    => "id",
				"db_field" => "m.tab_id",
			],
		];

	/**
	 * M_news constructor.
	 */
	public function __construct() {
		parent::__construct();
	}

}
