<?php

class M_user extends MY_model {

	/**
	 * @var string
	 */
	protected $_table = 'user';

	/**
	 * @var bool
	 */
	protected $soft_delete = true;

	/**
	 * @var array
	 */
	public $schema =
		[
			"id" => [
				"field"    => "id",
				"db_field" => "m.tab_id",
			],
		];

	/**
	 * M_user constructor.
	 */
	public function __construct() {
		parent::__construct();
	}


	/**
	 * Get all users in DB
	 * @param null
	 * @return array
	 */
	function checkUser( $userInfo ){
		$a_User	=	$this->db->select()
			->where('username', $userInfo['username'])
			->where('password', $userInfo['password'])
			->get($this->_table)
			->row_array();
		if(!empty($a_User)){
			return $a_User;
		} else {
			return false;
		}
	}

	/**
	 * Get User By Id
	 * @param $id
	 * @return array|bool
	 */
	function getUserById($id) {
		$a_User	=	$this->db->select()
			->where('id', $id)
			->get($this->_table)
			->row_array();
		if(!empty($a_User)){
			return $a_User;
		} else {
			return false;
		}
	}

	/**
	 * Update user by id
	 * @param $data
	 * @return bool
	 */
	function editInfoById($data) {
		$where = 'id = '.$data['id'];
		$query = $this->db->update($this->_table, $data, $where);
		return $query;
	}

	/**
	 * Check Email
	 * @param $email
	 * @return array|bool
	 */
	function checkEmail($email) {
        $this->db->where('email',$email);
        $a_User = $this->db->get($this->_table);
        if ($a_User->num_rows() > 0){
            return $a_User->row_array();
        }
        else{
            return false;
        }
	}

	/**
	 * Check username
	 * @param $username
	 * @return array|bool
	 */
	function checkUsername($username) {
		$a_User	= $this->db->select()
			->where('username', $username)
			->get($this->_table)
			->row_array();
		if(!empty($a_User)){
			return $a_User;
		} else {
			return false;
		}
	}
}
