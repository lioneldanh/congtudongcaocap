<?php

class M_book_video extends MY_model {

	/**
	 * @var string
	 */
	protected $_table = 'gk_book_video';

	/**
	 * @var string
	 */
	protected $_table_alias = 'gk_book_video_chap';

	/**
	 * @var string
	 */
	protected $_table_video_toc = 'gk_book_video_toc';

	/**
	 * @var bool
	 */
	protected $soft_delete = true;

	/**
	 * @var array
	 */
	public $schema =
		[
			"id" => [
				"field"    => "id",
				"db_field" => "m.id",
			],
		];

	/**
	 * M_user constructor.
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * get content proofread
	 *
	 * @param $book_id
	 *
	 * @return array
	 */
	public function get_content_proofread($book_id) {
		$this->db->select(
			$this->_table_alias . '.video_file'

		);
		$this->db->from($this->_table);
		$this->db->join($this->_table_alias, $this->_table . '.id = ' . $this->_table_alias . '.book_video_id');
		$this->db->join($this->_table_video_toc, $this->_table_alias . '.id = ' . $this->_table_video_toc . '.chap_id');
		if (!empty($book_id)) {
			$this->db->where($this->_table . '.book_id', $book_id);
		}
		$this->db->where($this->_table_video_toc . '.allow_preview', '1');
		$query = $this->db->get()->result_array();
		return $query;
	}
}
