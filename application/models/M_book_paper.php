<?php

class M_book_paper extends MY_model {

	/**
	 * @var string
	 */
	protected $_table = 'gk_book_paper';

	/**
	 * @var array
	 */
	public $schema =
		[
			"id" => [
				"field"    => "id",
				"db_field" => "m.id",
			],
		];

	/**
	 * M_user constructor.
	 */
	public function __construct() {
		parent::__construct();
	}
}
