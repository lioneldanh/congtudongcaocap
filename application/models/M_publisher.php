<?php

class M_publisher extends MY_model {

	/**
	 * @var string
	 */
	protected $_table = 'gk_publisher';

	/**
	 * @var bool
	 */
	protected $soft_delete = true;

	/**
	 * @var array
	 */
	public $schema =
		[
			"publisher_name" => [
				"field"    => "publisher_name",
				"db_field" => "m.publisher_name",
			],
		];

	/**
	 * M_user constructor.
	 */
	public function __construct() {
		parent::__construct();
	}
}
