<?php

class M_author extends MY_model {

	/**
	 * @var string
	 */
	protected $_table = 'gk_author';

	/**
	 * @var bool
	 */
	protected $soft_delete = true;

	/**
	 * @var array
	 */
	public $schema =
		[
			"author_name" => [
				"field"    => "author_name",
				"db_field" => "m.author_name",
			],
		];

	/**
	 * M_user constructor.
	 */
	public function __construct() {
		parent::__construct();
	}
}
