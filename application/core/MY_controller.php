<?php

/**
 * Created by IntelliJ IDEA.
 * User: miunh_nim
 * Date: 11-Feb-19
 * Time: 2:54 PM
 */

/**
 * Class MY_controller
 *
 * @property session session
 */
class MY_controller extends CI_Controller {
	public $_model = 'M_user';
	public $_view_folder = '';
	public $_object_name = 'tài khoản';
	public $_controller = 'user';
	private $_list_js_head = [];
	private $_list_js_footer = [];
	private $_list_css = [];
	public $_limit_default = 10;

	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');

		// for update new migration
		$this->load->library('migration');
		if ($this->migration->current() === FALSE) {
			show_error($this->migration->error_string());
		}
		$this->check_logged();

		$this->load->model($this->_model, 'model');
	}

	public function check_logged() {
		if (empty($this->session->userdata('user_id'))) {
			redirect(site_url('login'));
		}
	}

	public function index() {
		$data = [];
		$controller = $this->router->fetch_class();
		$data["url_ajax_data_table"] = site_url($controller . '/e_ajax_table');
		// get default limit
		$data["default_limit"] = $this->_limit_default;
		$data["default_offset"] = 0;
		$data['content_header'] = 'Quản lý ' . $this->_object_name;
		$data['object_name'] = $this->_object_name;
		$data['_controller'] = $this->_controller;
		$data['data_filter'] = $this->get_filter_setting();
		if (file_exists('application/views/' . $this->_view_folder . '/content.php')) {
			$content = $this->load->view($this->_view_folder . '/content', $data, TRUE);
		} else {
			$content = $this->load->view("base/content", $data, TRUE);
		}
		$this->load_view($content);
	}

	public function e_ajax_table($data = []) {
		if (!$this->input->is_ajax_request()) return FALSE;
		$data_condition = $this->input->post();
		$list_condition = $this->_process_condition($data_condition);

		// set condition
		$order_by = isset($list_condition["order_by"]) ? $list_condition["order_by"] : '';
		$where_condition = isset($list_condition["where"]) ? $list_condition["where"] : [];
		$wherein_condition = isset($list_condition["wherein"]) ? $list_condition["wherein"] : [];
		$like_condition = isset($list_condition["like"]) ? $list_condition["like"] : [];
		$data_paging = $this->get_paging_data($list_condition);
		$limit = $data_paging["limit"];
		$offset = $data_paging["post"];
		$record_list_data = $this->model->get_list_data($where_condition, $wherein_condition, $like_condition, $limit, $offset, $order_by);
		$count_record_list_data = $this->model->get_list_data_count($where_condition, $wherein_condition, $like_condition);
		$data = array_merge($data, $data_paging);
		$data["record_list_data"] = $record_list_data;
		$data["count_record_list_data"] = $count_record_list_data;
		// get url for href
		if (file_exists('application/views/' . $this->_view_folder . '/manage_table.php')) {
			$content_table = $this->load->view($this->_view_folder . '/manage_table', $data, TRUE);
		} else {
			$content_table = $this->load->view("base/manage_table", $data, TRUE);
		}
		$data_return = [
			"status"      => 1,
			"status_code" => "SUCCESS",
			"count"       => $data["count_record_list_data"],
			"html"        => $content_table,
		];
		echo json_encode($data_return);
		return TRUE;
	}

	/**
	 * Process data condition
	 */
	protected function _process_condition($data_condition) {
		if (empty($data_condition)) {
			$data_return = [
				"limit"  => $this->_limit_default,
				"offset" => 0,
				"paging" => 1,
			];
		} else {
			$data_return = [];
			$data_return["limit"] = isset($data_condition["limit"]) ? $data_condition["limit"] : $this->_limit_default;
			$data_return["offset"] = isset($data_condition["offset"]) ? $data_condition["offset"] : 0;
			$data_return["page"] = isset($data_condition["page"]) ? $data_condition["page"] : 0;
			// filter
			$filter_condition_tmp = isset($data_condition["filter"]) ? $data_condition["filter"] : [];
			$where_condition = [];
			$wherein_condition = [];
			if (isset($filter_condition_tmp["from"]) && isset($filter_condition_tmp["to"])) {
				if ($filter_condition_tmp["from"]) $where_condition["m.created_on >="] = date("Y-m-d H:i:s", strtotime($filter_condition_tmp["from"]));
				if ($filter_condition_tmp["to"]) $where_condition["m.created_on <="] = date("Y-m-d H:i:s", strtotime($filter_condition_tmp["to"]));
				unset($filter_condition_tmp["from"]);
				unset($filter_condition_tmp["to"]);
			}

			if (!empty($filter_condition_tmp) && is_array($filter_condition_tmp)) {
				foreach ($filter_condition_tmp as $key => $value) {
					if (!empty($value) && is_array($value)) {
						if (strpos($key, ".") !== FALSE) {
							$wherein_condition[$key] = $value;
						} else {
							$wherein_condition['m.' . $key] = $value;
						}
					} else {
						if (!empty($value)) {
							if (strpos($key, ".") !== FALSE) {
								$where_condition[$key] = $value;
							} else {
								$where_condition['m.' . $key] = $value;
							}
						}
					}
				}
			}

			$data_return["where"] = $where_condition;
			$data_return["wherein"] = $wherein_condition;
			// search - todo
			$search_condition_tmp = isset($data_condition["search"]["search_all"]) ? $data_condition["search"]["search_all"] : [];
			$start_key = isset($data_condition["search"]["search_key"]) ? $data_condition["search"]["search_key"] : '';
			$search_condition = [];
			$like_after = [];
			$search_condition_value = NULL;
			if (!empty($search_condition_tmp)) {
				foreach ($this->_get_column_search() as $col) {
					$search_condition[$col] = $search_condition_tmp;
				}
			}

			if (!empty($start_key)) {
				foreach ($this->_get_column_search() as $col) {
					$like_after[$col] = $start_key;
				}
			}
			$data_return["like"]["or"] = $search_condition;
			$data_return["like"]["after"] = $like_after;
			// order by
			$data_return['order_by'] = isset($data_condition["order_by"]) ? $data_condition["order_by"] : '';
		}
		return $data_return;
	}

	protected function _get_column_search() {
		$schema = $this->model->schema;
		$data_return = [];
		foreach ($schema as $schema_value) {
			$data_return[] = $schema_value["db_field"];
		}
		return $data_return;
	}

	/**
	 * @param $list_condition
	 *
	 * @return array
	 */
	protected function get_paging_data($list_condition, $model = NULL) {
		$data = [];
		$limit = isset($list_condition["limit"]) ? $list_condition["limit"] : $this->_limit_default;
		$offset = isset($list_condition["offset"]) ? $list_condition["offset"] : $this->_limit_default;
		$order_by = isset($list_condition["order_by"]) ? $list_condition["order_by"] : '';

		$where_condition = isset($list_condition["where"]) ? $list_condition["where"] : [];
		$wherein_condition = isset($list_condition["wherein"]) ? $list_condition["wherein"] : [];
		$like_condition = isset($list_condition["like"]) ? $list_condition["like"] : [];

		$current_page = intval(isset($list_condition["page"]) ? $list_condition["page"] : 1);
		if ($limit < 0) {
			$limit = 0;
		}
		/* If change limit or change filter: reset page to 1 */
		$old_condition = $this->session->userdata('table_manager_condition');
		$old_limit = intval(isset($old_condition["limit"]) ? $old_condition["limit"] : $this->_limit_default);
		$old_filter = isset($old_condition["filter"]) ? $old_condition["filter"] : [];
		ksort($old_filter);
		if (($limit != $old_limit)) {
			$current_page = 1;
		}
		//Update session condition after reset page
		$this->session->set_userdata('table_manager_condition', $list_condition);
		$post = ($current_page - 1) * $limit;
		if ($post < 0) {
			$post = 0;
			$current_page = 1;
		}
		if (!empty($model)) {
			$total_item = $model->get_list_data_count($where_condition, $wherein_condition, $like_condition);
		} else {
			$total_item = $this->model->get_list_data_count($where_condition, $wherein_condition, $like_condition);
		}
		if ($limit != 0) {
			$total_page = (int)($total_item / $limit);
		} else {
			$total_page = 0;
		}

		if (($total_page * $limit) < $total_item) {
			$total_page += 1;
		}

		$link = "#";
		$data["paging"] = $this->_get_paging($total_page, $current_page, 5, $link);
		$data["from"] = $post + 1;
		$data["to"] = $post + $limit;
		if ($data["to"] > $total_item || $limit == 0) {
			$data["to"] = $total_item;
		}
		$data["limit"] = $limit;
		$data["post"] = $post;
		$data["total"] = $total_item;
		$data["paging_detail"] = $this->load->view("base/paging_detail", $data, TRUE);
		return $data;
	}

	/**
	 * Get html for pagination
	 *
	 * @param int    $total   = total page
	 * @param int    $current = current page
	 * @param int    $display = page show on link
	 * @param string $link    = origin link
	 * @param string $key     = page key in get
	 *
	 * @return string html of pagination
	 */
	protected function _get_paging($total, $current, $display, $link, $key = 1) {
		$data["total_page"] = $total;
		$data["current_page"] = $current;
		$data["page_link_display"] = $display;
		$data["link"] = $link;
		$data["key"] = $key;
		return $this->load->view("base/paging", $data, TRUE);
	}

	protected function get_filter_setting() {
		$default_data = [
			'1' => 'group1',
			'2' => 'group2',
		];
		$data_filter = [
			[
				'label'       => 'Chủ đề',
				'db_field'    => 'username',
				'data_select' => json_encode($default_data),
			],
			[
				'label'       => 'Danh mục',
				'db_field'    => 'username',
				'data_select' => json_encode($default_data),
			],

		];
		return $data_filter;
	}

	/**
	 * get menu
	 *
	 * @return array
	 */
	protected function get_menu() {
		$data_menu = [
			[
				'label'      => 'Dashboard',
				'icon'       => 'fa-dashboard',
				'controller' => 'home',
			],
			[
				'label'      => 'Quản lý tổ chức',
				'icon'       => 'fa-user',
				'controller' => 'organization',
				'menu_child' => [
					[
						'label'      => 'Thêm tổ chức mới',
						'icon'       => 'fa-user',
						'controller' => 'organization/add',
					],
					[
						'label'      => 'Danh sách tổ chức',
						'icon'       => 'fa-user',
						'controller' => 'organization',
					],
				],
			],
			[
				'label'      => 'Quản lý người dùng',
				'icon'       => 'fa-user',
				'controller' => 'user',
				'menu_child' => [
					[
						'label'      => 'Quản lý user',
						'icon'       => 'fa-user',
						'controller' => 'user',
					],
					[
						'label'      => 'Quản lý admin cấp dưới',
						'icon'       => 'fa-user',
						'controller' => 'sub_admin',
					],
					[
						'label'      => 'Quản lý giáo viên',
						'icon'       => 'fa-user',
						'controller' => 'teacher',
					],
					[
						'label'      => 'Quản lý phụ huynh',
						'icon'       => 'fa-user',
						'controller' => 'parents',
					]
					, [
						'label'      => 'Quản lý học sinh',
						'icon'       => 'fa-user',
						'controller' => 'student',
					],
				],
			],
			[
				'label'      => 'Phân quyền',
				'icon'       => 'fa-dashboard',
				'controller' => 'role',
			],
			[
				'label'      => 'Quản lý lớp học',
				'icon'       => 'fa-user',
				'controller' => 'classroom',
			],
			[
				'label'      => 'Quản lý bài học',
				'icon'       => 'fa-user',
				'controller' => 'question',
				'menu_child' => [
					[
						'label'      => 'Vocabulary',
						'icon'       => 'fa-vimeo',
						'controller' => 'vocabulary/',
					],
					[
						'label'      => 'Grammar',
						'icon'       => 'fa-book',
						'controller' => 'grammar/',
					],
					[
						'label'      => 'Listening',
						'icon'       => 'fa-headphones',
						'controller' => 'listening/',
					],
					[
						'label'      => 'Speaking',
						'icon'       => 'fa-bullhorn',
						'controller' => 'speaking/',
					],
					[
						'label'      => 'Reading',
						'icon'       => 'fa-eye',
						'controller' => 'reading/',
					],
					[
						'label'      => 'Writing',
						'icon'       => 'fa-pencil-square',
						'controller' => 'writing/',
					],
				],
			],
			[
				'label'      => 'Quản lý giáo trình',
				'icon'       => 'fa-user',
				'controller' => 'curriculum',
			],
			[
				'label'      => 'Quản lý bài kiểm tra',
				'icon'       => 'fa-user',
				'controller' => 'exam',
			],
			[
				'label'      => 'Phản hồi',
				'icon'       => 'fa-user',
				'controller' => 'feedback',
			],
			[
				'label'      => 'Quản lý Licence',
				'icon'       => 'fa-user',
				'controller' => 'licence',
			],
			[
				'label'      => 'Cài đặt hệ thống',
				'icon'       => 'fa-cogs',
				'controller' => 'sys_config',
			],
		];
		return $data_menu;
	}

	/**
	 * defined breadcrumb
	 *
	 * @param string $controller
	 *
	 * @return array|mixed
	 */
	public function get_breadcrumb($controller = 'user') {
		$data_controller_config = [];
		$data_controller_config['home'] = [
			'first_controller' => 'Dashboard',
		];
		$data_controller_config['user'] = [
			'first_controller'  => 'Quản lý người dùng',
			'second_controller' => 'Quản lý người dùng',
			'list_method'       => [
				'profile' => 'Hồ sơ cá nhân',
				'add'     => 'Tạo tài khoản mới',
				'edit'    => 'Cập nhật tài khoản',
			],
		];

		$data_controller_config['teacher'] = [
			'first_controller'  => 'Quản lý người dùng',
			'second_controller' => 'Quản lý giáo viên',
			'list_method'       => [
				'add'  => 'Tạo tài khoản mới',
				'edit' => 'Cập nhật tài khoản',
			],
		];

		$data_controller_config['role'] = [
			'first_controller' => 'Quản lý phân quyền',
			'list_method'      => [
				'add' => 'Phân quyền',
			],
		];

		$data_controller_config['classroom'] = [
			'first_controller'  => 'Quản lý lớp học',
			'second_controller' => 'Quản lý lớp học',
			'list_method'       => [
				'add'  => 'Thêm lớp học',
				'edit' => 'Cập nhật lớp học',
			],
		];

		$data_controller_config['parents'] = [
			'first_controller'  => 'Quản lý người dùng',
			'second_controller' => 'Quản lý phụ huynh',
			'list_method'       => [
				'add'  => 'Tạo tài khoản mới',
				'edit' => 'Cập nhật tài khoản',
			],
		];

		$data_controller_config['sub_admin'] = [
			'first_controller'  => 'Quản lý admin cấp dưới',
			'second_controller' => 'Quản lý admin cấp dưới',
			'list_method'       => [
				'add'  => 'Tạo tài khoản mới',
				'edit' => 'Cập nhật tài khoản',
			],
		];

		$data_controller_config['student'] = [
			'first_controller'  => 'Quản lý người dùng',
			'second_controller' => 'Quản lý học sinh',
			'list_method'       => [
				'add'  => 'Tạo tài khoản mới',
				'edit' => 'Cập nhật tài khoản',
			],
		];

		$data_controller_config['vocabulary'] = [
			'first_controller'  => 'Quản lý bài học',
			'second_controller' => 'Bài học vocabulary',
			'list_method'       => [
				'add'  => 'Thêm bài mới',
				'edit' => 'Chỉnh sửa bài học',
			],
		];

		$data_controller_config['grammar'] = [
			'first_controller'  => 'Quản lý bài học',
			'second_controller' => 'Bài học grammar',
			'list_method'       => [
				'add'  => 'Thêm bài mới',
				'edit' => 'Chỉnh sửa bài học',
			],
		];

		$data_controller_config['listening'] = [
			'first_controller'  => 'Quản lý bài học',
			'second_controller' => 'Bài học listening',
			'list_method'       => [
				'add'  => 'Thêm bài mới',
				'edit' => 'Chỉnh sửa bài học',
			],
		];

		$data_controller_config['speaking'] = [
			'first_controller'  => 'Quản lý bài học',
			'second_controller' => 'Bài học speaking',
			'list_method'       => [
				'add'  => 'Thêm bài mới',
				'edit' => 'Chỉnh sửa bài học',
			],
		];

		$data_controller_config['reading'] = [
			'first_controller'  => 'Quản lý bài học',
			'second_controller' => 'Bài học reading',
			'list_method'       => [
				'add'  => 'Thêm bài mới',
				'edit' => 'Chỉnh sửa bài học',
			],
		];

		$data_controller_config['writing'] = [
			'first_controller'  => 'Quản lý bài học',
			'second_controller' => 'Bài học writing',
			'list_method'       => [
				'add'  => 'Thêm bài mới',
				'edit' => 'Chỉnh sửa bài học',
			],
		];
		$data_controller_config['curriculum'] = [
			'first_controller'  => 'Quản lý giáo trình',
			'second_controller' => 'Thêm mới giáo trình',
			'list_method'       => [
				'add_from_lib'  => 'Thêm giáo trình',
				'edit_from_lib' => 'Chỉnh sửa giáo trình',
			],
		];
		$data_controller_config['exam'] = [
			'first_controller'  => 'Quản lý bài kiểm tra',
			'second_controller' => 'Thêm từ thư viện',
			'list_method'       => [
				'add_from_lib'  => 'Thêm bài mới',
				'edit_from_lib' => 'Chỉnh sửa bài kiểm tra',
			],
		];
		$data_controller_config['licence'] = [
			'first_controller'  => 'Quản lý licence',
			'second_controller' => '',
			'list_method'       => [
				'add'  => 'Thêm licence mới',
				'edit' => 'Chỉnh sửa licence',
			],
		];
		$data_controller_config['organization'] = [
			'first_controller'  => 'Quản lý tổ chức',
			'second_controller' => 'Danh sách tổ chức',
			'list_method'       => [
				'add'      => 'Thêm tổ chức mới',
				'edit'     => 'Chỉnh sửa thông tin tổ chức',
				'view_log' => 'Lịch sử hoạt động',
			],
		];

		return empty($data_controller_config[$controller]) ? [] : $data_controller_config[$controller];
	}

	/**
	 * Get list controller and method
	 */
	public function get_controller_method() {
		return [
			'home'         => ['name'    => 'Dashboard',
							   'methods' => [],
			],
			'organization' => ['name'    => 'Tổ chức',
							   'methods' => ['add', 'edit', 'delete'],
			],
			'user'         => ['name'    => 'Người dùng',
							   'methods' => ['add', 'edit', 'delete'],
			],
			'classroom'    => ['name'    => 'Lớp học',
							   'methods' => ['add', 'edit', 'delete'],
			],
			'question'     => ['name'    => 'Bài học',
							   'methods' => ['add', 'edit', 'delete'],
			],
			'curriculum'   => ['name'    => 'Giáo trình',
							   'methods' => ['add', 'edit', 'delete'],
			],
			'exam'         => ['name'    => 'Bài kiểm tra',
							   'methods' => ['add', 'edit', 'delete'],
			],
			'feedback'     => ['name'    => 'Phản hồi',
							   'methods' => ['add', 'edit', 'delete'],
			],
			'sys_config'   => ['name'    => 'Cài đặt hệ thống',
							   'methods' => ['add', 'edit', 'delete'],
			],
		];
	}

	/**
	 * set config js file
	 *
	 * @param      $path_js
	 * @param bool $in_head
	 */
	protected function load_js($path_js, $in_head = FALSE) {
		if (!is_array($path_js)) {
			if (!empty($in_head)) {
				$this->_list_js_head[] = $path_js;
			} else {
				$this->_list_js_footer[] = $path_js;
			}
		} else {
			foreach ($path_js as $js_item) {
				if (!empty($in_head)) {
					$this->_list_js_head[] = $js_item;
				} else {
					$this->_list_js_footer[] = $js_item;
				}
			}
		}
	}

	/**
	 * set css
	 *
	 * @param $path_css
	 */
	protected function load_css($path_css) {
		if (is_array($path_css)) {
			foreach ($path_css as $css_item) {
				$this->_list_css[] = $css_item;
			}
		} else {
			$this->_list_css[] = $path_css;
		}
	}

	/**
	 * @param string $content
	 * @param bool   $is_html
	 *
	 * @return mixed
	 */
	public function load_view($content = '', $is_html = FALSE) {
		$data["content"] = $content;
		// check active menu
		$controller = $this->router->fetch_class();
		$method = $this->router->fetch_method();
		$data["controller"] = $controller;
		$data["method"] = $method;
		$data['data_menu'] = $this->get_menu();
		$data['breadcrumb_data'] = $this->get_breadcrumb($controller);
		$data['list_js_head'] = $this->_list_js_head;
		$data['list_js_footer'] = $this->_list_js_footer;
		$data['list_css'] = $this->_list_css;
		if ($is_html) {
			return $this->load->view("base/layout", $data, TRUE);
		}
		$this->load->view("base/layout", $data);
	}

	function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function dd($data = NULL) {
		echo "<pre>";
		var_dump($data);
		exit();
	}

	public function create_alias($str) {
		$str = strtolower($str);
		$str = str_replace(['%20', ' '], '_', $str);
		$str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
		$str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
		$str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
		$str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
		$str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
		$str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
		$str = preg_replace('/(đ)/', 'd', $str);
		$str = preg_replace('/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/', 'A', $str);
		$str = preg_replace('/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/', 'E', $str);
		$str = preg_replace('/(Ì|Í|Ị|Ỉ|Ĩ)/', 'I', $str);
		$str = preg_replace('/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/', 'O', $str);
		$str = preg_replace('/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/', 'U', $str);
		$str = preg_replace('/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/', 'Y', $str);
		$str = preg_replace('/(Đ)/', 'D', $str);
		$str = str_replace(' ', '_', str_replace('&*#39;', '', $str));
		return $str;
	}

	/**
	 * @param        $image ex : $FILES['logo']
	 * @param string $key   ex: $_FILES['logo'] -> $key = 'logo'
	 *
	 * @return array
	 */
	public function upload_file($image, $key = 'image') {
		if (!is_dir('assets/audio')) {
			mkdir('assets/audio', 0777, TRUE);
		}
		$data_file = explode('.', $image['name']);
		$data_file = $data_file[(count($data_file) - 1)];
		if (in_array(strtolower($data_file), ['jpg', 'jpeg', 'png', 'gif']) || in_array(strtolower($data_file), ['mp3'])) {
			$upload_path = in_array(strtolower($data_file), ['mp3']) ? 'audio' : 'img';
			$config['upload_path'] = './assets/' . $upload_path . '/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif|mp3';
			$config['file_name'] = $image['name'];
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ($this->upload->do_upload($key)) {
				$uploadData = $this->upload->data();
				return [
					'status' => TRUE,
					'type'   => $upload_path,
					'path'   => 'assets/' . $upload_path . '/' . $uploadData['file_name'],
				];
			}
			return ['status' => FALSE, 'msg' => $this->upload->display_errors()];
		}
		return ['status' => FALSE, 'msg' => 'Vui lòng chọn đúng định dạng ảnh.'];
	}

	/**
	 * show form confirm delete data
	 *
	 * @return bool
	 */
	public function delete() {
		$data['url_delete'] = site_url($this->_controller . '/submit_delete');
		$data['list_id'] = $this->input->post('list_id');
		$content_table = $this->load->view("base/form_delete", $data, TRUE);
		$data_return = [
			"status"      => 1,
			"status_code" => "SUCCESS",
			"html"        => $content_table,
		];
		echo json_encode($data_return);
		return TRUE;
	}

	/**
	 * delete list user id
	 *
	 *
	 * @return bool
	 */
	public function submit_delete() {
		$list_id = $this->input->post('list_id');
		$delete_id = $this->handle_delete_data($list_id);
		if ($delete_id) {
			$data_return = [
				"status" => 1,
				"msg"    => "Xóa " . $delete_id . " bản ghi thành công!",
			];
			echo json_encode($data_return);
			return TRUE;
		} else {
			$data_return = [
				"status" => 0,
				"msg"    => "Xóa bản ghi không thành công, vui lòng thử lại!",
			];
			echo json_encode($data_return);
			return TRUE;
		}
	}

	/**
	 * show form confirm public, private data
	 *
	 * @return bool
	 */
	public function pubic_private_record() {
		$data['url_submit'] = site_url($this->_controller . '/submit_pubic_private_record');
		$data['list_id'] = $this->input->post('list_id');
		$data['type'] = $this->input->post('type');
		$content_table = $this->load->view("base/form_public_private", $data, TRUE);
		$data_return = [
			"status"      => 1,
			"status_code" => "SUCCESS",
			"html"        => $content_table,
		];
		echo json_encode($data_return);
		return TRUE;
	}

	/**
	 * public/private list user id
	 *
	 *
	 * @return bool
	 */
	public function submit_pubic_private_record() {
		$list_id = $this->input->post('list_id');
		$type = $this->input->post('type');
		$result = $this->model->update_many($list_id, ['status' => $type]);
		if ($result) {
			$data_return = [
				"status" => 1,
				"msg"    => "Xử lý " . count($list_id) . " bản ghi thành công!",
			];
			echo json_encode($data_return);
			return TRUE;
		} else {
			$data_return = [
				"status" => 0,
				"msg"    => "Xử lý bản ghi không thành công, vui lòng thử lại!",
			];
			echo json_encode($data_return);
			return TRUE;
		}
	}

	/**
	 * delete data
	 *
	 * @param $list_id
	 *
	 * @return mixed
	 */
	public function handle_delete_data($list_id) {
		return $this->model->delete_many($list_id);
	}

	/**
	 * function to create user account on LMS
	 *
	 * @param $user_data
	 *
	 * @return bool
	 */
	public function insert_lms_user($user_data) {
		$this->load->model('M_lms_user', 'lms_user');
		$this->load->model('M_lms_user_address', 'lms_user_address');
		$this->load->model('M_lms_user_categories', 'lms_user_categories');
		$this->load->model('M_lms_user_curriculum', 'lms_user_curriculum');
		$this->load->model('M_lms_user_profile', 'lms_user_profile');
		$userLogonID = empty($user_data['username']) ? '' : $user_data['username'];
		$firstName = empty($user_data['firstName']) ? '' : $user_data['firstName'];
		$email = empty($user_data['email']) ? '' : $user_data['email'];
		if (empty($userLogonID) || empty($firstName) || empty($email)) {
			return FALSE;
		} else {
			// check user exist before insert
			$check_exist = $this->lms_user->get_by(['UserLogonID' => $userLogonID]);
			if (empty($check_exist)) {
				// step1: insert user info into table Users
				$data_insert_user = array(
					'UserLogonID'       => $userLogonID,
					'CompanyID'         => '1',
					'PasswordKey'       => '27CA36B19DC9BC1F13DD63B3EA335EC0',// mean 123@abc
					'FirstName'         => $firstName,
					'RaceID'            => '1',
					'UserType'          => '15',
					'IsSuperAdmin'      => '0',
					'IsActive'          => '1',
					'IsRegisteredFrom'  => '1',
					'EmailID'           => $email,
					'BusinessID'        => '3',
					'StudentEntityID'   => '1',
					'ManagerID'         => '1',
					'ManagerFirstName'  => 'admin',
					'ManagerLastName'   => 'admin',
					'EmployeeID'        => $userLogonID, // thay bang CMND neu co
					'TimeZoneID'        => '31',
					'SalesExecutivesID' => '1',
					'LanguageCode'      => 'vi_VN',
					'RegistrationDate'  => date('Y-m-d H:i:s'),
					'InsertDateTime'    => date('Y-m-d H:i:s'),
				);
				$user_id = $this->lms_user->insert($data_insert_user);
				if ($user_id) {
					// step2: insert user profile
					$profile_data = array(
						'UserID'         => $user_id,
						'InsertUserID'   => '1',
						'InsertDateTime' => date('Y-m-d H:i:s'),
					);
					$this->lms_user_profile->insert($profile_data);
					// step3: insert user address
					$address_data = array(
						'UserID'            => $user_id,
						'BusinessCountryID' => '1',
						'HomeCountryID'     => '1',
						'InsertUser'        => '1',
						'InsertDateTime'    => date('Y-m-d H:i:s'),
					);
					$this->lms_user_address->insert($address_data);
					// step4: insert user categories
					$category_data = array(
						'UserID'          => $user_id,
						'CategoryID'      => '10',
						'AddedByUserType' => '15',
						'InsertUserID'    => $user_id,
					);
					$this->lms_user_categories->insert($category_data);
					//step5: insert user curriculum
					$curriculum_data = array(
						'UserID'          => $user_id,
						'CurriculumID'    => '15',
						'AddedByUserType' => '15',
						'InsertUserID'    => $user_id,
					);
					$this->lms_user_curriculum->insert($curriculum_data);
					return TRUE;
				} else {
					return FALSE;
				}
			} else {
				return FALSE;
			}
		}
	}

}
