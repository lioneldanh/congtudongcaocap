<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('show_file_name')) {
    function show_file_name($file_name = '') {
        if(empty($file_name)) return 'Not found';
        $file_name = explode('/',$file_name);
        return !empty($file_name) ? end($file_name) : 'Not found';
    }
}