<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2019, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license	https://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller {

	/**
	 * Reference to the CI singleton
	 *
	 * @var	object
	 */
	private static $instance;

	/**
	 * @var int
	 */
	public $_limit_default = 10;

	/**
	 * @var array
	 */
	private $_column_search = [];

	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	public function __construct()
	{
		self::$instance =& $this;

		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class)
		{
			$this->$var =& load_class($class);
		}

		$this->load =& load_class('Loader', 'core');
		$this->load->initialize();
		log_message('info', 'Controller Class Initialized');
	}

	// --------------------------------------------------------------------

	/**
	 * Get the CI singleton
	 *
	 * @static
	 * @return	object
	 */
	public static function &get_instance()
	{
		return self::$instance;
	}

	/**
	 * Process data condition
	 */
	protected function _process_condition($data_condition) {
		if (empty($data_condition)) {
			$data_return = [
				"limit"  => $this->_limit_default,
				"offset" => 0,
				"paging" => 1,
			];
		} else {
			$data_return = [];
			$data_return["limit"] = isset($data_condition["limit"]) ? $data_condition["limit"] : $this->_limit_default;
			$data_return["offset"] = isset($data_condition["offset"]) ? $data_condition["offset"] : 0;
			$data_return["page"] = isset($data_condition["page"]) ? $data_condition["page"] : 0;
			// filter
			$filter_condition_tmp = isset($data_condition["filter"]) ? $data_condition["filter"] : [];
			$where_condition = [];
			$wherein_condition = [];
			if (isset($filter_condition_tmp["valueDateFrom"]) && isset($filter_condition_tmp["valueDateTo"])) {
				if ($filter_condition_tmp["valueDateFrom"]) $where_condition["m.RegistrationDate >="] = date("Y-m-d H:i:s", strtotime($filter_condition_tmp["valueDateFrom"]));
				if ($filter_condition_tmp["valueDateTo"]) $where_condition["m.RegistrationDate <="] = date("Y-m-d H:i:s", strtotime($filter_condition_tmp["valueDateTo"]));
				unset($filter_condition_tmp["valueDateFrom"]);
				unset($filter_condition_tmp["valueDateTo"]);
			}

			if (!empty($filter_condition_tmp) && is_array($filter_condition_tmp)) {
				foreach ($filter_condition_tmp as $key => $value) {
					if (!empty($value) && is_array($value)) {
						if (strpos($key, ".") !== FALSE) {
							$wherein_condition[$key] = $value;
						} else {
							$wherein_condition['m.' . $key] = $value;
						}
					} else {
						if (!empty($value)) {
							if (strpos($key, ".") !== FALSE) {
								$where_condition[$key] = $value;
							} else {
								$where_condition['m.' . $key] = $value;
							}
						}
					}
				}
			}

			$data_return["where"] = $where_condition;
			$data_return["wherein"] = $wherein_condition;
			// search - todo
			$search_condition_tmp = isset($data_condition["search"]["search_all"]) ? $data_condition["search"]["search_all"] : [];
			$start_key = isset($data_condition["search"]["search_key"]) ? $data_condition["search"]["search_key"] : '';
			$search_condition = [];
			$like_after = [];
			$search_condition_value = NULL;
			$this->_column_search = $this->_get_column_search();
			if (!empty($search_condition_tmp)) {
				foreach ($this->_column_search as $col) {
					$search_condition[$col] = $search_condition_tmp;
				}
			}

			if (!empty($start_key)) {
				foreach ($this->_column_search as $col) {
					$like_after[$col] = $start_key;
				}
			}
			$data_return["like"]["or"] = $search_condition;
			$data_return["like"]["after"] = $like_after;
			// order by
			$data_return['order_by'] = isset($data_condition["order_by"]) ? $data_condition["order_by"] : '';
		}
		return $data_return;
	}

	/**
	 * @param        $list_condition
	 * @param  		 $total_item
	 * @param string $type
	 * @param null   $model
	 *
	 * @return array
	 */
	protected function get_paging_data($list_condition, $total_item) {
		$data = [];
		$limit = isset($list_condition["limit"]) ? $list_condition["limit"] : $this->_limit_default;
		$current_page = intval(isset($list_condition["page"]) ? $list_condition["page"] : 1);
		if ($limit < 0) {
			$limit = 0;
		}
		/* If change limit or change filter: reset page to 1 */
		$old_condition = $this->session->userdata('table_manager_condition');
		$old_limit = intval(isset($old_condition["limit"]) ? $old_condition["limit"] : $this->_limit_default);
		$old_filter = isset($old_condition["filter"]) ? $old_condition["filter"] : [];
		ksort($old_filter);
		if (($limit != $old_limit)) {
			$current_page = 1;
		}
		//Update session condition after reset page
		$this->session->set_userdata('table_manager_condition', $list_condition);
		$post = ($current_page - 1) * $limit;
		if ($post < 0) {
			$post = 0;
			$current_page = 1;
		}
		if ($limit != 0) {
			$total_page = (int)($total_item / $limit);
		} else {
			$total_page = 0;
		}

		if (($total_page * $limit) < $total_item) {
			$total_page += 1;
		}

		$link = "#";
		$data["paging"] = $this->_get_paging($total_page, $current_page, 5, $link);
		$data["from"] = $post + 1;
		$data["to"] = $post + $limit;
		if ($data["to"] > $total_item || $limit == 0) {
			$data["to"] = $total_item;
		}
		$data["limit"] = $limit;
		$data["post"] = $post;
		$data["total"] = $total_item;
		$data["paging_detail"] = $this->load->view("pagination/paging_detail", $data, TRUE);
		return $data;
	}

	/**
	 * Get html for pagination
	 *
	 * @param int    $total   = total page
	 * @param int    $current = current page
	 * @param int    $display = page show on link
	 * @param string $link    = origin link
	 * @param string $key     = page key in get
	 *
	 * @return string html of pagination
	 */
	protected function _get_paging($total, $current, $display, $link, $key = 1) {
		$data["total_page"] = $total;
		$data["current_page"] = $current;
		$data["page_link_display"] = $display;
		$data["link"] = $link;
		$data["key"] = $key;
		return $this->load->view("pagination/paging", $data, TRUE);
	}

}
