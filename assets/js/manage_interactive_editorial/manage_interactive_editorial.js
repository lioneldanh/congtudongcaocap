let selected_img_arr = [];
$(document).ready(function(){
	// check_select();
	// Click button upload file
	$(document).on('click', '.btn-upload-file-media', function () {
		$('.input-file-media:last').trigger('click');
	});

	//Change value file
	$(document).on('change', '.input-file-media', function (event) {
		let files = event.target.files;
		let size = files[0].size;
		if (size > 100000000) {
			$.notify('File không được quá 100MB!', 'warn');
			$(this).val('');
			return false;
		} else {
			let type_upload = files[0].type;
			type_upload = type_upload.split('/');
			let src = '';
			if (type_upload[0] === 'image') {
				ajax_load_upload_media(type_upload[0], this, src, this.files[0]);
			} else if (type_upload[0] === 'video' || type_upload[0] === 'audio') {
				src = read_media(event, type_upload[0]);
				ajax_load_upload_media(type_upload[0], this, src, this.files[0]);
			}
		}
	});

	$(document).on('click', '.check-table-item', function () {
		let c = $(this).prop('checked');
		if (c) {
			selected_img_arr.push($(this).val());
		} else {
			let index = selected_img_arr.indexOf($(this).val());
			if (index > -1) {
				selected_img_arr.splice(index, 1);
			}
		}
	});
	// Save library
	$(document).on('click', '.btn-save-library', function () {
		show_loading();
		let url = $(this).attr('data-url');
		$.notify('Lưu thành công!', 'success');
		setTimeout(function () {
			hide_loading();
			window.location.href = url;
		}, 2000);
	});

	//Change value file
	$(document).on('click', '.div-media', function () {
		let value = $(this).attr("data-id");
		if($(this).hasBorder()) {
			$(this).css("border", "");
			$(this).css("box-shadow", "");
			selected_img_arr = $.grep(selected_img_arr, function(item) {
				return item !== value;
			});
		}
		else {
			$(this).css("border", "2px solid #005ba1");
			$(this).css("box-shadow", "0 4px 6px #a3a3a3");
			selected_img_arr.push(value); //something like this
		}
	});

	$(document).on('click', '.btn-save-attached', function () {
		let url = $(this).attr('data-url');
		let book_id = $('#bookId').val();
		let data = {};
		let data_file = [];
		$('.check-table-item').each(function () {
			if ($(this).is(":checked")) {
				data_file.push($(this).val());
			}
		});
		data['book_id'] = book_id;
		data['data'] = data_file;
		$.ajax({
			url         : url,
			type        : "POST",
			data        : data,
			beforeSend  : function () {
				show_loading();
			},
			success     : function (data) {
				$.notify('Lưu thành công!', 'success');
				setTimeout(function () {
					hide_loading();
					window.location.href = data;
				}, 2000);
			},
			error       : function (a, b, c) {
				hide_loading();
				console.log(a + b + c);
			}
		});
	});

	$(document).on('click', '.btn-remove-media-lib', function () {
		let url = $(this).attr('data-url');
		let file_name = $(this).attr('data-name');
		let type = $(this).attr('data-type');
		let data = {};
		data['file_name'] = file_name;
		data['file_type'] = type;
		let self = $(this);
		$.ajax({
			url         : url,
			type        : "POST",
			data        : data,
			beforeSend  : function () {
				show_loading();
			},
			success     : function (data) {
				$.notify('Xoá thành công!', 'success');
				self.parents('.form-preview-media:first').remove();
			},
			error       : function (a, b, c) {
				$.notify('Xoá không thành công!', 'success');
				console.log(a + b + c);
			},
			complete	: function () {
				hide_loading();
			}
		});
	});
});


//Load media
function read_media(event, type) {
	let files = event.target.files;
	let type_upload = files[0].type;
	let size = files[0].size;
	type_upload = type_upload.split('/');
	if (type_upload[0] === type) {
		if (size > 100000000) {
			$.notify('Kích thước file không được quá 100MB', "warn");
			return false;
		} else {
			return URL.createObjectURL(files[0]);
		}
	} else {
		$.notify('Không đúng dịnh dạng', "warn");
		return false;
	}
}

//Read image
function read_image(input) {
	if (input.files && input.files[0]) {
		let reader = new FileReader();

		reader.onload = function(e) {
			$('.image-preview:last').attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}

function ajax_load_upload_media(type, input, src, file) {
	let url = $('.btn-upload-file-media').attr('data-url');
	let input_file = $('.form-upload-media').html();
	let form_data = new FormData();
	form_data.append('type', type);
	form_data.append('src', src);
	form_data.append('file', file);
	$.ajax({
		url 	: url,
		type	: "POST",
		data	: form_data,
		dataType: "json",
		contentType : false,
		processData : false,
		beforeSend: function () {
			show_loading();
		},
		success: function (data) {
			if (type === 'image') {
				read_image(input);
				$('.content-upload-image').append(data.html);
				$('.content-upload-image').find('.form-preview-media:last').append(input_file);
			} else if (type === 'video') {
				$('.content-upload-video').append(data.html);
				$('.content-upload-video').find('.form-preview-media:last').append(input_file);
			} else {
				$('.content-upload-audio').append(data.html);
				$('.content-upload-audio').find('.form-preview-media:last').append(input_file);
			}
			$('.input-file-media').val('');
		},
		error: function (a, b, c) {
			console.log(a + b + c);
		},
		complete: function () {
			hide_loading();
		}
	});
}

$.fn.hasBorder = function() {
	if ((this.outerWidth() - this.innerWidth() > 0) ||  (this.outerHeight() - this.innerHeight() > 0)){
		return true;
	}
	else{
		return false;
	}
};

