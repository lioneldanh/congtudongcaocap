$(document).ready(function () {
	$(document).on("click", ".btn-upload-avatar", function () {
		$('#userAvatar').trigger('click');
	});

	$(document).on("change", "#userAvatar", function () {
		readURL(this);
	});

	$(document).on("click", ".btn-save-user", function () {
		let id = $(this).attr('data-id');
		let password = $('#password').val();
		let full_name = $('#fullName').val();
		let email = $('#email').val();
		let form_data = new FormData;
		form_data.append('id', id);
		form_data.append('full_name', full_name);
		form_data.append('email', email);
		form_data.append('avatar', $('#userAvatar')[0].files[0]);
		if (password) {
			let c = /^(?=.*\d)(?=.*[a-z])[0-9a-zA-Z]{8,}$/;
			let r = c.test(password);
			if (!r) {
				$.notify('Mật khẩu phải có lớn hơn 8 ký tự và chứ cả chữ lẫn số', 'warn');
				return false;
			} else {
				form_data.append('password', password);
			}
		}
		ajax_save(form_data);
	});
});

function ajax_save(form_data) {
	let url = $('.btn-save-user').attr('data-url');
	$.ajax({
		url        : url,
		type       : "POST",
		data       : form_data,
		contentType: false,
		processData: false,
		beforeSend : function () {
			show_loading();
		},
		success    : function (data) {
			data = JSON.parse(data);
			let status = data.status;
			if (status) {
				$.notify('Lưu thông tin thành công!', "success");
				setTimeout(function () {
					hide_loading();
					window.location.href = data.url;
				},2000);
			} else {
				hide_loading();
				$.notify(data.msg, "warn");
			}
		},
		error      : function (a, b, c) {
			hide_loading();
			$.notify('Có lỗi xảy ra vui lòng thử lại', "warn");
			console.log(a + b + c);
		}
	});
}


function readURL(input) {
	if (input.files && input.files[0]) {
		let reader = new FileReader();

		reader.onload = function(e) {
			$('#imageBookPreview').attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}
