$(document).ready(function () {
	$(document).on("click", ".btn-next-email", function () {
		let email = $('#email').val();
		if (email) {
			let data = {};
			data['email'] = email;
			ajax_send_mail(data);
		} else {
			$.notify('Vui lòng nhập email của bạn!', 'warn');
			return false;
		}
	});

	$(document).on("click", ".btn-save-password", function () {
		let token = $(this).attr('data-token');
		let password = $('#password').val();
		let re_password = $('#rePassword').val();
		if (token && password && re_password) {
			if (password === re_password) {
				let data = {};
				data['token'] = token;
				data['password'] = password;
				data['re_password'] = re_password;
				ajax_save_password(data);
			} else {
				$.notify('Mật khẩu vừa nhập không trùng nhau!', 'warn');
				return false;
			}
		}
		 else {
			$.notify('Vui lòng nhập email của bạn!', 'warn');
			return false;
		}
	});
});

function ajax_save_password(data) {
	let url = $('.btn-save-password').attr('data-url');
	$.ajax({
		url        : url,
		type       : "POST",
		data       : data,
		dataType   : 'json',
		beforeSend : function () {
			show_loading();
		},
		success    : function (data) {
			let status = data.status;
			if (status) {
				$.notify('Thay đổi mật khẩu thành công!', "success");
				setTimeout(function () {
					hide_loading();
					window.location.href = data.url;
				},2000);
			} else {
				hide_loading();
				$.notify(data.msg, "warn");
			}
		},
		error      : function (a, b, c) {
			hide_loading();
			$.notify('Có lỗi xảy ra vui lòng thử lạib', "warn");
			console.log(a + b + c);
		}
	});
}

function ajax_send_mail(data) {
	let url = $('.btn-next-email').attr('data-url');
	$.ajax({
		url        : url,
		type       : "POST",
		data       : data,
		dataType   : 'json',
		beforeSend : function () {
			show_loading();
		},
		success    : function (data) {
			let status = data.status;
			if (status) {
				$.notify('Vui lòng kiểm tra lại gmail của bạn!', "success");
				setTimeout(function () {
					hide_loading();
					window.location.href = data.url;
				},2000);
			} else {
				hide_loading();
				$.notify(data.msg, "warn");
			}
		},
		error      : function (a, b, c) {
			hide_loading();
			$.notify('Có lỗi xảy ra vui lòng thử lạib', "warn");
			console.log(a + b + c);
		}
	});
}
