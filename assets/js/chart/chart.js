/**
 * Created by miunh_nim on 28-May-19.
 */
$(document).ready(function ($) {
    var myChart = new Chart(document.getElementById("line-chart"), {
            type   : 'line',
            data   : {
                labels  : [],
                datasets: [
                    {
                        data            : [],
                        label           : "Sync Done",
                        borderColor     : "#cd0066",
                        fill            : false,
                        borderWidth     : 1,
                        pointBorderWidth: 0,
                        pointStyle      : 'line',
                        pointHitRadius  : 4,
                    },
                    {
                        data            : [],
                        label           : "Sync Fail",
                        borderColor     : "#0034ae",
                        fill            : false,
                        borderWidth     : 1,
                        pointBorderWidth: 0,
                        pointStyle      : 'line',
                        pointHitRadius  : 4,
                    }

                ]
            },
            options: {
                title     : {
                    display : true,
                    text    : '',
                    fontSize: 16,
                },
                responsive: true,
                scales    : {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            // suggestedMin: 0,
                            // suggestedMax: 1000,
                        }
                    }]
                },
                elements  : {
                    point: {
                        radius: 0
                    }
                }
            }
        })
        ;

    // show_loading();
    getChartData(myChart);

    $(document).on('click', '.filter-block .btn-submit', function (e) {
        getChartData(myChart);
    })


})

function getChartData(myChart) {
    var timeDiff = parseInt((Date.parse($('.to-time').val()) - Date.parse($('.from-time').val())) / (24 * 60 * 60 * 1000));
    if (timeDiff > 32) {
        alert('Invalid datetime filter. System only accept for less than 32 days duration. Please select again?')
    } else {
        callAjax(myChart)
    }
}

function callAjax(myChart) {
    show_loading();
    $.ajax({
        url     : $('body').attr('data-chart-url'),
        type    : "POST",
        data    : {
            from: $('.from-time').val(),
            to  : $('.to-time').val(),
        },
        dataType: "json",
        success : function (data) {
            // add new label and data point to chart's underlying data structures
            myChart.options.title.text = data.chart_title;
            myChart.data.labels = data.chart_label;
            $('.total_done').text(data.total_sync_done);
            $('.total_fail').text(data.total_sync_fail);

            myChart.data.datasets[0].data = data.data_sync_done;
            myChart.data.datasets[1].data = data.data_sync_fail;
            if (!data.check_range_valid) {
                $('.from-time').val(data.from);
            }
            // myChart.data.datasets[1].data = data.chart_course_raw;

            // re-render the chart
            myChart.update();
            hide_loading();
        }
    });
}


function show_loading() {
    // update size overlay
    var loadingModal = $("#loading-modal");
    var contentLayout = $(".content-layout");
    loadingModal.height(contentLayout.height());
    loadingModal.width(contentLayout.width());
    // show loading
    loadingModal.show();
}

function hide_loading() {
    setTimeout(function () {
        $("#loading-modal").hide();
    }, 200);
}
