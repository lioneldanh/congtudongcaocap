$(document).ready(function(){

    if (!$('#tsCover')[0].defaultValue) {
        $('#image-ts-cover').hide();
    }

    if (!$('#src-audio')[0].defaultValue) {
        $('.btn-remove-audio').prop('disabled', true);
        $('#preview-audio').hide();
    }
    $(document).on("change", "#tsCover", function () {
        readURL(this);
        show_image();
    });

    $(document).on("click", ".btn-upload-ts-cover", function () {
        $('#tsCover').trigger('click');
    });

    $(document).on("click", ".btn-save-ts", function () {
        save_meta_data();
    });

});

function show_image() {
    let av = $('#tsCover').val();
    if (av) {
        $('#image-ts-cover').show();
    } else {
        $('#image-ts-cover').hide();
    }
}

function readURL(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();

        reader.onload = function(e) {
            $('#image-ts-cover').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function save_meta_data() {
    //Get all value input
    let id = $('#id').val();
    let ts_name = $('#ts_name').val();
    let play_rule = $('#play_rule').val();

    //Check format_book and book_name
    if (!ts_name || !play_rule) {
        $.notify('Vui lòng điền đầy đủ thông tin!', 'warn');
        return false;
    } else {
        let form_data = new FormData();
        form_data.append('id', id);
        form_data.append('ts_name', ts_name);
        form_data.append('play_rule', play_rule);
        form_data.append('ts_image', $('#tsCover')[0].files[0]);
        form_data.append('story', $('#src-audio')[0].files[0]);
        ajax_save_meta_data(form_data);
    }
}

function ajax_save_meta_data(form_data) {
    let url = $('.btn-save-ts').attr('data-url');
    $.ajax({
        url        : url,
        type       : "POST",
        data       : form_data,
        dataType   : 'json',
        contentType: false,
        processData: false,
        beforeSend : function () {
            show_loading();
        },
        success    : function (data) {
            let status = data.status;
            if (status) {
                $.notify('Lưu thông tin thành công!', "success");
                setTimeout(function () {
                    hide_loading();
                    window.location.href = data.url;
                },2000);
            } else {
                hide_loading();
                $.notify(data.msg, "warn");
            }
        },
        error      : function (a, b, c) {
            hide_loading();
            console.log(a + b + c);
        }
    });
}

//

$(document).on("click", ".btn-upload-audio", function () {
    $('#src-audio').trigger('click');
});

$(document).on("change", "#src-audio", function (e) {
    read_audio(e);
    show_button_upload_audio();
});

function read_audio(event) {
    let files = event.target.files;
    let type = files[0].type;
    type = type.split('/');
    if (type[0] === 'audio') {
        $("#audio-preview").attr("src", URL.createObjectURL(files[0]));
        $("#preview-audio").load();
    } else {
        $.notify('Không đúng dịnh dạng', "warn");
    }
}

function show_button_upload_audio() {
    let av = $('#src-audio').val();
    if (av) {
        $('.btn-remove-audio').prop('disabled', false);
        $('#preview-audio').show();
    } else {
        $('.btn-remove-audio').prop('disabled', true);
        $('#preview-audio').hide();
    }
}
