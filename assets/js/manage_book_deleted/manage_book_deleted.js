$(document).ready(function(){
	$(document).on("click", ".btn-restore-book", function () {
		let url = $(this).attr('data-url');
		$('.btn-restore-data-book').attr('data-url', url);
		$('.restore-popup-modal').modal('show');
	});

	$(document).on("click", ".btn-delete-book", function () {
		let url = $(this).attr('data-url');
		$('.btn-delete-data-book').attr('data-url', url);
		$('.popup-delete-modal').modal('show');
	});

	$(document).on("click", ".btn-restore-data-book", function () {
		let url = $(this).attr('data-url');
		$.ajax({
			url        : url,
			type       : "GET",
			dataType   : 'json',
			beforeSend : function () {
				show_loading();
				$('.restore-popup-modal').modal('hide');
			},
			success    : function (data) {
				let status = data.status;
				$.notify(data.msg, status ? 'success' : 'warn');
				ajaxData($('.data-table'));
				hide_loading();
			},
			error      : function (a, b, c) {
				console.log(a + b + c);
				hide_loading();
			}
		});
	});

	$(document).on("click", ".btn-delete-data-book", function () {
		let url = $(this).attr('data-url');
		$.ajax({
			url        : url,
			type       : "GET",
			dataType   : 'json',
			beforeSend : function () {
				show_loading();
				$('.popup-delete-modal').modal('hide');
			},
			success    : function (data) {
				let status = data.status;
				$.notify(data.msg, status ? 'success' : 'warn');
				ajaxData($('.data-table'));
				hide_loading();
			},
			error      : function (a, b, c) {
				console.log(a + b + c);
				hide_loading();
			}
		});
	});
});
