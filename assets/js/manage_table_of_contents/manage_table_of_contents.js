$(document).ready(function () {
	$(document).on("change", "#nameTableOfContents", function () {
		let value = $(this).val();
		let url = $(this).attr('data-url');
		let self = this;
		$.ajax({
			url       : url,
			type      : "POST",
			data      : {
				content: value
			},
			dataType  : "json",
			beforeSend: function () {
				show_loading();
			},
			success   : function (data) {
				$(".list-table-of-contents").append(data.html);
				$(self).val('');
				remove_form();
			},
			error     : function (a, b, c) {
				console.log(a + b + c);
			},
			complete  : function () {
				hide_loading();
			}
		});
	});

	$(document).on("click", ".btn-create-table-of-contents", function () {
		remove_form();
		$(this).parents().find('.form-add-contents-main').show();
	});

	$(document).on("change", ".name-table-of-contents-child", function () {
		let self = this;
		let value = $(self).val();
		let url = $(self).attr('data-url');
		let data_insert = $(self).attr('data-insert');
		$.ajax({
			url       : url,
			type      : "POST",
			data      : {
				content: value
			},
			dataType  : "json",
			beforeSend: function () {
				show_loading();
			},
			success   : function (data) {
				if (data_insert === 'insert-later') {
					$(self).parents('.form-add-child-contents:first').after(data.html);
				} else if (data_insert === 'insert-before') {
					$(self).parents('.form-add-child-contents:first').before(data.html);
				} else {
					$(self).parents('.list-table-of-contents-main:first').append(data.html);
				}
				$(self).val('');
				remove_form();
			},
			error     : function (a, b, c) {
				console.log(a + b + c);
			},
			complete  : function () {
				hide_loading();
			}
		});
	});

	$(document).on("click", ".btn-remove-content", function () {
		remove_form();
		$(this).parents('.list-table-of-contents-main:first').remove();
	});

	$(document).on("click", ".btn-remove-child-content", function () {
		remove_form();
		$(this).parents('.list-table-of-contents-child:first').remove();
	});

	$(document).on("click", ".btn-edit-child-contents", function () {
		remove_form();
		let list_table = $(this).parents('.list-table-of-contents-child:first');
		list_table.find('.form-edit-child-contents').show();
	});

	$(document).on("change", ".list-contents-child", function () {
		$(this).parents('.list-table-of-contents-child:first').find('label').text($(this).val());
		remove_form();
	});

	$(document).on("click", ".btn-edit-contents", function () {
		remove_form();
		let list_table = $(this).parents('.list-table-of-contents-main:first');
		list_table.find('.form-edit-main-contents').show();
	});

	$(document).on("change", ".list-contents-main", function () {
		$(this).parents('.list-table-of-contents-main:first').find('label').text($(this).val());
		remove_form();
	});

	$(document).on("click", ".btn-insert-before-contents", function () {
		let value = 'insert-before';
		let self = this;
		get_form_insert_main(value, self);
	});

	$(document).on("click", ".btn-insert-later-contents", function () {
		let value = 'insert-later';
		let self = this;
		get_form_insert_main(value, self);
	});

	$(document).on("change", ".name-table-of-contents-main", function () {
		let self = this;
		let value = $(self).val();
		let url = $(self).attr('data-url');
		let data_insert = $(self).attr('data-insert');
		$.ajax({
			url       : url,
			type      : "POST",
			data      : {
				content: value
			},
			dataType  : "json",
			beforeSend: function () {
				show_loading();
			},
			success   : function (data) {
				if (data_insert === 'insert-later') {
					$(self).parents('.form-add-contents-main:first').after(data.html);
				} else {
					$(self).parents('.form-add-contents-main:first').before(data.html);
				}
				$(self).val('');
				remove_form();
			},
			error     : function (a, b, c) {
				console.log(a + b + c);
			},
			complete  : function () {
				hide_loading();
			}
		});
	});

	$(document).on("click", ".btn-add-child-content", function () {
		remove_form();
		let self = $(this);
		let url = $(self).attr('data-url');
		let value = '';
		$.ajax({
			url       : url,
			type      : "POST",
			data      : {
				value : value
			},
			dataType  : "json",
			beforeSend: function () {
				show_loading();
			},
			success   : function (data) {
				$(self).parents('.list-table-of-contents-main:first').append(data.html);
			},
			error     : function (a, b, c) {
				console.log(a + b + c);
			},
			complete  : function () {
				hide_loading();
			}
		});
	});

	$(document).on("click", ".btn-insert-before-contents-child", function () {
		get_form_insert_child('insert-before', this);
	});

	$(document).on("click", ".btn-insert-later-contents-child", function () {
		get_form_insert_child('insert-later', this);
	});

	$(document).on("click", ".btn-save-table-of-contents", function () {
		let value = $('.list-table-of-contents').html();
		if (value) {
			let data = [];
			$('.list-contents-main').each(function (index, value) {
				let main = $(this).val();
				let child = get_table_of_contents_child(this);
				let arr = {main:main, child:child};
				data.push(arr);
			});
			if (data) {
				data = JSON.stringify(data);
				$('.form-control-button').remove();
				ajax_save_table_of_contents(data,value);
			} else {
				$.notify('Chưa có danh mục!');
			}
		} else {
			$.notify('Chưa có danh mục!');
		}
	});
});

function remove_form() {
	$('.form-add-contents-main').hide();
	$('.form-add-child-contents').remove();
	$('.form-edit-main-contents').hide();
	$('.form-edit-child-contents').hide();
}

function get_form_insert_main(value, self) {
	remove_form();
	let url = $(self).attr('data-url');
	$.ajax({
		url       : url,
		type      : "POST",
		data      : {
			value : value
		},
		dataType  : "json",
		success   : function (data) {
			if (value === 'insert-later') {
				$(self).parents('.list-table-of-contents-main:first').after(data.html);
			} else {
				$(self).parents('.list-table-of-contents-main:first').before(data.html);
			}
		},
		error     : function (a, b, c) {
			console.log(a + b + c);
		},
		complete  : function () {
			hide_loading();
		}
	});
}

function get_form_insert_child(value, self) {
	remove_form();
	let url = $(self).attr('data-url');
	$.ajax({
		url       : url,
		type      : "POST",
		data      : {
			value : value
		},
		dataType  : "json",
		beforeSend: function () {
			show_loading();
		},
		success   : function (data) {
			if (value === 'insert-later') {
				$(self).parents('.list-table-of-contents-child:first').after(data.html);
			} else {
				$(self).parents('.list-table-of-contents-child:first').before(data.html);
			}
		},
		error     : function (a, b, c) {
			console.log(a + b + c);
		},
		complete  : function () {
			hide_loading();
		}
	});
}

function get_table_of_contents_child(self) {
	let child = $(self).parents('.list-table-of-contents-main:first').find('.list-contents-child');
	let data = [];
	child.each(function (index, value) {
		data.push($(this).val());
	});
	return data;
}

function ajax_save_table_of_contents(data, value) {
	let url = $('.btn-save-table-of-contents').attr('data-url');
	let book_id = $('#bookId').val();
	if (book_id) {
		$.ajax({
			url: url,
			type: "POST",
			data: {
				book_id:book_id,
				data: data,
				value: value
			},
			dataType: "json",
			beforeSend: function () {
				show_loading();
			},
			success: function (data) {
				let status = data.status;
				if (status) {
					$.notify('Lưu thông tin thành công!', "success");
					setTimeout(function () {
						hide_loading();
						window.location.href = data.url;
					}, 2000);
				} else {
					hide_loading();
					$.notify(data.msg, "warn");
				}
			},
			error: function (a, b, c) {
				$.notify("Đã có lỗi xảy ra vui lòng thử lại!", 'warn');
				hide_loading();
				console.log(a + b + c);
			}
		});
	} else {
		$.notify("Đã có lỗi xảy ra vui lòng thử lại!", 'warn');
	}
}
