$(document).ready(function(){
	let data_remove = {};
	// Click button upload file
	$(document).on('click', '.btn-upload-file-pdf', function () {
		$('.input-file-pdf').trigger('click');
	});

	$(document).on('click', '.icon-no-preview', function () {
		$(this).hide();
		$(this).parents('.row-item-toc').find('.tick-preview-toc').show();
		$(this).parents('.row-item-toc').find('.check-proofread').prop('checked', true);
	});

	$(document).on('click', '.tick-preview-toc', function () {
		$(this).hide();
		$(this).parents('.row-item-toc').find('.icon-no-preview').show();
		$(this).parents('.row-item-toc').find('.check-proofread').prop('checked', false);
	});

	//Before choose file
	$(document).on('change', '.input-file-pdf', function () {
		let size = this.files[0].size;
		let type = this.files[0].type;
		let result = true;
		//Check size file
		if (size > 100000000) {
			$.notify('File không được quá 100MB', 'warn');
			result = false;
			}
		//Check type file
		if (type !== 'application/pdf') {
			$.notify('File phải ở định dạng pdf', 'warn');
			result = false;
		}
		//Load muc luc
		if (result) {
			$('.file-html').html(this.files[0].name);
			load_table_of_contents();
		}
	});

	//Click button save
	$(document).on('click', '.btn-save-ebook', function (e) {
		e.preventDefault();
		let data = [];
		//get data cho tung chuong
		$('.row-table-of-contents').each(function () {
			let data_row = get_data_in_row(this, '');
			data.push(data_row);
		});
		//submit form
		save_book(data);
	});

	//Click button upload audio
	$(document).on('click', '.btn-upload-media', function () {
		remove_input();
		let self = this;
		let url = $(self).attr('data-url');
		$.ajax({
			url       : url,
			type      : "GET",
			dataType  : "json",
			beforeSend: function () {
				show_loading();
			},
			success   : function (data) {
				let preview = $(self).parents('.form-preview-media-upload').find('.preview-media-upload');
				preview.append(data.html);
				preview.find('.input-media-upload:last').trigger('click');
			},
			error     : function (a, b, c) {
				console.log(a + b + c);
			},
			complete  : function () {
				hide_loading();
			}
		});
	});

	//Event when input thay đổi
	$(document).on('change', '.input-media-upload', function (e) {
		let type = $('#typeUpload').val();
		ajax_load_upload_audio(e, this, type);
	});

	//Remove audio
	$(document).on('click', '.btn-remove-media', function () {
		let type = $(this).attr('data-type');
		if (type === 'video') {
			let url = $(this).attr('data-url');
			let book_id = $('#bookId').val();
			let table_of_contents = $(this).parents('tr:first').find('.name-table-of-contents').val();
			$.ajax({
				url       : url,
				type      : "POST",
				data	  : {
					book_id: book_id,
					chap_name : table_of_contents
				},
				dataType  : "json",
				beforeSend: function () {
					show_loading();
				},
				success   : function (data) {
					$.notify('Xoá file thành công!', 'success');
				},
				error     : function (a, b, c) {
					console.log(a + b + c);
				},
				complete  : function () {
					hide_loading();
				}
			});
		}
		let r = $(this).parents('.form-preview-media:first').find('.input-media-upload').attr("data-path");
		let toc = $(this).parents('.row-upload-media:first').find('.name-table-of-contents').val();
		data_remove[toc.toString()] = r;
		$(this).parents('.form-preview-media-upload:first').find('.btn-upload-media').show();
		$(this).parents('.form-preview-media:first').remove();
	});

	//Save audio
	$(document).on('click', '.btn-save-media', function (e) {
		e.preventDefault();
		remove_input();
		let data = [];
		//get data cho tung chuong
		$('.row-upload-media').each(function () {
			let data_row = get_data_in_row(this, 'audio');
			data.push(data_row);
		});
		//submit form
		if (data) {
			save_media(data, data_remove);
		} else {
			$.notify('Không có dữ liệu!', 'warn');
		}
	});

	$(document).on('click', '.btn-save-editor-chap', function () {
		let chap_id = $('#chapId').val();
		let chap_description = $('#chapDescription').val();
		let number_page = $('#numberPage').val();
		let chap_data = CKEDITOR.instances['chap_data'].getData();
		if (chap_id) {
			let data = {};
			data['chap_id'] = chap_id;
			data['chap_description'] = chap_description;
			data['number_page'] = number_page;
			data['chap_data'] = chap_data;
			ajax_save_editor_chap(data);
		} else {
			$.notify('Đã có lỗi xảy ra!', 'warn');
		}
	});

	$(document).on('click', '.btn-save-editor', function () {
		let book_editor_id = $('.book-editor-id').val();
		let book_id = $('.book-id').val();
		if (book_editor_id && book_id) {
			let data_toc = [];
			$('.row-table-of-contents').each(function () {
				let item = {};
				item['toc_id'] = $(this).find($('.toc-id')).val();
				item['toc_name'] = $(this).find($('.name-table-of-contents')).val();
				item['chap_id'] = $(this).find($('.chap-id')).val();
				item['allow_preview'] = 0;
				if ($(this).find($('.check-proofread')).is(':checked')) {
					item['allow_preview'] = 1;
				}
				data_toc.push(item);
			});
			let data = {};
			data['book_editor_id'] = book_editor_id;
			data['data_toc'] = data_toc;
			data['book_id'] = book_id;
			ajax_save_editor_toc(data);
		} else {
			$.notify('Đã có lỗi xảy ra!', 'warn');
		}
	});

	$(document).on('click', '.btn-save-paper', function () {
		let book_paper_id = $('.book-paper-id').val();
		let book_id = $('.book-id').val();
		if (book_id) {
			let data_toc = [];
			$('.row-table-of-contents').each(function () {
				let item = {};
				item['paper_toc_id'] = $(this).find($('.paper-toc-id')).val();
				item['toc_name'] = $(this).find($('.name-table-of-contents')).val();
				item['allow_preview'] = 0;
				if ($(this).find($('.check-proofread')).is(':checked')) {
					item['allow_preview'] = 1;
				}
				data_toc.push(item);
			});
			let data = {};
			data['book_paper_id'] = book_paper_id;
			data['data_toc'] = data_toc;
			data['book_id'] = book_id;
			ajax_save_paper_toc(data);
		} else {
			$.notify('Đã có lỗi xảy ra!', 'warn');
		}
	});

	$(document).on('change', '.option-upload-epub', function () {
		let v = $(this).val();
		$('.toc_table').remove();
		load_input_upload_epub(v);
	});

	$(document).on('click', '.div-epub', function () {
		let bl = $(this).find('.text-file-name').hasClass("bg-info");
		$(".text-file-name").removeClass("bg-info");
		$(".text-file-name").addClass("bg-primary");
		if (!bl) {
			$(this).find('.text-file-name').addClass("bg-info");
			$(this).find('.text-file-name').removeClass("bg-primary");
			let length = $('.toc_table').length;
			if (length === 0) {
				load_table_of_contents();
			}
		} else {
			$('.toc_table').remove();
		}
	});

	$(document).on('click', '.btn-upload-file-epub', function () {
		$('.input-file-epub').trigger('click');
	});

	$(document).on('change', '.input-file-epub', function () {
		let size = this.files[0].size;
		let type = this.files[0].type;
		type = type.split('/');
		type = type[1].split('+');
		let result = true;
		//Check size file
		if (size > 100000000) {
			$.notify('File không được quá 100MB', 'warn');
			result = false;
		}
		//Check type file
		if (type[0] !== 'epub') {
			$.notify('File phải ở định dạng pdf', 'warn');
			result = false;
		}
		//Load muc luc
		if (result) {
			load_table_of_contents();
		}
	});

	$(document).on('click', '.btn-save-epub', function () {
		let v = '2';
		$('.option-upload-epub').each(function () {
			if ($(this).is(':checked')) {
				v = $(this).val();
			}
		});
		if (v === '1') {
			ajax_save_epub_upload(v);
		} else {
			ajax_save_epub(v);
		}
	});
});

//Load muc luc
function load_table_of_contents() {
	let url = $('.data-table-of-contents').attr('data-url');
	let book_id = $('#bookId').val();
	$.ajax({
		url       : url,
		type      : "POST",
		data	  : {
			book_id: book_id
		},
		dataType  : "json",
		beforeSend: function () {
			show_loading();
		},
		success   : function (data) {
			$(".ajax-data-table-of-contents").html(data.html);
		},
		error     : function (a, b, c) {
			console.log(a + b + c);
		},
		complete  : function () {
			hide_loading();
		}
	});
}

//Check file
function validate_file() {
	let value = $('.input-file-pdf').val();
	let result = true;
	if (!value) {
		result = false;
	}
	return result;
}

//Get data cho tung chuong
function get_data_in_row(self, type) {
	let data = {};
	let table_of_contents = $(self).find('.name-table-of-contents').val();
	let main = $(self).find('.name-main').val();
	let check_proofread = $(self).find('.check-proofread');
	if (check_proofread.is(":checked")) {
		check_proofread = 1;
	} else {
		check_proofread = 0;
	}
	if (type !== 'audio') {
		let page_from = $(self).find('.page-from').val();
		let page_to = $(self).find('.page-to').val();
		data['page_from'] = page_from;
		data['page_to'] = page_to;
	}
	data['table_of_contents'] = table_of_contents;
	data['main'] = main;
	data['check_proofread'] = check_proofread;
	return data;
}

//Ajax save book
function save_book(data) {
	let url = $('.btn-save-ebook').attr('data-url');
	let upload = new FormData();
	let book_id = $('#bookId').val();
	let data_book = JSON.stringify(data);
	upload.append( 'file', $('.input-file-pdf')[0].files[0] );
	upload.append('data', data_book);
	upload.append('book_id', book_id);
	if (book_id) {
		$.ajax({
			url       : url,
			type      : "POST",
			data	  : upload,
			contentType: false,
			cache:false,
			processData:false,
			beforeSend: function () {
				show_loading();
			},
			success   : function (data) {
				$.notify('Lưu thành công!', 'success');
				setTimeout(function () {
					hide_loading();
					window.location.href = data;
				}, 3000);
			},
			error     : function (a, b, c) {
				hide_loading();
				console.log(a + b + c);
			}
		});
	} else {
		$.notify('Đã có lỗi xảy ra!', 'warn');
	}
}

//Get load audio
function ajax_load_upload_audio(event, self, type) {
	let url = $('.preview-media-upload').attr('data-url');
	let src = read_media(event, type);
	let val = $(self).val();
	let book_id = $('#bookId').val();
	let size = $(self)[0].files[0].size;
	let duration = $(self)[0].duration;
	let table_of_contents = $(self).parents('tr:first').find('.name-table-of-contents').val();
	let form_data = new FormData;
	form_data.append('book_id', book_id);
	form_data.append('url', url);
	form_data.append('data', src);
	form_data.append('type', type);
	form_data.append('value', val);
	form_data.append('size', size);
	form_data.append('duration', duration);
	form_data.append('chap_data', table_of_contents);
	form_data.append('file', $(self)[0].files[0]);
	if (src) {
		$.ajax({
			url 	: url,
			type	: "POST",
			data	: form_data,
			contentType: false,
			cache:false,
			processData:false,
			dataType: "json",
			beforeSend: function () {
				show_loading();
			},
			success: function (data) {
				$(self).parents('.form-preview-media-upload').find('.form-preview-media:last').append(data.html);
				$(self).parents('.form-preview-media-upload').find('.btn-upload-media').hide();
				let height = $('.item-table-toc-data').height();
				$('.item-table-toc').height(height);
			},
			error: function (a, b, c) {
				console.log(a + b + c);
			},
			complete: function () {
				hide_loading();
			}
		});
	}
}

//Load audio
function read_media(event, type) {
	let files = event.target.files;
	let type_upload = files[0].type;
	let size = files[0].size;
	type_upload = type_upload.split('/');
	if (type_upload[0] === type) {
		if (size > 100000000) {
			$.notify('Kích thước file không được quá 100MB', "warn");
			remove_input();
			return false;
		} else {
			return URL.createObjectURL(files[0]);
		}
	} else {
		$.notify('Không đúng dịnh dạng', "warn");
		remove_input();
		return false;
	}
}

//Check upload audio
function validate_file_audio() {
	let value = $('.input-media-upload').length;
	let result = true;
	if (!value) {
		result = false;
	}
	return result;
}

//Save media
function save_media(data, data_remove) {
	let form_data = new FormData();
	let type = $('#typeUpload').val();
	if (type === 'audio') {
		let number_media = 0;
		let duration = 0;
		let size = 0;
		$('.row-upload-media').each(function () {
			let table_of_contents = $(this).find('.name-table-of-contents').val();
			let upload = $(this).find('.input-media-upload');
			let file_upload = $(this).find('.preview-media');
			let i = 0;
			let j = 0;
			upload.each(function () {
				if ($(this).attr('data-size')) {
					size += $(this).attr('data-size');
				} else {
					size += $(this)[0].files[0].size ? $(this)[0].files[0].size : 0;
				}
				form_data.append(table_of_contents + '_' + i, $(this)[0].files[0]);
				i++;
			});
			file_upload.each(function () {
				duration += $(this)[0].duration ? $(this)[0].duration : 0;
				form_data.append(table_of_contents + '_duration_' + j, $(this)[0].duration);
				j++;
				number_media++;
			});
		});
		form_data.append('duration', duration);
		form_data.append('size', size);
		form_data.append('number_media', number_media);
		form_data.append('data_remove', JSON.stringify(data_remove));
	}
	let book_id = $('#bookId').val();
	form_data.append('data', JSON.stringify(data));
	form_data.append('book_id', book_id);
	form_data.append('type', type);
	if (book_id) {
		ajax_save_media(form_data);
	} else {
		$.notify('Đã có lỗi xảy ra!', 'warn');
	}
}

//remove input
function remove_input() {
	$('.input-media-upload').each(function () {
		let v = $(this).val();
		let size = $(this).attr('data-size');
		if (!v && !size) {
			$(this).parents('.form-preview-media').remove();
		}
	});
}

function ajax_save_media(form_data) {
	let url = $('.btn-save-media').attr('data-url');
	$.ajax({
		url: url,
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		beforeSend: function () {
			show_loading();
		},
		success: function (data) {
			$.notify('Lưu thành công!', 'success');
			setTimeout(function () {
				hide_loading();
				window.location.href = data;
			}, 3000);
		},
		error: function (a, b, c) {
			hide_loading();
			console.log(a + b + c);
		}
	});
}

function ajax_save_editor_chap(data) {
	let url = $('.btn-save-editor-chap').attr('data-url');
	$.ajax({
		url: url,
		type: "POST",
		data: data,
		dataType: 'json',
		beforeSend: function () {
			show_loading();
		},
		success: function (data) {
			if (data.status) {
				$.notify('Lưu thành công!', 'success');
				setTimeout(function () {
					hide_loading();
					window.location.href = data.url;
				}, 2000);
			} else {
				hide_loading();
				$.notify('Đã có lỗi xảy ra vui lòng thử lại!', 'warn');
			}
		},
		error: function (a, b, c) {
			hide_loading();
			$.notify('Đã có lỗi xảy ra vui lòng thử lại!', 'warn');
			console.log(a + b + c);
		}
	});
}

function ajax_save_editor_toc(data) {
	let url = $('.btn-save-editor').attr('data-url');
	$.ajax({
		url: url,
		type: "POST",
		data: data,
		dataType: 'json',
		beforeSend: function () {
			show_loading();
		},
		success: function (data) {
			if (data.status) {
				$.notify('Lưu thành công!', 'success');
				setTimeout(function () {
					hide_loading();
					window.location.href = data.url;
				}, 2000);
			} else {
				hide_loading();
				$.notify('Đã có lỗi xảy ra vui lòng thử lại!', 'warn');
			}
		},
		error: function (a, b, c) {
			hide_loading();
			$.notify('Đã có lỗi xảy ra vui lòng thử lại!', 'warn');
			console.log(a + b + c);
		}
	});
}

function ajax_save_paper_toc(data) {
	let url = $('.btn-save-paper').attr('data-url');
	$.ajax({
		url: url,
		type: "POST",
		data: data,
		dataType: 'json',
		beforeSend: function () {
			show_loading();
		},
		success: function (data) {
			if (data.status) {
				$.notify('Lưu thành công!', 'success');
				setTimeout(function () {
					hide_loading();
					window.location.href = data.url;
				}, 2000);
			} else {
				hide_loading();
				$.notify('Đã có lỗi xảy ra vui lòng thử lại!', 'warn');
			}
		},
		error: function (a, b, c) {
			hide_loading();
			$.notify('Đã có lỗi xảy ra vui lòng thử lại!', 'warn');
			console.log(a + b + c);
		}
	});
}

function load_input_upload_epub(v) {
	let url = $('.select-checkbox').attr('data-url');
	$.ajax({
		url: url,
		type: "POST",
		data: {
			data: v
		},
		dataType: 'json',
		beforeSend: function () {
			show_loading();
		},
		success: function (data) {
			if (data.status) {
				$('.form-upload-epub').html(data.html);
			} else {
				hide_loading();
				$.notify('Đã có lỗi xảy ra vui lòng thử lại!', 'warn');
			}
		},
		error: function (a, b, c) {
			$.notify('Đã có lỗi xảy ra vui lòng thử lại!', 'warn');
			console.log(a + b + c);
		},
		complete: function () {
			hide_loading();
		}
	});
}

function ajax_save_epub_upload(v) {
	let url = $('.btn-save-epub').attr('data-url');
	let book_id = $('#bookId').val();
	let form_data = new FormData();
	let toc = get_check_proofread();
	form_data.append('book_id', book_id);
	form_data.append('type', v);
	form_data.append('file', $('.input-file-epub')[0].files[0]);
	form_data.append('toc', toc);
	$.ajax({
		url: url,
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		beforeSend: function () {
			show_loading();
		},
		success: function (data) {
			$.notify('Lưu thành công!', 'success');
			setTimeout(function () {
				hide_loading();
				window.location.href = data;
			}, 3000);
		},
		error: function (a, b, c) {
			hide_loading();
			console.log(a + b + c);
		}
	});
}

function ajax_save_epub(v) {
	let file_id = 0;
	$('.text-file-name').each(function () {
		if ($(this).hasClass('bg-info')) {
			file_id = $(this).attr('data-id');
		}
	});
	let url = $('.btn-save-epub').attr('data-url');
	let book_id = $('#bookId').val();
	let toc = get_check_proofread();
	let data = {};
	data['book_id'] = book_id;
	data['type'] = v;
	data['file_id'] = file_id;
	data['toc'] = toc;
	$.ajax({
		url: url,
		type: "POST",
		data: data,
		beforeSend: function () {
			show_loading();
		},
		success: function (data) {
			$.notify('Lưu thành công!', 'success');
			setTimeout(function () {
				hide_loading();
				window.location.href = data;
			}, 3000);
		},
		error: function (a, b, c) {
			hide_loading();
			console.log(a + b + c);
		}
	});
}

function get_check_proofread() {
	let data_pf = [];
	//get data cho tung chuong
	$('.row-table-of-contents').each(function () {
		let data = {};
		let table_of_contents = $(this).find('.name-table-of-contents').val();
		let check_proofread = $(this).find('.check-proofread');
		if (check_proofread.is(":checked")) {
			check_proofread = 1;
		} else {
			check_proofread = 0;
		}
		data['table_of_contents'] = table_of_contents;
		data['check_proofread'] = check_proofread;
		data_pf.push(data);
	});
	return JSON.stringify(data_pf);
}
