$(document).ready(function () {
	$(document).on("click", ".btn-save-user", function () {
		let username = $('input[name="username"]').val();
		let password = $('#password').val();
		let full_name = $('#full_name').val();
		let email = $('#email').val();
		let role_alias = $('#role_alias').val();
		let re_password = $('#re_password').val();
		let id = $('#id').val();
		if (username && password && re_password && email) {
			if (password !== re_password) {
				$.notify('Mật khẩu không trùng nhau!', 'warn');
				return false;
			} else {
				let data = {};
				data['username'] = username;
				data['password'] = password;
				data['re_password'] = password;
				data['full_name'] = full_name;
				data['email'] = email;
				data['role_alias'] = role_alias;
				if (id) {
					data['id'] = id;
				}
				ajax_save_user(data);
			}
		} else {
			$.notify('Vui lòng điền đầy đủ thông tin!', 'warn');
			return false;
		}
	});
});

function ajax_save_user(data) {
	let url = $('.btn-save-user').attr('data-url');
	$.ajax({
		url        : url,
		type       : "POST",
		data       : data,
		dataType   : 'json',
		beforeSend : function () {
			show_loading();
		},
		success    : function (data) {
			let status = data.status;
			if (status) {
				$.notify('Lưu thông tin thành công!', "success");
				setTimeout(function () {
					hide_loading();
					window.location.href = data.url;
				},2000);
			} else {
				hide_loading();
				$.notify(data.msg, "warn");
			}
		},
		error      : function (a, b, c) {
			hide_loading();
			$.notify('Có lỗi xảy ra vui lòng thử lạib', "warn");
			console.log(a + b + c);
		}
	});
}
