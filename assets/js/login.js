/**
 * Created by miunh_nim on 11-Feb-19.
 */
$(document).ready(function (e) {
	hide_loading();
	// send request reset password
	$(document).on('click', '.btn-request-reset', function (e) {
		var form = $('.form-request-reset');
		var url = form.attr('data-url');
		var data = {
			email: form.find('#email').val(),
		};
		$.ajax({
			url       : url,
			type      : "POST",
			data      : data,
			dataType  : "json",
			beforeSend: function () {
				show_loading();
			},
			success   : function (dataAll) {
				if (dataAll.status) {
					$.notify(dataAll.msg, "success");
				} else {
					$.notify(dataAll.msg, "warn");
				}
			},
			error     : function (a, b, c) {
				alert(a + b + c);
				// window.location = url;
			},
			complete  : function () {
				hide_loading();
			}
		});
	})

	// save new password
	$(document).on('click', '.btn-submit-reset', function (e) {
		var form = $('.form-reset-pass');
		var url = form.attr('data-url');
		var data = {
			password   : form.find('#password').val(),
			re_password: form.find('#re_password').val(),
		};
		$.ajax({
			url       : url,
			type      : "POST",
			data      : data,
			dataType  : "json",
			beforeSend: function () {
				show_loading();
			},
			success   : function (dataAll) {
				if (dataAll.status) {
					$.notify(dataAll.msg, "success");
					setTimeout(function () {
						window.location = dataAll.redirect_url;
					}, 1000);
				} else {
					$.notify(dataAll.msg, "warn");
				}
			},
			error     : function (a, b, c) {
				alert(a + b + c);
				// window.location = url;
			},
			complete  : function () {
				hide_loading();
			}
		});
	});

	$(document).on('click', '.btn-check-login', function (e) {
		let data = {};
		let username = $('#username').val();
		let password = $('#password').val();
		let url = $(this).attr('data-url');
		data['username'] = username;
		data['password'] = password;
		$.ajax({
			url       : url,
			type      : "POST",
			data      : data,
			dataType  : "json",
			beforeSend: function () {
				show_loading();
			},
			success   : function (data) {
				if (data.status) {
					setTimeout(function () {
						$.notify("Đăng nhập thành công", "success");
						window.location.href = data.url;
					},2000);
				} else {
					$.notify(data.msg, "warn");
					hide_loading();
				}
			},
			error     : function (a, b, c) {
				console.log(a + b + c);
				$.notify("tài khoản hoặc mật khẩu không chính xác", "warn");
				hide_loading();
				// window.location = url;
			}
		});
	})
});
