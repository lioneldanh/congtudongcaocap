$(document).ready(function(){
	if (!$('#image-audio')[0].defaultValue) {
		$('.btn-remove-image-audio').prop('disabled', true);
		$('#image-audio-preview').hide();
	}

	if (!$('#src-audio')[0].defaultValue) {
		$('.btn-remove-audio').prop('disabled', true);
		$('#preview-audio').hide();
	}

	$(document).on("click", ".btn-upload-image-audio", function () {
		$('#image-audio').trigger('click');
	});

	$(document).on("change", "#image-audio", function () {
		readURL(this);
		show_image();
	});

	$(document).on("click", ".btn-upload-audio", function () {
		$('#src-audio').trigger('click');
	});

	$(document).on("change", "#src-audio", function (e) {
		read_audio(e);
		show_button_upload_audio();
	});

});

function show_image() {
	let av = $('#image-audio').val();
	if (av) {
		$('.btn-remove-image-audio').prop('disabled', false);
		$('#image-audio-preview').show();
	} else {
		$('.btn-remove-image-audio').prop('disabled', true);
		$('#image-audio-preview').hide();
	}
}

function readURL(input) {
	if (input.files && input.files[0]) {
		let reader = new FileReader();

		reader.onload = function(e) {
			$('#image-audio-preview').attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}

function show_button_upload_audio() {
	let av = $('#src-audio').val();
	if (av) {
		$('.btn-remove-audio').prop('disabled', false);
		$('#preview-audio').show();
	} else {
		$('.btn-remove-audio').prop('disabled', true);
		$('#preview-audio').hide();
	}
}

function read_audio(event) {
	let files = event.target.files;
	let type = files[0].type;
	type = type.split('/');
	if (type[0] === 'audio') {
		$("#audio-preview").attr("src", URL.createObjectURL(files[0]));
		$("#preview-audio").load();
	} else {
		$.notify('Không đúng dịnh dạng', "warn");
	}
}
