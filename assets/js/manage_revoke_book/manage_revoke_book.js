$(document).ready(function () {
	$(document).on("click", ".btn-save-revoke", function () {
		let comment = $('.comment').val();
		let length = comment.length;
		if (length === 0) {
			$.notify('Vui lòng điền lý do!', 'warn');
			return false;
		} else if (length > 500) {
			$.notify('Lý do tối đa 500 ký tự!', 'warn');
			return false;
		} else {
			$('.popup-modal-revoke-book').modal('show');
		}
	});

	$(document).on("click", ".btn-revoke-book-data", function () {
		$('.popup-modal-revoke-book').modal('hide');
		let book_id = $('#bookId').val();
		let comment = $('.comment').val();
		let url = $(this).attr('data-url');
		$.ajax({
			url       : url,
			type      : "POST",
			data      : {
				comment : comment,
				book_id: book_id
			},
			dataType  : "json",
			beforeSend: function () {
				show_loading();
			},
			success   : function (data) {
				let status = data.status;
				if (status) {
					$.notify('Lưu thông tin thành công!', "success");
					setTimeout(function () {
						hide_loading();
						window.location.href = data.url;
					},2000);
				} else {
					hide_loading();
					$.notify(data.msg, "warn");
				}
			},
			error     : function (a, b, c) {
				hide_loading();
				console.log(a + b + c);
			}
		});
	});
});
