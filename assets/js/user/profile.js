$(document).ready(function () {
    $(document).on('click', '.e_submit_form', update_data);
});

function update_data(e) {
    e.preventDefault();
    var form = $(this).closest('form');
    var validator = form.validate(
        {
            rules: {
                confirm_password: {
                    equalTo: "#password"
                }
            }
        }
    );
    if (validator.form()) {
        var url = form.attr('action');
        var form_Data = new FormData();
        var data = $('.e_form_profile').serializeArray();
        $.each(data, function (i, field) {
            form_Data.append(field.name, field.value);
        });
        form_Data.append('image', $('input[type=file]')[0].files[0]);
        $.ajax({
            url        : url,
            type       : "POST",
            data       : form_Data,
            dataType   : "json",
            contentType: false,
            processData: false,
            beforeSend : function () {
                show_loading();
            },
            success    : function (data) {
                console.log(data.msg);
                if (data.status) {
                    $.notify(data.msg, "success");
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                } else {
                    $.notify(data.msg, "warn");
                }
            },
            error      : function (a, b, c) {
                alert(a + b + c);
                // window.location = url;
            },
            complete   : function () {
                hide_loading();
            }
        });
    }
}
