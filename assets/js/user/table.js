$(document).ready(function (e) {
	// save add/edit user
	$(document).on('click', '#modalUser .btn-submit-form', saveAdd);
});

/**
 * save add/edit
 * @param e
 */
function saveAdd(e) {
	var form = $('.form-user');
	var url = form.attr('data-url');
	// return;
	if (!form.find('.username').val() || !form.find('.email').val()) {
		$.notify('Vui lòng nhập đủ các thông tin bắt buộc', "warn");
		return;
	}


	var data = {
		fullname   : form.find('.fullname').val(),
		username   : form.find('.username').val(),
		email      : form.find('.email').val(),
		password   : form.find('.password').val(),
		re_password: form.find('.re_password').val()
	};
	$.ajax({
		url       : url,
		type      : "POST",
		data      : data,
		dataType  : "json",
		beforeSend: function () {
			show_loading();
		},
		success   : function (dataAll) {
			if (dataAll.status) {
				$.notify(dataAll.msg, "success");
				ajaxData($('.data-table'));
			} else {
				$.notify(dataAll.msg, "warn");
			}
		},
		error     : function (a, b, c) {
			alert(a + b + c);
			// window.location = url;
		},
		complete  : function () {
			hide_loading();
		}
	});
}
