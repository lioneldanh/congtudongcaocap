$(document).ready(function(){
	$(document).on("click", ".btn-save-category", function () {
		let id = $('#id').val();
		let categoryNameVn = $('#categoryNameVn').val();
		let categoryNameEn = $('#categoryNameEn').val();
		let status = $('.form-status:checked').val();
		if (categoryNameEn && categoryNameVn) {
			let url = $(this).attr('data-url');
			let data = {};
			data['category_name_vn'] = categoryNameVn;
			data['category_name_en'] = categoryNameEn;
			data['status'] = status;
			data['id'] = id;
			$.ajax({
				url: url,
				type: "POST",
				data: data,
				dataType: "json",
				beforeSend: function () {
					show_loading();
				},
				success: function (data) {
					if (data.status) {
						$.notify("Lưu thông tin thành công", "success");
						setTimeout(function () {
							hide_loading();
							window.location.href = data.url;
						}, 2000);
					} else {
						hide_loading();
						$.notify(data.msg, "warn");
					}
				},
				error: function (a, b, c) {
					debugger;
					hide_loading();
					$.notify("Đã có lỗi xảy ra vui lòng thử lại!", "warn");
					alert(a + b + c);
					// window.location = url;
				}
			});
		} else {
			$.notify('Vui lòng điền đầy đủ thông tin', 'warn');
		}
	});

});
