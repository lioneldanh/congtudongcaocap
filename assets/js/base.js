$(document).ready(function () {
	$('.modal').attr('data-backdrop','static');
	$('.modal').attr('data-keyboard','false');
    $('input,textarea').prop('autocomplete','off');
	$('.datetimepicker').datepicker({
        dateFormat: 'dd-mm-yy',
	});
	$(document).on('click', '.e_choose_image', function (e) {
		// $(this).closest('.e_content_image').find('input.e_input_image').click();
		$('.e_input_image').click();
	});


	$(document).mouseup(function (e) {
		var container = $('#left-panel');
		// if the target of the click isn't the container nor a descendant of the container
		if (!container.is(e.target) && container.has(e.target).length === 0 && e.target.id != 'btnMenu' && e.target.id != 'btnMenuOpen' && e.target.id != 'btnMenuClose') {
			var btnMenu = $('#btnMenu');
			if (btnMenu.attr('data-open') == '1') {
				container.fadeOut('slow');
				btnMenu.attr('data-open', '0');
				btnMenu.find('i.icon-open').addClass('hidden');
				btnMenu.find('i.icon-close').removeClass('hidden');
			}
		}
	});

	// show hide menu left
	$(document).on('click', '#btnMenu', function (e) {
		var btnMenu = $(this);
		var menuLeft = $('#left-panel');
		if (btnMenu.attr('data-open') == '1') {
			menuLeft.fadeOut('slow');
			btnMenu.attr('data-open', '0');
			btnMenu.find('i.icon-open').addClass('hidden');
			btnMenu.find('i.icon-close').removeClass('hidden');
		} else {
			menuLeft.fadeIn('slow');
			btnMenu.attr('data-open', '1');
			btnMenu.find('i.icon-open').removeClass('hidden');
			btnMenu.find('i.icon-close').addClass('hidden');

		}

	})
});

function log(obj) {
	console.log(obj);
}
