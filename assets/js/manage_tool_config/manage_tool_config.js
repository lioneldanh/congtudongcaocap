$(document).ready(function () {
	if (!$('#imageToolConfig')[0].defaultValue) {
		$('.btn-remove-image-audio').prop('disabled', true);
		$('#image-audio-preview').hide();
	}

	if (!$('#audio')[0].defaultValue) {
		$('.btn-remove-audio').prop('disabled', true);
		$('#preview-audio').hide();
	}

	$(document).on("click", ".btn-upload-image-audio", function () {
		$('#imageToolConfig').trigger('click');
	});

	$(document).on("change", "#imageToolConfig", function () {
		readURL(this);
		show_image();
	});

	$(document).on("click", ".btn-upload-audio", function () {
		$('#audio').trigger('click');
	});

	$(document).on("change", "#audio", function (e) {
		read_audio(e);
		show_button_upload_audio();
	});

	$(document).on("click", ".btn-save-tool-config", function () {
		let tool_name = $('#toolName').val();
		let image_tool_config = $('#imageToolConfig').val();
		let audio = $('#audio').val();
		let story = $('#story').val();
		let status = $('.status:checked').val();
		let id = $('#id').val();
		if (tool_name) {
			if (!id && !image_tool_config && !audio) {
				$.notify("Vui lòng điền đầy đủ thông tin!", "warn");
				return false;
			}
			let form_data = new FormData();
			form_data.append('image_tool_config', $('#imageToolConfig')[0].files[0]);
			form_data.append('audio_tool_config', $('#audio')[0].files[0]);
			form_data.append('tool_name', tool_name);
			form_data.append('story',story);
			form_data.append('status', status);
			if (id) {
				form_data.append('id', id);
			}
			ajax_save_tool_config(form_data);
		} else {
			$.notify("Vui lòng điền đầy đủ thông tin!", "warn");
		}
	});
});

function ajax_save_tool_config(form_data) {
	let url = $('.btn-save-tool-config').attr('data-url');
	$.ajax({
		url: url,
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		dataType: 'json',
		beforeSend: function () {
			show_loading();
		},
		success: function (data) {
			let status = data.status;
			if (status) {
				$.notify('Lưu thông tin thành công!', "success");
				setTimeout(function () {
					hide_loading();
					window.location.href = data.url;
				},2000);
			} else {
				hide_loading();
				$.notify(data.msg, "warn");
			}
		},
		error: function (a, b, c) {
			hide_loading();
			console.log(a + b + c);
		}
	});
}

function show_image() {
	let av = $('#imageToolConfig').val();
	if (av) {
		$('.btn-remove-image-audio').prop('disabled', false);
		$('#image-audio-preview').show();
	} else {
		$('.btn-remove-image-audio').prop('disabled', true);
		$('#image-audio-preview').hide();
	}
}

function readURL(input) {
	if (input.files && input.files[0]) {
		let reader = new FileReader();

		reader.onload = function(e) {
			$('#image-audio-preview').attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}

function show_button_upload_audio() {
	let av = $('#audio').val();
	if (av) {
		$('.btn-remove-audio').prop('disabled', false);
		$('#preview-audio').show();
	} else {
		$('.btn-remove-audio').prop('disabled', true);
		$('#preview-audio').hide();
	}
}

function read_audio(event) {
	let files = event.target.files;
	let type = files[0].type;
	type = type.split('/');
	if (type[0] === 'audio') {
		$("#audio-preview").attr("src", URL.createObjectURL(files[0]));
		$("#preview-audio").load();
	} else {
		$.notify('Không đúng dịnh dạng', "warn");
	}
}
