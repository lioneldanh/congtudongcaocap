/**
 * Created by miunh_nim on 10-Jun-19.
 */
$(document).ready(function ($) {
    // save add new time server down
    $(document).on('click', '.time-server-down-form .btn-add-new-time', function (e) {
        show_loading();
        $.ajax({
            url     : $(this).attr('data-url'),
            type    : "POST",
            data    : {
                start_time: $('.time-server-down-form .start_time').val(),
                end_time  : $('.time-server-down-form .end_time').val(),
                note      : $('.time-server-down-form .note').val(),
            },
            dataType: "json",
            success : function (data) {
                hide_loading();
                if (data.status == '1') {
                    $.notify(data.msg);
                    window.location.reload();
                } else {
                    $.notify(data.msg, 'warn');
                }
            }
        });
    })

    /**
     *delete item
     */
    $(document).on('click', '.btn-delete-item', function (e) {
        show_loading();
        $.ajax({
            url     : $(this).attr('data-url'),
            type    : "POST",
            data    : {},
            dataType: "json",
            success : function (data) {
                hide_loading();
                if (data.status == '1') {
                    $.notify(data.msg);
                    $('tr[data-id=' + data.id + ']').fadeOut('slow');
                } else {
                    $.notify(data.msg, 'warn');
                }
            }
        });
    })
});

function show_loading() {
    // update size overlay
    var loadingModal = $("#loading-modal");
    var contentLayout = $(".content-layout");
    loadingModal.height(contentLayout.height());
    loadingModal.width(contentLayout.width());
    // show loading
    loadingModal.show();
}

function hide_loading() {
    setTimeout(function () {
        $("#loading-modal").hide();
    }, 200);
}
