/**
 * Created by miunh_nim on 28-May-19.
 */
$(document).ready(function ($) {
    var myChart = new Chart(document.getElementById("line-chart"), {
            type   : 'line',
            data   : {
                labels  : [],
                datasets: [
                    {
                        data            : [],
                        label           : "Registration",
                        borderColor     : "#cd0066",
                        fill            : false,
                        borderWidth     : 1,
                        pointBorderWidth: 0,
                        pointStyle      : 'line',
                        pointHitRadius  : 4,
                    },
                    {
                        data            : [],
                        label           : "Online",
                        borderColor     : "#03ae1e",
                        fill            : false,
                        borderWidth     : 1,
                        pointBorderWidth: 0,
                        pointStyle      : 'line',
                        pointHitRadius  : 4,
                    },
                    {
                        data            : [],
                        label           : "Completion",
                        borderColor     : "#0034ae",
                        fill            : false,
                        borderWidth     : 1,
                        pointBorderWidth: 0,
                        pointStyle      : 'line',
                        pointHitRadius  : 4,
                    }

                ]
            },
            options: {
                title     : {
                    display : true,
                    text    : '',
                    fontSize: 16,
                },
                responsive: true,
                scales    : {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            // suggestedMin: 0,
                            // suggestedMax: 1000,
                        }
                    }]
                },
                elements  : {
                    point: {
                        radius: 0
                    }
                }
            }
        })
        ;

    // show_loading();
    getChartData(myChart);

    $(document).on('click', '.filter-block .btn-submit', function (e) {
        getChartData(myChart);
    })


})

function getChartData(myChart) {
    var timeDiff = parseInt((Date.parse($('.to-time').val()) - Date.parse($('.from-time').val())) / (24 * 60 * 60 * 1000));
    if (timeDiff > 32) {
        alert('Invalid datetime filter. System only accept for less than 32 days duration. Please select again?')
    } else {
        callAjax(myChart)
    }
}

function callAjax(myChart) {
    show_loading();
    $.ajax({
        url     : $('body').attr('data-chart-url'),
        type    : "POST",
        data    : {
            from     : $('.from-time').val(),
            to       : $('.to-time').val(),
            course_id: $('.select-course').val(),
            // user_id  : $('.select-user').val(),
        },
        dataType: "json",
        success : function (data) {
            // add new label and data point to chart's underlying data structures
            myChart.options.title.text = data.chart_title;
            myChart.data.labels = data.chart_label;
            $('.total_login').text(data.total_login);
            $('.total_complete').text(data.total_complete);
            $('.total_register').text(data.total_register);
            myChart.data.datasets[0].data = data.chart_reg_raw;
            myChart.data.datasets[1].data = data.chart_active_raw;
            myChart.data.datasets[2].data = data.chart_complete_raw;
            if (!data.check_range_valid) {
                $('.from-time').val(data.from);
            }
            // myChart.data.datasets[1].data = data.chart_course_raw;

            // re-render the chart
            myChart.update();
            hide_loading();
        }
    });
}


function show_loading() {
    // update size overlay
    var loadingModal = $("#loading-modal");
    var contentLayout = $(".content-layout");
    loadingModal.height(contentLayout.height());
    loadingModal.width(contentLayout.width());
    // show loading
    loadingModal.show();
}

function hide_loading() {
    setTimeout(function () {
        $("#loading-modal").hide();
    }, 200);
}
