$(document).ready(function(){
	$(document).on("click", ".btn-save-publisher", function () {
		let id = $('#publisherId').val();
		let publisher_name = $('#publisherName').val();
		let trade_name = $('#tradeName').val();
		let business_registration_number = $('#businessRegistrationNumber').val();
		let publisher_code = $('#publisherCode').val();
		let username = $('#userName').val();
		let password = $('#password').val();
		let re_password = $('#rePassword').val();
		let email = $('#email').val();
		let email_delegate = $('#emailDelegate').val();
		let number_phone = $('#numberPhone').val();
		let fax = $('#fax').val();
		let address = $('#address').val();
		let city = $('#city').val();
		let country = $('#country').val();
		let status = $('#status').val();
		if (publisher_name && business_registration_number && publisher_code && username && email && email_delegate && trade_name) {
			let data = {};
			data['id'] = id;
			data['publisher_name'] = publisher_name;
			data['business_registration_number'] = business_registration_number;
			data['publisher_code'] = publisher_code;
			data['trade_name'] = trade_name;
			data['username'] = username;
			data['email'] = email;
			data['email_delegate'] = email_delegate;
			data['number_phone'] = number_phone;
			data['fax'] = fax;
			data['address'] = address;
			data['city'] = city;
			data['country'] = country;
			data['status'] = status;
			if (!id) {
				if (password && re_password) {
					if (password !== re_password) {
						$.notify('Mật khẩu không trùng nhau!', 'warn');
						return false;
					}
				} else {
					$.notify('Vui lòng điền đầy đủ thông tin!', 'warn');
					return false;
				}
			} else {
				if (password && re_password && (password !== re_password)) {
					$.notify('Mật khẩu không trùng nhau!', 'warn');
					return false;
				}
			}
			data['password'] = password;
			data['re_password'] = re_password;
			ajax_save_publisher(data);
		} else {
			$.notify('Vui lòng điền đầy đủ thông tin!', 'warn');
		}
	});

});

function ajax_save_publisher(data) {
	let url = $('.btn-save-publisher').attr('data-url');
	$.ajax({
		url        : url,
		type       : "POST",
		data       : data,
		dataType   : "json",
		beforeSend : function () {
			show_loading();
		},
		success    : function (data) {
			if (data.status) {
				$.notify("Lưu thông tin thành công", "success");
				setTimeout(function () {
					hide_loading();
					window.location.href = data.url;
				},2000);
			} else {
				hide_loading();
				$.notify(data.msg, "warn");
			}
		},
		error      : function (a, b, c) {
			hide_loading();
			$.notify("Đã có lỗi xảy ra vui lòng thử lại!", "warn");
			alert(a + b + c);
			// window.location = url;
		}
	});
}
