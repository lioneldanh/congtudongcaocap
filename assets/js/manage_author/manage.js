$(document).ready(function(){
	$(document).on('click', '.learn-more', function () {
		$(this).parents('.author_story').find('.text-hidden').show();
		$(this).parents('.text-more').hide();
	});

	$(document).on('click', '.hide-more', function () {
		$(this).parents('.author_story').find('.text-more').show();
		$(this).parents('.text-hidden').hide();
	});
});
