$(document).ready(function(){
	if (!$('#avatar')[0].defaultValue) {
		$('.btn-remove-avatar').prop('disabled', true);
		$('#image-avatar').hide();
	}

	$(document).on("change", "#avatar", function () {
		readURL(this);
		show_image();
	});

	$(document).on("click", ".btn-upload-avatar", function () {
		$('#avatar').trigger('click');
	});

	$(document).on("click", ".btn-save-author", function () {
		let id = $('#authorId').val();
		let avatar_author = $('#avatar')[0].files[0];
		let author_name = $('#authorName').val();
		let birthday = $('#birthday').val();
		let gender = $('#gender').val();
		let author_tab = $('#authorTab').val();
		let status = $('#status').val();
		let author_story = CKEDITOR.instances['author_story'].getData();
		if (author_name) {
			let form_data = new FormData();
			form_data.append('id', id);
			form_data.append('avatar_author', avatar_author);
			form_data.append('author_name', author_name);
			form_data.append('birthday', birthday);
			form_data.append('author_tab', author_tab);
			form_data.append('gender', gender);
			form_data.append('status', status);
			form_data.append('author_story', author_story);
			ajax_save_author(form_data);
		} else {
			$.notify('Tên tác giả không được bỏ trống!', 'warn');
		}
	});

});

function show_image() {
	let av = $('#avatar').val();
	if (av) {
		$('.btn-remove-avatar').prop('disabled', false);
		$('#image-avatar').show();
	} else {
		$('.btn-remove-avatar').prop('disabled', true);
		$('#image-avatar').hide();
	}
}

function readURL(input) {
	if (input.files && input.files[0]) {
		let reader = new FileReader();

		reader.onload = function(e) {
			$('#image-avatar').attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}

function ajax_save_author(form_data) {
	let url = $('.btn-save-author').attr('data-url');
	$.ajax({
		url        : url,
		type       : "POST",
		data       : form_data,
		dataType   : "json",
		contentType: false,
		processData: false,
		beforeSend : function () {
			show_loading();
		},
		success    : function (data) {
			if (data.status) {
				$.notify("Lưu thông tin thành công", "success");
				setTimeout(function () {
					hide_loading();
					window.location.href = data.url;
				},2000);
			} else {
				hide_loading();
				$.notify(data.msg, "warn");
			}
		},
		error      : function (a, b, c) {
			hide_loading();
			$.notify("Đã có lỗi xảy ra vui lòng thử lại!", "warn");
			alert(a + b + c);
			// window.location = url;
		}
	});
}
