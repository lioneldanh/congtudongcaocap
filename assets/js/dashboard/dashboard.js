$(document).ready(function () {
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawChart);
	$(document).on('change', '.input-date-from', function () {
		load_info_chart();
	});

	$(document).on('change', '.input-date-to', function () {
		load_info_chart();
	});

	$(document).on('change', '.select-picker-status', function () {
		load_info_chart();
	});

	$(document).on('change', '.select-picker-status-export', function () {
		load_export();
	});

	$(document).on('change', '.input-date-from-export', function () {
		load_export();
	});

	$(document).on('change', '.input-date-to-export', function () {
		load_export();
	});

	$(document).on('change', '.register-number-export', function () {
		load_export();
	});

	$(document).on('change', '.test-number-export', function () {
		load_export();
	});

	$(document).on('change', '.score-number-export', function () {
		load_export();
	});

	$(document).on('click', '.btn-export-data-dashboard', function () {
		show_loading();
		let url = $('.btn-export-data-dashboard').attr('data-url');
		let from = ($('.input-date-from-export').val()) ? $('.input-date-from-export').val() : '';
		let to  =  ($('.input-date-to-export').val()) ? $('.input-date-to-export').val() : '';
		let number_register  =  ($('.register-number-export').val()) ? $('.register-number-export').val() : '';
		let number_test  =  ($('.test-number-export').val()) ? $('.test-number-export').val() : '';
		let score  =  ($('.score-number-export').val()) ? $('.score-number-export ').val() : '';
		let status = [];
		$(".select-picker-status-export").find('option:selected').each(function (i) {
			status[i] = $(this).val();
		});
		event.preventDefault();
		$.ajax({
			type: "POST",
			url: url,
			data      : {
				status: status,
				from: from,
				to: to,
				number_register: number_register,
				number_test: number_test,
				score: score
			},
			success:function(data){
				hide_loading();
				$('.export-popup-modal').modal('hide');
				$("#link-download-export").attr("href", data);
				$('.export-popup-modal-success').modal('show');
			},
			error:function(data){
				hide_loading();
				$('.export-popup-modal').modal('hide');
				$.notify('Không thể import', "warn");
			}
		})
	});

});

function load_info_chart() {
	let from = ($('.input-date-from').val()) ? $('.input-date-from').val() : '';
	let to  =  ($('.input-date-to').val()) ? $('.input-date-to').val() : '';
	let status = [];
	let url = $('.load-info-chart').attr('data-url');
	$(".select-picker-status").find('option:selected').each(function (i) {
		status[i] = $(this).val();
	});
	$.ajax({
		type: "POST",
		url: url,
		data      : {
			status: status,
			from: from,
			to: to
		},
		dataType  : "json",
		success: function(data){
			$('.load-info-chart').html(data.html);
			google.charts.load('current', {'packages':['corechart']});
			google.charts.setOnLoadCallback(drawChart);
			hide_loading();
		}
	});
}

function drawChart() {

	let data = [];
	let date = [];
	data['date'] = [];
	data['count'] = [];
	data['status'] = [];
	let status = [];
	let count = [];
	$('.count-student-status').each(function(i){
		data['date'][i] = $(this).attr('data-date');
		data['count'][i] = $(this).attr('data-count');
		data['status'][i] = $(this).attr('data-status');
		date[i] = $(this).attr('data-date');
		status[i] = $(this).attr('data-status');
		count[i] = $(this).attr('data-count');
	});

	console.log(data);

	date = date.filter(function (item, index) {
		return date.indexOf(item) === index;
	});

	status = status.filter(function (item, index) {
		return status.indexOf(item) === index;
	});

	let a = [];
	for (let i = 0; i < date.length; i ++) {
		a[i] = [];
		for (let j = 0; j < data['date'].length; j ++) {
			if (date[i] === data['date'][j]) {
				for (let k = 0; k < status.length; k ++) {
					if (status[k] === data['status'][j]) {
						if (a[i][k]) {
							a[i][k] = parseInt(data['count'][j]);
						} else {
							a[i].push(parseInt(data['count'][j]));
						}
					} else {
						if (!a[i][k]) {
							a[i].push("0");
						}
					}
				}
			}
		}
		a[i].unshift(date[i]);
	}
	for (let i = 0; i < a.length; i++) {
		for (let j = 1; j < a[i].length; j++) {
			a[i][j] = parseInt(a[i][j]);
		}
	}
	status.unshift('Ngày');
	let array = [status];
	for (let i = 0; i < a.length; i++) {
		array.push(a[i]);
	}
	console.log(array);

	var data_chart = google.visualization.arrayToDataTable(array);


	var options = {
		curveType: 'function',
		legend: { position: 'bottom' }
	};

	var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

	chart.draw(data_chart, options);
}

function load_export() {
	show_loading();
	let from = ($('.input-date-from-export').val()) ? $('.input-date-from-export').val() : '';
	let to  =  ($('.input-date-to-export').val()) ? $('.input-date-to-export').val() : '';
	let number_register  =  ($('.register-number-export').val()) ? $('.register-number-export').val() : '';
	let number_test  =  ($('.test-number-export').val()) ? $('.test-number-export').val() : '';
	let score  =  ($('.score-number-export').val()) ? $('.score-number-export ').val() : '';
	let status = [];
	let url = $('#filter-export').attr('data-url');
	$(".select-picker-status-export").find('option:selected').each(function (i) {
		status[i] = $(this).val();
	});

	$.ajax({
		type: "POST",
		url: url,
		data      : {
			status: status,
			from: from,
			to: to,
			number_register: number_register,
			number_test: number_test,
			score: score
		},
		success: function(data){
			$('#number-export').html('Có tổng cộng ' + data + ' bản ghi.');
			hide_loading();
		}
	});
}
