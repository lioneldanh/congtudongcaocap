$(document).ready(function(){
	$(document).on('click', '.btn-edit-meta-data', function () {
		let self = this;
		let book_id = $('#bookId').val();
		let type = $(self).attr('data-edit');
		let url = $('.form-meta-data-book').attr('data-url');
		$('.form-edit-book').remove();
		$('.text-info-meta-data-book').show();
		if (book_id) {
			$.ajax({
				url 	: url,
				type	: "POST",
				data	: {
					book_id: book_id,
					type: type
				},
				dataType: "json",
				beforeSend: function () {
					show_loading();
				},
				success: function (data) {
					$(self).parents('.col-md-12:first').find('.info-meta-data-book').append(data.html);
					$(self).parents('.col-md-12:first').find('.text-info-meta-data-book').hide();
				},
				error: function (a, b, c) {
					console.log(a + b + c);
				},
				complete: function () {
					hide_loading();
				}
			});
		} else {
			$.notify('Đã có lỗi xảy ra!!', 'warn');
		}
	});

	$(document).on('click', '.btn-save-format-book', function () {
		let self = this;
		let book_id = $('#bookId').val();
		let value = $('.select-format-book').val();
		let v = '';
		value.forEach(function (e) {
			if (!v) {
				v = e;
			} else {
				v += ',' + e;
			}
		});
		let type = $(self).parents('.col-md-12:first').find('.btn-edit-meta-data').attr('data-edit');
		if (book_id) {
			load_ajax(v, book_id, type, self);
		} else {
			$.notify('Đã có lỗi xảy ra!!', 'warn');
		}
	});

	$(document).on('change', '.form-edit-meta-data', function () {
		let self = this;
		let book_id = $('#bookId').val();
		let value = $(self).val();
		let type = $(self).parents('.col-md-12:first').find('.btn-edit-meta-data').attr('data-edit');
		if (book_id) {
			load_ajax(value, book_id, type, self);
		} else {
			$.notify('Đã có lỗi xảy ra!!', 'warn');
		}
	});

	$(document).on('change', '.form-edit-meta-data-isbn', function () {
		let self = this;
		let book_id = $('#bookId').val();
		let value = Number($(self).val());
		let type = $(self).parents('.col-md-12:first').find('.btn-edit-meta-data').attr('data-edit');
		if (book_id) {
			if (Number.isInteger(value)) {
				let length = value.toString().length;
				if (length !== 12) {
					$.notify('Mã số sách phải là 12 số!!', 'warn');
				} else {
					load_ajax(value, book_id, type, self);
				}
			} else {
				$.notify('Mã số sách phải ở dạng số!!', 'warn');
			}
		} else {
			$.notify('Đã có lỗi xảy ra!!', 'warn');
		}
	});

	$(document).on('change', '.form-edit-meta-data-price', function () {
		let self = this;
		let book_id = $('#bookId').val();
		let value = Number($(self).val());
		let type = $(self).parents('.col-md-12:first').find('.btn-edit-meta-data').attr('data-edit');
		if (book_id) {
			if (Number.isInteger(value)) {
				let url = $('.form-meta-data').attr('data-url');
				$.ajax({
					url 	: url,
					type	: "POST",
					data	: {
						value: value,
						book_id: book_id,
						type: type,
						type_price: '1'
					},
					dataType: "json",
					beforeSend: function () {
						show_loading();
					},
					success: function (data) {
						$(self).parents('.info-meta-data-book').find('.text-info-meta-data-book').text(data);
						$(self).parents('.info-meta-data-book').find('.text-info-meta-data-book').show();
						$('.form-edit-book').remove();
					},
					error: function (a, b, c) {
						console.log(a + b + c);
					},
					complete: function () {
						hide_loading();
					}
				});
			} else {
				$.notify('Giá phải ở dạng số!!', 'warn');
			}
		} else {
			$.notify('Đã có lỗi xảy ra!!', 'warn');
		}
	});

	$(document).on('click', '.btn-edit-book-cover', function () {
		$('.input-book-cover').trigger('click');
	});

	$(document).on('click', '.btn-back-cancel', function () {
		$(this).parents('.info-meta-data-book:first').find('.text-info-meta-data-book').show();
		$(this).parents('.form-edit-book:first').remove();
	});

	$(document).on('change', '.input-book-cover', function () {
		let self = this;
		let url = $(this).attr('data-url');
		let upload = new FormData();
		let book_id = $('#bookId').val();
		upload.append( 'book_cover', $(self)[0].files[0] );
		upload.append('book_id', book_id);
		if (book_id) {
			$.ajax({
				url       : url,
				type      : "POST",
				data	  : upload,
				contentType: false,
				cache:false,
				processData:false,
				beforeSend: function () {
					show_loading();
				},
				success   : function (data) {
					readURL(self);
				},
				error     : function (a, b, c) {
					console.log(a + b + c);
				},
				complete	: function () {
					hide_loading();
				}
			});
		} else {
			$.notify('Đã có lỗi xảy ra!', 'warn');
		}
	});

	$(document).on('click', '.btn-next-censorship', function () {
		if ($('.checkbox-censorship').is(":checked")) {
			$('.censorship').text('đồng ý');
		} else {
			$('.censorship').text('không');
		}
		$('.popup-censorship-modal').modal('show');
	});

	$(document).on('click', '.btn-censorship-data', function () {
		$('.popup-censorship-modal').modal('hide');
		let value = 0;
		let book_id = $('#bookId').val();
		if ($('.checkbox-censorship').is(":checked")) {
			value = 1;
		}
		let url = $(this).attr('data-url');
		$.ajax({
			url 	: url,
			type	: "POST",
			data	: {
				censorship: value,
				book_id: book_id
			},
			beforeSend: function () {
				show_loading();
			},
			success: function (data) {
				$.notify('Kiểm duyệt thành công!', 'success');
				setTimeout(function () {
					hide_loading();
					window.location.href = data;
				}, 3000);
			},
			error: function (a, b, c) {
				hide_loading();
				console.log(a + b + c);
			}
		});
	});
});

function load_ajax(value, book_id, type, self) {
	let url = $('.form-meta-data').attr('data-url');
	$.ajax({
		url 	: url,
		type	: "POST",
		data	: {
			value: value,
			book_id: book_id,
			type: type
		},
		dataType: "json",
		beforeSend: function () {
			show_loading();
		},
		success: function (data) {
			$(self).parents('.info-meta-data-book').find('.text-info-meta-data-book').text(data);
			$(self).parents('.info-meta-data-book').find('.text-info-meta-data-book').show();
			$('.form-edit-book').remove();
		},
		error: function (a, b, c) {
			console.log(a + b + c);
		},
		complete: function () {
			hide_loading();
		}
	});
}

function readURL(input) {
	if (input.files && input.files[0]) {
		let reader = new FileReader();

		reader.onload = function(e) {
			$('.image-preview-book-cover').attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}
