$(document).ready(function(){
	$('body').delegate(".check-table-item", "click", function(){
		set_select_all();
	});

	$(document).on("click", ".btn-next-author", function () {
		show_loading();
		let count = 0;
		$('.check-table-item').each(function () {
			$(this).is(":checked") ? count++ : count;
		});
		if (count) {
			let data_author = [];
			let url = $(this).attr('data-url');
			$('.check-table-item').each(function () {
				if ($(this).is(":checked")) {
					let data_au = {};
					data_au['author_id'] = $(this).val();
					data_au['author_role'] = $(this).parents('tr:first').find('.author-role').val();
					data_au['editorial_role'] = $(this).parents('tr:first').find('.editorial-role').val();
					data_author.push(data_au);
				}
			});
			ajax_save_data_author(JSON.stringify(data_author), url);
		} else {
			let data_form = $(this).attr('data-form');
			$.notify('Vui lòng chọn ít nhất 1 ' + data_form + '!', "warn");
			hide_loading();
			return false;
		}
	});

	$(document).on("click", ".checkbox-category", function () {
		if ($(this).is(":checked")) {
			let c = $(this).parents('tr:first').find('.check-table-category').is(":checked");
			if (c) {
				$('.checkbox-category').prop('checked', false);
				$(this).prop('checked', true);
			} else {
				$(this).prop('checked', false);
			}
		}
	});

	$('body').delegate(".check-table-category", "click", function(){
		let count_check_category = $('.check-table-category:checked').length;
		if (count_check_category > 3) {
			$(this).prop('checked', false);
			$.notify('Bạn chỉ có thể chọn tối đa 3 danh mục', 'warn');
		} else {
			if (!$(this).is(":checked")) {
				$(this).parents('tr:first').find('.checkbox-category').prop('checked', false);
			}
		}
		set_select_all();
	});

	// $(document).on("click", ".category_name", function () {
	// 	select_category(this);
	// });

	$(document).on("click", ".btn-category", function () {
		show_loading();
		let count = $('.check-table-category:checked').length;
		let count_category = $('.checkbox-category:checked').length;
		if (count) {
			if (count <= 3) {
				if (count_category) {
					$('.form-manage-author').submit();
				} else {
					$.notify('Vui lòng chọn ít nhất 1 danh mục chính!', "warn");
				}
			} else {
				$.notify('Bạn chỉ có thể chọn tối đa 3 danh mục!', "warn");
			}
		} else {
			$.notify('Vui lòng chọn ít nhất 1 danh mục!', "warn");
		}
		hide_loading();
	});

	$(document).on("click", ".btn-save-category", function () {
		let count = $('.check-table-category:checked').length;
		let count_category = $('.checkbox-category:checked').length;
		if (count) {
			if (count <= 3) {
				if (count_category) {
					let data_category = [];
					let url = $(this).attr('data-url');
					$('.check-table-category').each(function () {
						if ($(this).is(":checked")) {
							let data_au = {};
							let main_category = $(this).parents('tr:first').find('.checkbox-category').is(':checked');
							data_au['category_id'] = $(this).val();
							data_au['main_category'] = main_category ? 1 : 0;
							data_category.push(data_au);
						}
					});
					ajax_save_data_author(JSON.stringify(data_category), url);
				} else {
					$.notify('Vui lòng chọn ít nhất 1 danh mục chính!', "warn");
				}
			} else {
				$.notify('Bạn chỉ có thể chọn tối đa 3 danh mục!', "warn");
			}
		} else {
			$.notify('Vui lòng chọn ít nhất 1 danh mục!', "warn");
		}
	});

	$(document).on("click", ".checkbox-same", function () {
		if ($(this).is(":checked")) {
			let share_out = $('#share_outside_book').val();
			$('#share_in_book').val(share_out);
		}
	});

	$(document).on("click", ".btn-save-publisher", function () {
		let count = 0;
		$('.check-table-item').each(function () {
			$(this).is(":checked") ? count++ : count;
		});
		if (count) {
			let data_publisher = [];
			let url = $(this).attr('data-url');
			$('.check-table-item').each(function () {
				let data_au = {};
				if ($(this).is(":checked")) {
					data_au['publisher_id'] = $(this).val();
					data_publisher.push(data_au);
				}
			});
			ajax_save_data_author(JSON.stringify(data_publisher), url);
		} else {
			let data_form = $(this).attr('data-form');
			$.notify('Vui lòng chọn ít nhất 1 ' + data_form + '!', "warn");
			hide_loading();
			return false;
		}
	});

	$(document).on("click", ".btn-save-share-info", function () {
		let share_outside_book = $('#share_outside_book').val();
		let share_in_book = $('#share_in_book').val();
		let share = {};
		let url = $(this).attr('data-url');
		share['share_outside_book'] = share_outside_book;
		share['share_in_book'] = share_in_book;
		ajax_save_data_author(JSON.stringify(share), url);
	});
});

function select_category(self) {
	let category = $(self).parents('tr:first').find('.tick-category');
	let checkbox_category = $(self).parents('tr:first').find('.checkbox-category');
	let checkbox = $(self).parents('tr:first').find('.check-table-category');
	if (checkbox.is(":checked")) {
		$('.tick-category').css("display", "none");
		$('.checkbox-category').prop('checked', false);
		category.css("display", "block");
		checkbox_category.prop('checked', true);
	}
}

function ajax_save_data_author(data, url) {
	let book_id = $('#bookId').val();
	$.ajax({
		url        : url,
		type       : "POST",
		data       : {
			data : data,
			book_id : book_id
		},
		dataType   : 'json',
		beforeSend : function () {
			show_loading();
		},
		success    : function (data) {
			let status = data.status;
			if (status) {
				$.notify('Lưu thông tin thành công!', "success");
				setTimeout(function () {
					hide_loading();
					window.location.href = data.url;
				},2000);
			} else {
				hide_loading();
				$.notify(data.msg, "warn");
			}
		},
		error      : function (a, b, c) {
			$.notify("Đã có lỗi xảy ra vui lòng thử lại!", 'warn');
			hide_loading();
			console.log(a + b + c);
		}
	});
}
