$(document).ready(function(){
	show_price();
	if (!$('#bookCover')[0].defaultValue) {
		$('#image-book-cover').hide();
	}

	$(document).on("change", "#bookCover", function () {
		readURL(this);
		show_image();
	});

	$(document).on("click", ".btn-upload-book-cover", function () {
		$('#bookCover').trigger('click');
	});

	$(document).on("change", ".select-format-book", function () {
		show_price();
	});

	$(document).on("click", ".btn-save-book", function () {
		let selected = $('.select-format-book').val();
		if (selected) {
			save_meta_data();
		} else {
			$.notify('Vui lòng chọn ít nhất 1 định dạng sách!', "warn");
			return false;
		}
	});

});

function show_image() {
	let av = $('#bookCover').val();
	if (av) {
		$('#image-book-cover').show();
	} else {
		$('#image-book-cover').hide();
	}
}

function readURL(input) {
	if (input.files && input.files[0]) {
		let reader = new FileReader();

		reader.onload = function(e) {
			$('#image-book-cover').attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}

function show_price() {
	let selected = $('.select-format-book').val();
	let price_paper = false, price_electronic = false, price_audio = false, price_video = false;
	if (selected) {
		selected.forEach(function(element){
			switch (element) {
				case '1':
					price_paper = true;
					break;
				case '2':
					price_electronic = true;
					break;
				case '3':
					price_audio = true;
					break;
				default:
					price_video = true;
					break;
			}
		});

		(price_paper)       ? $('#price-paper').show()      : $('#price-paper').hide();
		(price_electronic)  ? $('#price-electronic').show() : $('#price-electronic').hide();
		(price_audio)       ? $('#price-audio').show()      : $('#price-audio').hide();
		(price_video)       ? $('#price-video').show()      : $('#price-video').hide();
	} else {
		$('.price-book').hide();
	}
}

function save_meta_data() {
	//Get all value input
	let id = $('#id').val();
	let format_book = $('.select-format-book').val();
	let book_name = $('#bookName').val();
	let isbn = $('#isbn').val();
	let publishing_year = $('#publishingYear').val();
	let price_paper = $('#pricePaper').val();
	let price_electronic = $('#priceElectronic').val();
	let price_audio = $('#priceAudio').val();
	let price_video = $('#priceVideo').val();
	let keyword = $('#keyword').val();
	let keyword_recommendation = $('#keywordRecommendation').val();
	let description = CKEDITOR.instances['description'].getData();

	//Check format_book and book_name
	if (!format_book || !book_name) {
		$.notify('Vui lòng điền đầy đủ thông tin!', 'warn');
		return false;
	} else {
		if (isbn) {
			isbn = Number(isbn);
			if (Number.isInteger(isbn)) {
				let length = isbn.toString().length;
				if (length !== 12) {
					$.notify('Mã số sách phải có 12 ký tụ!', 'warn');
					return false;
				}
			} else {
				$.notify('Mã số sách phải ở dạng số!', 'warn');
				return false;
			}
		}
		let form_data = new FormData();
		form_data.append('id', id);
		form_data.append('format_book', format_book);
		form_data.append('book_name', book_name);
		form_data.append('isbn', isbn);
		form_data.append('publishing_year', publishing_year);
		form_data.append('price_paper', price_paper);
		form_data.append('price_electronic', price_electronic);
		form_data.append('price_audio', price_audio);
		form_data.append('price_video', price_video);
		form_data.append('keyword', keyword);
		form_data.append('keyword_recommendation', keyword_recommendation);
		form_data.append('description', description);
		form_data.append('book_cover', $('#bookCover')[0].files[0]);
		ajax_save_meta_data(form_data);
	}
}

function ajax_save_meta_data(form_data) {
	let url = $('.btn-save-book').attr('data-url');
	$.ajax({
		url        : url,
		type       : "POST",
		data       : form_data,
		dataType   : 'json',
		contentType: false,
		processData: false,
		beforeSend : function () {
			show_loading();
		},
		success    : function (data) {
			let status = data.status;
			if (status) {
				$.notify('Lưu thông tin thành công!', "success");
				setTimeout(function () {
					hide_loading();
					window.location.href = data.url;
				},2000);
			} else {
				hide_loading();
				$.notify(data.msg, "warn");
			}
		},
		error      : function (a, b, c) {
			hide_loading();
			console.log(a + b + c);
		}
	});
}
