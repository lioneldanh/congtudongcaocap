$(document).ready(function(){
	$(document).on('click', '.btn-preview-news', function () {
		let ckeditor = $(this).attr('data-display');
		$('.content-preview-news').html(ckeditor);
		$('.popup-preview-modal').modal('show');
	});
});
