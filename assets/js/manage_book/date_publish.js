$(document).ready(function(){
	$(document).on("change", ".date-publish", function () {
		let value = $(this).val();
		if (value === '2') {
			let url = $(this).attr('data-url');
			$.ajax({
				url 	: url,
				type	: "GET",
				dataType: "json",
				beforeSend: function () {
					show_loading();
				},
				success: function (data) {
					$('.form-meta-data-book').append(data.html);
				},
				error: function (a, b, c) {
					console.log(a + b + c);
				},
				complete: function () {
					hide_loading();
				}
			});
		} else {
			$('.form-datelocal').remove();
		}
	});

	$(document).on("click", ".btn-publish", function () {
		let value = $('.date-publish').val();
		let date = '';
		if (value === 2) {
			date = $('.input-date').val();
		}
		let url = $(this).attr('data-url');
		let book_id = $('#bookId').val();
		if (book_id) {
			$.ajax({
				url: url,
				type: "POST",
				data: {
					book_id: book_id,
					date: date
				},
				beforeSend: function () {
					show_loading();
				},
				success: function (data) {
					$.notify('Xuất bản thành công!', 'success');
					setTimeout(function () {
						hide_loading();
						window.location.href = data;
					}, 3000);
				},
				error: function (a, b, c) {
					hide_loading();
					console.log(a + b + c);
				}
			});
		} else {
			$.notify('Đã có lỗi xảy ra vui lòng thử lại!', 'warn');
		}
	});
});
