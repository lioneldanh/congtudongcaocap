$(document).ready(function(){
	$('.keyword-recommendation-input').keyup(function(){
		let value = $(this).val();
		let result = get_value_input(value, this);
		if (value && result) {
			$('.live_search_k_r').show();
			let ps = $(this).position();
			let left = ps.left;
			$('._s-kw-r').text(value);
			$('.live_search_k_r').css('margin-left', left);
		} else {
			$('.live_search_k_r').hide();
		}
	});

	$('.keyword-input').keyup(function(){
		let value = $(this).val();
		let result = get_value_input(value, this);
		if (value && result) {
			$('.live_search_k').show();
			let ps = $(this).position();
			let left = ps.left;
			$('._s-kw').text(value);
			$('.live_search_k').css('margin-left', left);
		} else {
			$('.live_search_k').hide();
		}
	});

	$(document).on("click", "._s-kw-r-o", function () {
		let self = this;
		let value = $(self).parents('.form-group:first').find('.ui-widget-content').val();
		let url = $(self).attr('data-url');
		$.ajax({
			url        : url,
			type       : "POST",
			data       : {
				value: value
			},
			dataType   : 'json',
			beforeSend : function () {
				show_loading();
			},
			success    : function (data) {
				let html = data.html;
				$(self).parents('.form-group:first').find('.ui-widget-content').before(html);
				$(self).parents('.form-group:first').find('.ui-widget-content').val('');
				$(self).parents('.live_search:first').hide();
				let val = $(self).parents('.form-group:first').find('.input-keyword').val();
				if (val) {
					val += ',' + value;
				} else {
					val = value;
				}
				$(self).parents('.form-group:first').find('.input-keyword').val(val);
				hide_loading();
			},
			error      : function (a, b, c) {
				hide_loading();
				console.log(a + b + c);
			}
		});
	});
});

function get_value_input(value, self) {
	let val = $(self).parents('.form-group:first').find('.input-keyword').val();
	let result = true;
	if (val) {
		val = val.split(',');
		let v = val.indexOf(value);
		if (v !== -1) {
			result = false;
		}
	}
	return result;
}
