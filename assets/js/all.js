$(document).ready(function () {
    $(".custom-file-input").on("change", function() {
        let fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
    $('.form-student-exam .datepicker').daterangepicker({
        "singleDatePicker": true,
        "timePicker": true,
        locale: {
            format: 'hh:mm A'
        }
    });


    $("body").delegate(".load-more button", "click", function () {
        ajaxLoadMoreData($('.data-table'));
    });

	$(document).on("click", ".checkbox-all-value", function () {
		$('.checkbox-all').trigger('click');
		if ($('.checkbox-all').prop('checked')) {
			$('.custom-control-input').prop('checked', true);
			$('#check_all').val('all');
			$('#id_student').val('all');
			$('#count-item').text('tất cả');
		} else {
			$('.custom-control-input').prop('checked', false);
		}
		$('.check-table-item').each(function () {
			show_select(this);
		});
		showButton();
		setCountSelected();
	});

	$(document).on("click", ".image-preview", function () {
		$('#overlay')
			.css({backgroundImage: `url(${this.src})`})
			.addClass('open')
			.one('click', function() { $(this).removeClass('open'); });
	});

	$(document).on("click", ".btn-back-history", function () {
		window.history.back();
	});
});

