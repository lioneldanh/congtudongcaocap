/**
 * Created by miunh_nim on 03-Jun-19.
 */
$(document).ready(function ($) {
    $('.search-field').keydown(function (e) {
        if (e.keyCode == 13) {
            if (!$('.search-field').val()) {
                $.notify('Vui lòng nhập mã tư vấn viên cần tìm kiếm');
                return;
            } else {
                var url = $('.search-block .btn-submit').attr('data-url');
                ajaxSearch(url);
            }
        }
    })

    $(document).on('click', '.search-block .btn-submit', function (e) {
        if (!$('.search-field').val()) {
            $.notify('Vui lòng nhập mã tư vấn viên cần tìm kiếm');
            return;
        } else {
            var url = $(this).attr('data-url');
            ajaxSearch(url);
        }
    })

    // call ajax sync user data
    $(document).on('click', '.js_btn_sync', function (e) {
        var url = $(this).attr('data-url');
        var date = $(this).attr('data-date');
        $.ajax({
            url       : url,
            type      : "POST",
            data      : {
                date: date,
            },
            dataType  : "json",
            beforeSend: function () {
                show_loading();
            },
            success   : function (dataAll) {
                if (dataAll.status) {
                    var url = $('.search-block .btn-submit').attr('data-url');
                    ajaxSearch(url);
                } else {
                    $.notify(dataAll.msg);
                }
            },
            error     : function (a, b, c) {
                alert(a + b + c);
                // window.location = url;
            },
            complete  : function () {
                hide_loading();
            }
        });
    })

    // call ajax check sync status
    $(document).on('click', '.js_btn_check_status', function (e) {
        var btnStatus
        var url = $(this).attr('data-url-check');
        $.ajax({
            url       : url,
            type      : "POST",
            data      : {},
            dataType  : "json",
            beforeSend: function () {
                show_loading();
            },
            success   : function (dataAll) {
                console.log(dataAll.status_sync);
                if (dataAll.status) {
                    if (dataAll.type == 'register') {
                        var statusSync = $('.sync_register_status');
                    } else {
                        var statusSync = $('.sync_completion_status');
                    }
                    statusSync.removeClass('hidden');
                    if (dataAll.status_sync) {
                        statusSync.find('.sync_true').removeClass('hidden');
                    } else {
                        statusSync.find('.sync_fail').removeClass('hidden');
                    }
                } else {
                    $.notify(dataAll.msg);
                }
            },
            error     : function (a, b, c) {
                alert(a + b + c);
                // window.location = url;
            },
            complete  : function () {
                hide_loading();
            }
        });
    })
})

/**
 * ajax find user
 * @param url
 * @returns {boolean}
 */
function ajaxSearch(url) {
    // var url = obj.attr("data-url");
    if (!url) {
        return false;
    }

    $.ajax({
        url       : url,
        type      : "POST",
        data      : {
            search: $('.search-field').val(),
        },
        dataType  : "json",
        beforeSend: function () {
            show_loading();
        },
        success   : function (dataAll) {
            $(".table-result").html(dataAll.html);
            $(".table-result").show();
        },
        error     : function (a, b, c) {
            alert(a + b + c);
            // window.location = url;
        },
        complete  : function () {
            hide_loading();
        }
    });
}

function show_loading() {
    // update size overlay
    var loadingModal = $("#loading-modal");
    var contentLayout = $(".content-layout");
    loadingModal.height(contentLayout.height());
    loadingModal.width(contentLayout.width());
    // show loading
    loadingModal.show();
}

function hide_loading() {
    setTimeout(function () {
        $("#loading-modal").hide();
    }, 200);
}
