$(document).ready(function (e) {

    // build select
    $('.e_content_manage').find('select').selectpicker();
    // load ajax data
    ajaxData($('.data-table'));
    hide_loading();

    // search and call ajax
    $(document).on('change', '.input-search', function () {
        var btnResetSearch = $('.js-btn-reset-search');
        if ($(this).val()) {
            btnResetSearch.addClass('active');
        } else {
            btnResetSearch.removeClass('active');
        }

        ajaxData($('.data-table'));
    });

    // reset search
    $(document).on("click", ".js-btn-reset-search", function () {
        var inputSearch = $('.input-search');
        inputSearch.val('');
        $(this).removeClass('active');
        ajaxData($('.data-table'));
    });

    // filter by time and call ajax
    $(document).on('change', '.js-filter-by-time input.input-date', function () {
        var obj = $('.data-table');
        obj.attr('data-page', '');
        ajaxData(obj);
    });

    // filter by status, type, group
    $(document).on('change', '.filter-select select', function () {
        ajaxData($('.data-table'));
    });

    // change limit
    $(document).on('change', 'input.js-limit-value', function () {

        ajaxData($('.data-table'));
    });

    // change sort by row order
    $(document).on('change', '.js-filter-item select', function () {

        ajaxData($('.data-table'));
    })

    // paging
    $(document).on("click", ".page-item", clickChangePage);

    // checkbox select all
    $(document).on('change', '.col-check input.check-all', checkAll);

    // check item -> check all/un check all
    $(document).on('change', '.col-check input.check-item', function (e) {
        var checkAll = $('.col-check input.check-all');
        var totalItemInPage = $('.js-data-table input.check-item').length;
        var numberItemChecked = $('.js-data-table input.check-item:checked').length;
        if (numberItemChecked < totalItemInPage) {
            checkAll.prop('checked', false);
        } else {
            checkAll.prop('checked', true);
        }
    });


    $(document).on("change", ".page-item.current-page-item input", changeInputPage);

    // export excel
    $(document).on('click', '.btn-export-file', function () {
        var obj = $(this);
        exportTable(obj);
    });

    // ajax show form
    $(document).on('click', '.e_ajax_form', showFormAjax);

    $(document).on('click', '.page-title .btn-sync', function (e) {
        var url = $(this).attr('data-url');
        // get list record delete
        var listId = [];
        if (!$('.js-data-table input.check-item:checked').length) {
            $.notify('Vui lòng chọn ít nhất 1 bản ghi', "warn");
            return;
        }
        $('.js-data-table input.check-item:checked').each(function (e) {
            listId.push($(this).attr('data-id'));
        })

        $.ajax({
            url       : url,
            type      : "POST",
            data      : {
                list_id: listId,
            },
            dataType  : "json",
            beforeSend: function () {
                show_loading();
            },
            success   : function (dataAll) {
                $('#modalConfirmSync').html(dataAll.html);
                $('#modalConfirmSync').modal('show');
            },
            error     : function (a, b, c) {
                alert(a + b + c);
                // window.location = url;
            },
            complete  : function () {
                hide_loading();
            }
        });
    });
    $(document).on('click', '.page-title .btn-public-private', function (e) {
        var url = $(this).attr('data-url');
        var type = $(this).attr('data-type');
        // get list record delete
        var listId = [];
        if (!$('.js-data-table input.check-item:checked').length) {
            $.notify('Vui lòng chọn ít nhất 1 bản ghi', "warn");
            return;
        }
        $('.js-data-table input.check-item:checked').each(function (e) {
            listId.push($(this).attr('data-id'));
        })

        $.ajax({
            url       : url,
            type      : "POST",
            data      : {
                list_id: listId,
                type   : type,
            },
            dataType  : "json",
            beforeSend: function () {
                show_loading();
            },
            success   : function (dataAll) {
                $('#modalConfirmDelete').html(dataAll.html);
                $('#modalConfirmDelete').modal('show');
            },
            error     : function (a, b, c) {
                alert(a + b + c);
                // window.location = url;
            },
            complete  : function () {
                hide_loading();
            }
        });
    });

    // delete record
    $(document).on('click', '#modalConfirmSync .btn-submit-sync', syncRecord);

    $(document).on('click', '#modalConfirmDelete .btn-submit-public-private', publicPrivateRecord);
    // filter by start key
    $(document).on('click', '.row-left .filter-by-character', function (e) {
        var currentKey = $(this);
        var rowLeft = currentKey.closest('.row-left');
        if (currentKey.hasClass('active')) {
            currentKey.removeClass('active');
        } else {
            rowLeft.find('.filter-by-character.active').removeClass('active');
            currentKey.addClass('active');
        }

        ajaxData($('.data-table'));
    })
    $(document).on('click', '.btn-submit-form-modal', saveAdd);

    $(document).on('click', '.btn-group-toggle .js_btn_switch', function (e) {
        var btnSwitch = $(this);
        window.location = btnSwitch.attr('data-url');
    })

    // show form import file
    $(document).on('click', '.page-title .btn-import', function (e) {
        var url = $(this).attr('data-url');
        $.ajax({
            url       : url,
            type      : "POST",
            data      : {},
            dataType  : "json",
            beforeSend: function () {
                show_loading();
            },
            success   : function (dataAll) {
                $('#modalImport').html(dataAll.html);
                $('#modalImport').modal('show');
            },
            error     : function (a, b, c) {
                alert(a + b + c);
                // window.location = url;
            },
            complete  : function () {
                hide_loading();
            }
        });
    });

	$(document).on('change', '.input-date-from', function () {
		if ($('.input-date-from').val()) {

		}
	});

	$(document).on('change', '.input-date-to', function () {
		if ($('.input-date-to').val()) {

		}
	});

	$('.manager_user .btn-import-data').on('click', function(event){
		show_loading();
		let url = $('.btn-import-user').attr('data-url'),
            data = new FormData();
        data.append('file',$( '#input_file' )[0].files[0]);
		event.preventDefault();
		$.ajax({
			url:url,
			method:"POST",
			data:data,
			contentType:false,
			cache:false,
			processData:false,
			success:function(data){

				$('#input_file').val('');
				hide_loading();
                $('.import-popup-modal').modal('hide');
                $.notify(data, "success");
			},
            error:function(data){

                $('#input_file').val('');
                hide_loading();
                $('.import-popup-modal').modal('hide');
                $.notify('Không thể import', "warn");
            }

		})
	});

	$(".btn-import-user").click(function(){
        $('.import-popup-modal').modal('show');
	});
});

/**
 * save add/edit
 * @param e
 */
function saveAdd(e) {
    e.preventDefault();
    var form = $(this).closest('form');
    let data = $(form).serializeArray();
    var url = form.attr('data-url');
    // return;
    let validate = form.validate();
    if (validate.form()) {
        $.ajax({
            url       : url,
            type      : "POST",
            data      : data,
            dataType  : "json",
            beforeSend: function () {
                show_loading();
            },
            success   : function (dataAll) {
                if (dataAll.status) {
                    $.notify(dataAll.msg, "success");
                    ajaxData($('.data-table'));
                } else {
                    $.notify(dataAll.msg, "warn");
                }
            },
            error     : function (a, b, c) {
                alert(a + b + c);
                // window.location = url;
            },
            complete  : function () {
                $('#modalFormEdit .close').click();
                hide_loading();
            }
        });
    }
}

function changeInputPage(e) {
    var page = $(this).val();
    var obj = $('.data-table');
    obj.attr('data-page', page);
    ajaxData(obj);
}

function clickChangePage(e) {
    if (!$(this).hasClass('current-page-item')) {
        var page = $(this).find('.page-link').attr('data-page');
        var obj = $('.data-table');
        obj.attr('data-page', page);
        ajaxData(obj);
    }
}

function checkAll(e) {
    var checkAll = $(this);
    if (checkAll.is(":checked")) {
        $('.js-data-table input.check-item').prop('checked', true);
    } else {
        $('.js-data-table input.check-item').prop('checked', false);
    }
}

/**
 * save sync record
 * @param e
 */
function syncRecord(e) {
    var url = $(this).attr('data-url');
    var listId = [];
    $('.js-data-table input.check-item:checked').each(function (e) {
        listId.push($(this).attr('data-id'));
    })
    $.ajax({
        url       : url,
        type      : "POST",
        data      : {
            list_id: listId,
        },
        dataType  : "json",
        beforeSend: function () {
            show_loading();
        },
        success   : function (dataAll) {
            if (dataAll.status) {
                $.notify(dataAll.msg, "success");
                // hidden record
                ajaxData($('.data-table'));
            } else {
                $.notify(dataAll.msg, "warn");
            }
        },
        error     : function (a, b, c) {
            alert(a + b + c);
            // window.location = url;
        },
        complete  : function () {
            hide_loading();
        }
    });
}

/**
 * save public/private record
 * @param e
 */
function publicPrivateRecord(e) {
    var url = $(this).attr('data-url');
    var type = $(this).attr('data-type');
    var listId = [];
    $('.js-data-table input.check-item:checked').each(function (e) {
        listId.push($(this).attr('data-id'));
    })
    $.ajax({
        url       : url,
        type      : "POST",
        data      : {
            list_id: listId,
            type   : type
        },
        dataType  : "json",
        beforeSend: function () {
            show_loading();
        },
        success   : function (dataAll) {
            if (dataAll.status) {
                $.notify(dataAll.msg, "success");
                // hidden record
                ajaxData($('.data-table'));
            } else {
                $.notify(dataAll.msg, "warn");
            }
        },
        error     : function (a, b, c) {
            alert(a + b + c);
            // window.location = url;
        },
        complete  : function () {
            hide_loading();
        }
    });
}


/**
 * ajax load table data
 * @param obj
 * @returns {boolean}
 */
function ajaxData(obj) {
    // get param ajax
    var url = obj.attr("data-url");
    if (!url) {
        return false;
    }
    var data = {
        filter  : {},
        search  : {},
        page    : {},
        limit   : 0,
        order_by: {},
    };
    data.search["search_all"] = $('.input-search').val();
    data.search["search_key"] = $('.filter-by-character.active').attr('data-key');
    // get value filter by date
    $('.js-filter-by-time input.input-date').each(function (e) {
        var itemDate = $(this);
        data.filter[itemDate.attr('data-filter')] = itemDate.val();
    })

    var limit = $('select.js-limit-value').val();
    data.page = obj.attr('data-page');
    data.limit = limit;
    // init data filter other column
    $('.js-filter').each(function (e) {
        var item = $(this);
        data.filter[item.attr('data-filter')] = item.find('select').val();
    })
    var filterItemSelected = $('.js-filter-item select option:selected');
    data.order_by[filterItemSelected.attr('data-filter-row')] = $('.js-filter-item select').val();
    $.ajax({
        url       : url,
        type      : "POST",
        data      : data,
        dataType  : "json",
        beforeSend: function () {
            show_loading();
        },
        success   : function (dataAll) {
            obj.find(".ajax-data-table").html(dataAll.html);
        },
        error     : function (a, b, c) {
            alert(a + b + c);
            // window.location = url;
        },
        complete  : function () {
            hide_loading();
        }
    });
}

/**
 * show form add/edit user
 * @param e
 */
function showFormAjax(e) {
    var url = $(this).attr('data-url');
    $.ajax({
        url       : url,
        type      : "POST",
        data      : {},
        dataType  : "json",
        beforeSend: function () {
            show_loading();
        },
        success   : function (dataAll) {
            $('#modalAjax').html(dataAll.html);
            $('#modalAjax').modal('show');
        },
        error     : function (a, b, c) {
            alert(a + b + c);
            // window.location = url;
        },
        complete  : function () {
            hide_loading();
        }
    });
}


function show_loading() {
    // update size overlay
    var loadingModal = $("#loading-modal");
    var contentLayout = $(".content-layout");
    loadingModal.height(contentLayout.height());
    loadingModal.width(contentLayout.width());
    // show loading
    loadingModal.show();
}

function hide_loading() {
    setTimeout(function () {
        $("#loading-modal").hide();
    }, 200);
}


/**
 * export excel
 * @param obj
 * @returns {boolean}
 */
function exportTable(obj) {
    // get param ajax
    var url = obj.attr("data-url");
    if (!url) {
        return false;
    }
    var data = {
        filter: {},
        search: {},
        page  : {}
    };
    data.search["search_all"] = $('.input-search').val();
    // get value filter by date
    var fromTime = $('.js-filter-by-time input.from-time').val();
    var toTime = $('.js-filter-by-time input.to-time').val();
    data.page = obj.attr('data-page');
    data.filter['from'] = fromTime;
    data.filter['to'] = toTime;
    data.filter['chat_status'] = $('.js-filter-by-status select').val();
    data.filter['chat_type'] = $('.js-filter-by-type select').val();
    data.filter['group_id'] = $('.js-filter-by-group select').val();
    data.filter['operator_name'] = $('.js-filter-by-operator select').val();

    $.ajax({
        url       : url,
        type      : "POST",
        data      : data,
        dataType  : "json",
        beforeSend: function () {
            show_loading();
        },
        success   : function () {
            console.log('export success');
        },
        error     : function (a, b, c) {
            // alert(a + b + c);
            console.log(a, b, c);
        },
        complete  : function () {
            hide_loading();
        }
    }).done(function (data) {
        var $a = $("<a>");
        $a.attr("href", data.file);
        $("body").append($a);
        $a.attr("download", "report-chat.xls");
        $a[0].click();
        $a.remove();
    });
}

/**
 * ajax load more table data
 * @param obj
 * @returns {boolean}
 */
function ajaxLoadMoreData(obj) {
    // get param ajax
    var url = obj.attr("data-url");
    if (!url) {
        return false;
    }
    var data = {
        filter  : {},
        search  : {},
        page    : {},
        limit   : 0,
        order_by: {},
    };
    data.search["search_all"] = $('.input-search').val();
    data.search["search_key"] = $('.filter-by-character.active').attr('data-key');
    // get value filter by date
    $('.js-filter-by-time input.input-date').each(function (e) {
        var itemDate = $(this);
        data.filter[itemDate.attr('data-filter')] = itemDate.val();
    })

    var limit = $('select.js-limit-value').val();
    if($('.next-btn').hasClass('disabled')){
        $('.load-more button').text('No pages to load');
        return true;
    }else{
        data.page = $('.next-btn .page-link').attr('data-page');
    }
    data.limit = limit;
    // init data filter other column
    $('.js-filter').each(function (e) {
        var item = $(this);
        data.filter[item.attr('data-filter')] = item.find('select').val();
    })
    var filterItemSelected = $('.js-filter-item select option:selected');
    data.order_by[filterItemSelected.attr('data-filter-row')] = $('.js-filter-item select').val();
    $.ajax({
        url       : url,
        type      : "POST",
        data      : data,
        dataType  : "json",
        beforeSend: function () {
            show_loading();
        },
        success   : function (dataAll) {
            obj.find(".js-data-table").append($(dataAll.html).find(".js-data-table").html());
            obj.find(".data-paging").html($(dataAll.html).find('.data-paging').html());
        },
        error     : function (a, b, c) {
            alert(a + b + c);
            // window.location = url;
        },
        complete  : function () {
            hide_loading();
        }
    });
}
$('.load-more button').click(function (e) {
    ajaxLoadMoreData($('.data-table'));
});
