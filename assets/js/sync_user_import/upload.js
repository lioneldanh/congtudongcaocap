/**
 * Created by miunh_nim on 28-Aug-19.
 */
$(document).ready(function () {
    $('.btn-upload-report').on('click', function () {
        var url = $('form').attr('data-url');
        //Lấy ra files
        var file_data = $('#file-input').prop('files')[0];
        if (typeof file_data == 'undefined') {
            $.notify('Vui lòng chọn file', 'warn');
            return;
        }
        if (!validateSize(file_data)) {
            $.notify('File quá lớn, vui lòng thử lại! Kích thước tối đa: 8MB.', 'warn');
            return;
        }

        var extension = file_data.name.substr((file_data.name.lastIndexOf('.') + 1));
        if (!/(xls|xlsx|csv)$/ig.test(extension)) {
            $.notify("Định dạng không hợp lệ: " + extension + ".  Vui lòng sử dụng file có định dạng xls,xlsx,csv.");
            return;
        }

        //khởi tạo đối tượng form data
        var form_data = new FormData();
        //thêm files vào trong form data
        form_data.append('import', file_data);
        //sử dụng ajax post
        $.ajax({
            url        : url,
            dataType   : 'json',
            cache      : false,
            contentType: false,
            processData: false,
            data       : form_data,
            type       : 'post',
            beforeSend : function () {
                show_loading();
            },
            success    : function (dataAll) {
                if (dataAll.status) {
                    $.notify(dataAll.msg, "success");
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                } else {
                    $.notify(dataAll.msg, "warn");
                }
            },
            error      : function (a, b, c) {
                alert(a + b + c);
                // window.location = url;
            },
            complete   : function () {
                hide_loading();
            }
        });
    });
});


function validateSize(file) {
    var FileSize = file.size / 1024 / 1024; // in MB
    if (FileSize > 8) {
        return false;
    } else {
        return true;
    }
}
